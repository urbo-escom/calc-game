/* Depende de https://github.com/SheetJS/js-xlsx */
var XLSX_JSON = function(XLSX) {

	var dediacritize = function(s) {
		var l = {
			'Á': 'A',
			'É': 'E',
			'Í': 'I',
			'Ó': 'O',
			'Ú': 'U',
			'Ñ': 'N',
			'á': 'a',
			'é': 'e',
			'í': 'i',
			'ó': 'o',
			'ú': 'u',
			'ñ': 'n',
		};
		var d = '';
		for (var i = 0; i < s.length; i++)
			d += l[s.charAt(i)] || s.charAt(i);
		return d;
	}

	/*
	 * Extrae en un arreglo todas las celdas que provengan de una columna
	 * que concuerde con el regex.
	 *
	 * file debe provenir de un input.files
	 */
	var input2xlsx2arr = function(file, regex, cb) {
		var reader = new FileReader();
		var func = cb || function() {};

		reader.addEventListener('load', function(e) {
			var data = e.target.result;
			var wb;

			try {
				wb = XLSX.read(data, {type: 'binary'});
			} catch(e) {
				var err = e.message.toLowerCase();
				// TODO better error reporting
				if (err.match(/unsupported/))
					func(null, file);
				else if (err.match(/undefined/))
					func(null, file);
				return;
			}

			/*
			 * extrae todas las celdas de cada hoja a un arreglo
			 */
			var j = {};
			var c = [];
			wb.SheetNames.forEach(function(s) {
				var r = XLSX.utils.sheet_to_row_object_array(
					wb.Sheets[s]);
				s = dediacritize(s);
				if (r.length <= 0)
					return;
				j[s] = [];
				for (var i = 0; i < r.length; i++) {
					var v = {};
					for (var k in r[i]) {
						if (!r[i].hasOwnProperty(k))
							continue;
						var e = dediacritize(k).
							toLowerCase();
						var f = dediacritize(r[i][k])
							.trim();
						if (e.match(regex))
							c.push(f);
						v[e] = f;
					}
					j[s].push(v);
				}
			});

			func(c, file.name);
		});
		reader.readAsBinaryString(file);
	}

	return {
		input2xlsx2arr: input2xlsx2arr,
	};
}
