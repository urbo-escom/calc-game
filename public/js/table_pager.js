/*
 * Agrega paginación a una tabla, hay que proveer el theader, tbody y tfoot,
 * junto con el parámetro de offset, el limit, y el total de toda la tabla;
 * también se deben proveer las funciones para extraer nuevos elementos.
 *
 * Del theader extrae los th's que tengan atributos data-col y usa estos
 * atributos en el mismo orden encontrado para substraer los valores que
 * se llenarán en la tabla.
 *
 * Del tbody extrae los tr's y th's que van a ser llenados por los valores
 * provistos, no crea ningún elemento así que el número de tr's y th's
 * debe ser lo suficientemente grande para los valores.
 *
 * Del tfoot extrae los controles para moverse al inicio, al final, a la
 * página previa, a la página siguiente y cualquier página, en la siguiente
 * tabla aparecen las clases y tipos de elementos que deben ser estos
 * controles.
 *
 *          clase      |         tipo de elemento         |   descripción
 *         ------------|----------------------------------|------------------
 *          first      |             <button>             | primera página
 *          prev       |             <button>             | página anterior
 *          next       |             <button>             | página siguiente
 *          last       |             <button>             | última página
 *          input      |             <input>              | página actual
 *          page-total |   cualquiera que soporte texto   | total de páginas
 *          item-start |   cualquiera que soporte texto   | primer valor
 *          item-end   |   cualquiera que soporte texto   | último valor
 *          item-total |   cualquiera que soporte texto   | total de valores
 *
 * El objeto resultante también provee funciones para activar los controles
 * del tfoot.
 *
 * Parámetros
 *
 * ** Esta información se usa para calcular nuevos offsets y páginas **
 *
 *         offset   índice actual (empieza en 0)
 *         limit    máximo número de elementos mostrados
 *         total    total de elementos existentes
 *
 *
 * Funciones necesarias para cambiar de página, en todas ellas el offset es
 * el índice que tendría la nueva página y limit es el número de elementos
 * máximo a extraer, ready es una función que espera los valores de la nueva
 * página como json. Las claves del json se usan para acomodar cada columna
 * en el mismo orden en que aparecen los data-col's en los th del theader.
 *
 *         onfirst  callback(offset, limit, ready(items) {...})
 *         onprev   callback(offset, limit, ready(items) {...})
 *         onnext   callback(offset, limit, ready(items) {...})
 *         onlast   callback(offset, limit, ready(items) {...})
 *         onpage   callback(offset, limit, ready(items) {...})
 *
 * Objeto
 *         return {
 *                 offset(p), // establece un nuevo offset
 *                 limit(p), // establece un nuevo limit
 *                 total(p), // establece un nuevo total
 *                 page(p), // ve a la página indicada (índice en 0)
 *                 first, // ve a la página primera
 *                 last, // ve a la página última
 *                 prev, // ve a la página previa
 *                 next, // ve a la página siguiente
 *                 goto(p), // mueve el número de páginas previas o siguientes
 *         }
 *
 * Ejemplos
 *
 *         page(0), // ve a la página primera (first() es igual)
 *         page(-1), // ve a la última página (last() es igual)
 *         goto(-1), // ve a la página anterior (prev() es igual)
 *         goto(1), // ve a la página siguiente (next() es igual)
 *         goto(10), // ve a la página siguiente x10 (next()x10 es igual)
 *         goto(0), // no hagas nada
 *
 */
var TablePager = function(opts) {
	var table = opts.table;

	var offset = opts.offset || 0;
	var limit  = opts.limit  || 0;
	var total  = opts.total  || 0;

	var count = 0;
	var page_count = 1;
	var page_total = 1;
	var item_start = 0;
	var item_end   = 0;
	var item_total = total;

	var bpage_first   = table.getElementsByClassName('page-first')[0];
	var bpage_prev    = table.getElementsByClassName('page-prev')[0];
	var bpage_next    = table.getElementsByClassName('page-next')[0];
	var bpage_last    = table.getElementsByClassName('page-last')[0];

	var inpage_input  = table.getElementsByClassName('page-input')[0];
	var outpage_total = table.getElementsByClassName('page-total')[0];

	var outitem_start = table.getElementsByClassName('item-start')[0];
	var outitem_end   = table.getElementsByClassName('item-end')[0];
	var outitem_total = table.getElementsByClassName('item-total')[0];

	var onfirst = opts.onfirst;
	var onprev  = opts.onprev;
	var onnext  = opts.onnext;
	var onlast  = opts.onlast;
	var onpage  = opts.onpage;

	var tr_td = [];


	var calculate_pages = function() {
		page_count = Math.floor(offset/limit) + 1;
		page_total = Math.floor(total/limit);
		if (0 !== total%limit)
			page_total++;
		item_start = offset + 1;
		item_end   = offset + count;
		item_total = total;
	};


	var dispatch_changes = function() {
		inpage_input.value = page_count + '';
		inpage_input.size = (page_count + '').length + 1;
		outpage_total.innerHTML = page_total + '';
		outitem_start.innerHTML = item_start + '';
		outitem_end.innerHTML   = item_end   + '';
		outitem_total.innerHTML = item_total + '';
	};


	var verify_buttons = function() {
		bpage_prev.disabled = offset < limit;
		bpage_next.disabled = total <= offset + limit;
	};


	var generate_rows = function() {
		var th = table
			.getElementsByTagName('thead')[0]
			.getElementsByTagName('tr')[0]
			.getElementsByTagName('th');
		var tbody = table.getElementsByTagName('tbody')[0];
			tbody.innerHTML = '';
		tr_td = [];
		for (var i = 0; i < limit; i++) {
			var tr = document.createElement('tr');
			var trs = [];
			for (var j = 0; j < th.length; j++) {
				var td = document.createElement('td');
					td.innerHTML = '-';
				tr.appendChild(td);
				trs.push(td);
			}
			tbody.appendChild(tr);
			tr_td.push(trs);
		}
	};


	var update_state = function() {
		calculate_pages();
		dispatch_changes();
		verify_buttons();
	};


	var bpage_first_onclick = function(e) {
		e.preventDefault();
		onfirst(0, limit, tr_td, function(count_) {
			offset = 0;
			count  = count_;
			update_state();
		});
	};
	bpage_first.addEventListener('click', bpage_first_onclick);


	var bpage_last_onclick = function(e) {
		e.preventDefault();
		var t = total;
			t -= total%limit ? total%limit:limit;
		onlast(t, limit, tr_td, function(count_) {
			var r = total;
			offset = r - (total%limit ? total%limit:limit);
			count = count_;
			update_state();
		});
	};
	bpage_last.addEventListener('click', bpage_last_onclick);


	var bpage_prev_onclick = function(e) {
		e.preventDefault();
		if (offset < limit)
			return;
		onprev(offset - limit, limit, tr_td, function(count_) {
			offset -= limit;
			count = count_;
			update_state();
		});
	};
	bpage_prev.addEventListener('click', bpage_prev_onclick);


	var bpage_next_onclick = function(e) {
		e.preventDefault();
		if (total < offset + limit)
			return;
		onnext(offset + limit, limit, tr_td, function(count_) {
			offset += limit;
			count = count_;
			update_state();
		});
	};
	bpage_next.addEventListener('click', bpage_next_onclick);


	var inpage_input_oninput = function(e) {
		var s = inpage_input.value + '';
		var p = parseInt(inpage_input.value, 10);
		if (0 === s.length || isNaN(p) || p < 1 || page_total < p)
			return true;
		p--;
		onpage(p*limit, limit, tr_td, (function(p) {
			return function(count_) {
				offset = p*limit;
				count = count_;
				update_state();
			};
		})(p));
		return false;
	};
	inpage_input.addEventListener('input', inpage_input_oninput, true);


	calculate_pages();
	generate_rows();

	return {
		refresh: function() {
			calculate_pages();
			generate_rows();
		},
		offset: function(n) { offset = n; this.refresh(); },
		limit:  function(n) { limit  = n; this.refresh(); },
		total:  function(n) { total  = n; this.refresh(); },
		page: function(p) {
			if (p < 0)
				p = (p%page_total + page_total)%page_total;
			if (page_total <= p)
				p = p%page_total;
			inpage_input.value = p + 1;
			inpage_input_oninput();
		},
		first: function() {
			this.page(0);
		},
		last: function() {
			this.page(page_total - 1);
		},
		prev: function() {
			this.goto(-1);
		},
		next: function() {
			this.goto(1);
		},
		goto: function(n) {
			if (0 < n || n < 0)
				this.page(page_count - 1 + n);
		},
	};
};
