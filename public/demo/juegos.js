var CircleTimer = function(canvas, time) {
	var parent = canvas.parentNode;
	var context = canvas.getContext('2d');
	var data = null;

	parent.style.position = 'relative';
	canvas.position = 'absolute';
	canvas.style.top = '0';
	canvas.style.left = '0';
	canvas.style.width = parent.offsetWidth;
	canvas.style.height = parent.offsetHeight;
	canvas.width = parent.offsetWidth;
	canvas.height = parent.offsetHeight;

	time.style.paddingTop = '0.5em';
	time.style.marginLeft = 'auto';
	time.style.marginRight = 'auto';
	time.style.width = '4em';
	time.style.textAlign = 'center';
	time.style.fontSize = '2em';

	var emsize = parseFloat(getComputedStyle(
		parent, "").fontSize.match(/(\d+(\.\d*)?)px/)[1]);
	context.beginPath();
	context.closePath();
	context.fill();
	context.lineCap = 'square';
	context.lineWidth = emsize;
	context.strokeStyle = 'hsla(200, 90%, 30%, 0.4)';
	data = context.getImageData(0, 0, canvas.width, canvas.height);

	var total_msec = 60000;
	var progress = 0.0;
	var two_pi = 2*Math.PI;
	var quarter_anticlockwise = -Math.PI/2;

	var set_progress = function(p) {
		progress = p;
		var left = Math.ceil(total_msec*progress/1000);
			time.innerHTML = left + 's';
		var length = two_pi*progress;

		if (progress == 0.0) {
			time.innerHTML = '0s';
			context.clearRect(0, 0, canvas.width, canvas.width);
			return;
		}

		if (1.0 <= progress)
			length = 2*Math.PI;
		context.putImageData(data, 0, 0);
		context.beginPath();
		context.arc(
			canvas.width/2,
			canvas.height/2,
			canvas.height/2 - context.lineWidth/2,
			0 + quarter_anticlockwise,
			length + quarter_anticlockwise);
		context.stroke();
		return o;
	};

	var o = {};
	Object.defineProperty(o, 'total', {
		get: function() {
			return total_msec;
		},
		set: function(mseconds) {
			total_msec = parseInt(mseconds);
		},
	});

	Object.defineProperty(o, 'color', {
		get: function() {
			return context.strokeStyle;
		},
		set: function(color) {
			context.strokeStyle = color;
		},
	});

	Object.defineProperty(o, 'progress', {
		get: function() {
			return progress;
		},
		set: set_progress,
	});

	return o;
};


var SectionSwitcher = function() {
	var sections = [];
	var displays = [];
	var current;
	var o = {};

	o.add = function(name, section, display) {
		sections[name] = section;
		displays[name] = display || section.style.display || 'block';
		return o;
	};

	o.remove = function(name) {
		if (sections[name]) delete sections[name];
		if (displays[name]) delete displays[name];
		return o;
	};

	o.switchTo = function(name, options) {
		var opt = options || {};
		var onload;
		var onprogress;
		var time = 500;
		var delay = 70;

		if (typeof opt === 'function') {
			onload = opt;
		} else if (typeof opt === 'number') {
			time = opt;
		} else {
			if (typeof opt.onload === 'function')
				onload = opt.onload;
			if (typeof opt.onprogress === 'function')
				onprogress = opt.onprogress;
			if (typeof opt.time === 'number')
				time = opt.time;
			if (typeof opt.delay === 'number')
				delay = opt.delay;
		}

		var from = current ? sections[current]:undefined;
		var to = sections[name];

		if (from === to)
			from = undefined;

		for (var k in sections) {
			if (!sections.hasOwnProperty(k))
				continue;
			if (k != current)
				sections[k].style.display = 'none';
		}

		to.style.display = displays[name];
		to.style.opacity = 0.0;
		to.style.filter = 'alpha(opacity = 0)';
		var start = performance.now();
		var t = setInterval(function() {
			var elapsed = -start + performance.now();

			if (time < elapsed) {
				clearInterval(t);
				if (from) {
					from.style.display = 'none';
					from.style.opacity = 1.0;
					from.style.filter = '';
				}
				to.style.opacity = 1.0;
				to.style.filter = '';
				current = name;
				if (onprogress)
					onprogress(1.0);
				if (onload)
					onload();
				return;
			}

			var opacity = 1.0*elapsed/time;
			var fopac = -Math.pow(opacity - 1.0, 3);
			var tupac = Math.pow(opacity, 3);
			if (from) {
				from.style.opacity = fopac;
				from.style.filter = 'alpha(fopac = ' +
					(100*fopac)
				')';
			}
			to.style.opacity = tupac;
			to.style.filter = 'alpha(tupac = ' +
				(100*tupac)
			')';
			if (onprogress)
				onprogress(opacity);
		}, delay);
	};

	return o;
};


var CalcScreen = function(article) {
	article.style.width = '30em';
	article.style.height = '30em';


	var timer = (function() {
		var time = document.createElement('p');
			time.className = 'clocktime';
			time.innerHTML = '000s';
		var canvas = document.createElement('canvas');
			canvas.className = 'clock';
		article.appendChild(canvas);
		article.appendChild(time);
		return new CircleTimer(canvas, time);
	})();
	timer.total = 60000;
	timer.progress = 1.0;


	var article_emsize = parseFloat(
		getComputedStyle(article, "")
			.fontSize.match(/(\d+(\.\d*)?)px/)[1]
	);


	/*
	 * Calculate the dimensions of a square inscribed in the incircle
	 * of article.
	 */
	var screen = document.createElement('div');
	var w = (article.offsetWidth - 2*article_emsize)/Math.sqrt(2);
	var h = (article.offsetHeight - 2*article_emsize)/Math.sqrt(2);
	var l = (article.offsetWidth - w)/2;
	var t = (article.offsetHeight - h)/2;
		w = Math.floor(w);
		h = Math.floor(h);
		l = Math.floor(l);
		t = Math.floor(t);
	screen.style.position = 'absolute';
	screen.style.width = w + 'px';
	screen.style.height = h + 'px';
	screen.style.top = t + 'px';
	screen.style.left = l + 'px';
	article.appendChild(screen);

	var switcher = new SectionSwitcher();
	var views = [];
	var divs = [];
	var current;

	var o = {};

	o.timer = timer;

	o.start = function(name, param, switch_options) {
		switch_options = switch_options || {};
		if (!views[name])
			throw new Error('No view named ' + name);

		if (!divs[name]) {
			divs[name] = document.createElement('section');
			divs[name].style.display = 'none';
			divs[name].style.position = 'absolute';
			divs[name].style.width = '100%';
			divs[name].style.height = '100%';
			views[name].oncreate(divs[name], param);
			screen.appendChild(divs[name]);
			switcher.add(name, divs[name], 'block');
		}

		if (current &&
		typeof views[current].onpreunload == 'function')
			views[current].onpreunload();

		if (typeof views[name].onpreload == 'function')
			views[name].onpreload(param);

		var notify = function() {
			if (current &&
			typeof views[current].onunload == 'function')
				views[current].onunload();
			if (typeof views[name].onload == 'function')
				views[name].onload(param);
			current = name;
		};

		if (switch_options.onload) {
			var t = switch_options.onload;
			switch_options.onload = function() {
				if (typeof t == 'function')
					t();
				notify();
			};
		} else {
			switch_options.onload = notify;
		}
		switcher.switchTo(name, switch_options);
	};

	o.add_view = function(name, view) {
		if (typeof view.oncreate != 'function')
			throw new Error("View doesn't implement oncreate");
		views[name] = view;
		return o;
	};
	o.remove_view = function(name) {
		switcher.remove(name);
		if (views[name]) delete views[name];
		if (divs[name])  delete divs[name];
		return o;
	};

	return o;
};


var CalcStat = function(section) {
	var progress = document.createElement('progress');
	var stat = document.createElement('p');
	var text = document.createElement('span');
	var value = document.createElement('span');
	var max = document.createElement('span');

	var slash = document.createElement('span');
		slash.innerHTML = '/';
	stat.appendChild(text);
	stat.appendChild(value);
	stat.appendChild(slash);
	stat.appendChild(max);

	text.className = 'text';
	value.className = 'value';
	max.className = 'max';
	section.appendChild(progress);
	section.appendChild(stat);

	var o = {};

	Object.defineProperty(o, 'text', {
		get: function() {
			return text.innerHTML;
		},
		set: function(message) {
			text.innerHTML = message;
		},
	});

	Object.defineProperty(o, 'value', {
		get: function() {
			return progress.value;
		},
		set: function(v) {
			progress.value = v;
			value.innerHTML = v;
		},
	});

	Object.defineProperty(o, 'max', {
		get: function() {
			return progress.max;
		},
		set: function(v) {
			progress.max = v;
			max.innerHTML = v;
		},
	});

	return o;
};



var CalcInput = function(section) {
	var operand_a;
	var operand_b;
	var operation;
	var equal_sign;
	var result;
	var da = document.createElement('div');
	var db = document.createElement('div');
	var dc = document.createElement('div');
		da.className = 'stat_panel';
		db.className = 'operation_panel';
		dc.className = 'result_panel';

	var calc_stat = new CalcStat(da);

	operand_a = document.createElement('output');
	operand_a.className = 'operand_a';
	operand_b = document.createElement('output');
	operand_b.className = 'operand_b';
	operation = document.createElement('output');
	operation.className = 'operation';
		db.appendChild(operand_a);
		db.appendChild(operation);
		db.appendChild(operand_b);

	equal_sign = document.createElement('output');
	equal_sign.className = 'equal';
	result = document.createElement('input');
	result.className = 'result';
		dc.appendChild(equal_sign);
		dc.appendChild(result);

	section.appendChild(da);
	section.appendChild(db);
	section.appendChild(dc);

	var o = {};

	o.stat = calc_stat;
	o.result = result;

	Object.defineProperty(o, 'operand_a', {
		get: function() {
			return operand_a.value;
		},
		set: function(v) {
			operand_a.value = v;
		},
	});

	Object.defineProperty(o, 'operand_b', {
		get: function() {
			return operand_b.value;
		},
		set: function(v) {
			operand_b.value = v;
		},
	});

	Object.defineProperty(o, 'operation', {
		get: function() {
			return operation.value;
		},
		set: function(v) {
			operation.value = v;
		},
	});

	Object.defineProperty(o, 'equal_sign', {
		get: function() {
			return equal_sign.value;
		},
		set: function(v) {
			equal_sign.value = v;
		},
	});

	return o;
};


function create_select(options) {
	var lt = options.label;
	var os = options.options;
	var s = document.createElement('select');
	var l = document.createElement('label');
	var t = document.createElement('em');
		t.innerHTML = lt;
		l.appendChild(t);

	var o = [];
	os.forEach(function(e) {
		var opt = document.createElement('option');
		opt.value = e.value;
		opt.innerHTML = e.text;
		if (options.selected && e.text == options.selected)
			opt.selected = true;
		o.push(opt);
	});
		o.forEach(function(e) { s.appendChild(e); });
		l.appendChild(s);
	return {
		label: l,
		select: s,
		option: o,
	};
}



var Digits = {};
Digits.min = function min(size) {
	return Math.pow(10, size - 1);
};
Digits.max = function max(size) {
	return Math.pow(10, size) - 1;
};
Digits.rand = function rand(size) {
	var range = Digits.max(size) - Digits.min(size) + 1;
	return Math.floor(Math.random()*range) + Digits.min(size);
}


var SumasConsec = function(digit_size) {
	var size = digit_size || 1;
	var nums = [];
	var sum = 0;

	var o = {};

	Object.defineProperty(o, 'size', {
		get: function() {
			return size;
		},
		set: function(s) {
			size = s;
			nums = [];
			sum = 0;
		},
	});

	Object.defineProperty(o, 'next', {
		get: function() {
			var n = Digits.rand(size);
			nums.push(n);
			sum += n;
			return n;
		},
	});

	Object.defineProperty(o, 'sum', {
		get: function() {
			return sum;
		},
	});

	Object.defineProperty(o, 'numbers', {
		get: function() {
			return nums.slice();
		},
	});

	return o;
};


var MultCifras = function(m1_size, m2_size) {
	var ms1 = m1_size || 2;
	var ms2 = m2_size || 2;
	var a;
	var b;
	var o = {};

	Object.defineProperty(o, 'operand_a', {
		get: function() { return a = Digits.rand(ms1); },
	});

	Object.defineProperty(o, 'operand_b', {
		get: function() { return b = Digits.rand(ms2); },
	});

	Object.defineProperty(o, 'result', {
		get: function() { return a*b; },
	});

	Object.defineProperty(o, 'operation', {
		get: function() { return 'x'; },
	});

	return o;
};


var Loader = [];


window.addEventListener('load', function() {
	var games = document.getElementsByClassName('game');
	for (var i = 0; i < games.length; i++) {
		if (Loader[games[i].id]) {
			Loader[games[i].id](games[i]);
			console.log('loaded', games[i].id);
		}
	}
});


Loader['sumas-consec'] = function(article) {
	var screen = new CalcScreen(article);
	var delay = 50;


	var screen_menu = (function() {
		var h1;
		var digits;
		var speed;
		var size;
		var start;
		var o = {};

		var start_countdown = function() {
			var _digits = parseInt(digits.select.value);
			var _speed = parseInt(speed.select.value);
			var _size = parseInt(size.select.value);
			var sumas_consec = new SumasConsec(_digits);
			screen.start('countdown', {
				digits: _digits,
				speed: _speed,
				size: _size,
				play: sumas_consec,
			});
		};

		o.onpreunload = function() {
			digits.select.disabled = true;
			speed.select.disabled = true;
			size.select.disabled = true;
			start.disabled = true;
		};

		o.onpreload = function() {
			digits.select.disabled = false;
			speed.select.disabled = false;
			size.select.disabled = false;
			start.disabled = false;
		};

		o.oncreate = function(sec) {
			sec.className = 'menu';
			h1 = document.createElement('h1');
			h1.innerHTML = 'Sumas consecutivas';

			digits = create_select({
				label: 'Cifras',
				options: [
					{value: 1, text: '1 cifra'},
					{value: 2, text: '2 cifras'},
				],
			});

			speed = create_select({
				label: 'Velocidad',
				options: [
					{value: 300, text: '0.3 segundos'},
					{value: 500, text: '0.5 segundos'},
					{value: 700, text: '0.7 segundos'},
					{value: 1000, text: '1 segundo'},
					{value: 1500, text: '1.5 segundos'},
					{value: 2000, text: '2 segundos'},
				],
				selected: '1 segundo',
			});

			size = create_select({
				label: 'Cantidad',
				options: [
					{value: 5, text: '5 números'},
					{value: 10, text: '10 números'},
					{value: 15, text: '15 números'},
					{value: 20, text: '20 números'},
					{value: 30, text: '30 números'},
					{value: 50, text: '50 números'},
					{value: 100, text: '100 números'},
				],
				selected: '10 números',
			});

			start = document.createElement('button');
			start.innerHTML = 'Comenzar';
			start.addEventListener('click', start_countdown);

			sec.appendChild(h1);
			sec.appendChild(digits.label);
			sec.appendChild(speed.label);
			sec.appendChild(size.label);
			sec.appendChild(start);
		};

		return o;
	})();
	screen.add_view('menu', screen_menu);


	var screen_countdown = (function() {
		var timeleft;
		var cancel;
		var params;
		var count = 5;
		var canceled = false;
		var timestart = 0;
		var o = {};

		var ontick = function() {
			if (canceled)
				return;
			if (!timestart)
				timestart = performance.now();
			var diff = -timestart + performance.now();
			var diff_sec = Math.floor(diff/1000);
			var left_sec = -diff_sec + count;

			if (!left_sec) {
				screen.timer.progress = 1.0;
				screen.start('game', params);
				return;
			}

			timeleft.innerHTML = left_sec;
			screen.timer.progress = diff/(count*1000);
			setTimeout(ontick, delay);
		};

		var oncancel = function() {
			canceled = true;
			var l = screen.timer.progress;
			screen.start('menu', {}, {
				onprogress: function(progress) {
					screen.timer.progress =
						l*(1 - progress);
				},
				delay: delay,
			});
		};

		o.onload = function(params_) {
			params = params_;
			canceled = false;
			timestart = 0;
			screen.timer.total = params.speed*params.size;
			screen.timer.progress = 0.0;
			setTimeout(ontick, delay);
		};

		o.oncreate = function(sec) {
			sec.className = 'countdown';
			timeleft = document.createElement('output');
			timeleft.className = 'time';
			cancel = document.createElement('button');
			cancel.className = 'cancel';
			cancel.innerHTML = 'Cancelar';
			cancel.addEventListener('click', oncancel);
			sec.appendChild(timeleft);
			sec.appendChild(cancel);
		};

		o.onpreload = function() {
			timeleft.value = count;
			cancel.disabled = false;
		};

		o.onpreunload = function() {
			cancel.disabled = true;
		};

		return o;
	})();
	screen.add_view('countdown', screen_countdown);


	var screen_game = (function() {
		var number;
		var params;
		var left = 0;
		var timestart = 0;
		var timecorrection = 0;
		var o = {};

		var ontick = function() {
			if (!left) {
				screen.timer.progress = 0.0;
				screen.start('verify', params, {
					onprogress: function(p) {
						screen.timer.progress = p;
					},
					time: 700,
					delay: delay,
				});
				return;
			}
			if (!timestart) {
				timestart = performance.now();
				timestart += timecorrection;
				number.className = left%2 ? 'odd':'even';
				number.value = params.play.next;
			}
			var diff = -timestart + performance.now();
			var l = left - (diff/params.speed);
			if (0 <= l/params.size)
				screen.timer.progress = l/params.size;
			if (params.speed <= diff) {
				timestart = 0;
				timecorrection = -diff + params.speed;
				left--;
			}
			setTimeout(ontick, delay);
		};

		o.oncreate = function(sec) {
			sec.className = 'gameplay';
			number = document.createElement('output');
			sec.appendChild(number);
		};

		o.onpreload = function() {
			number.className = '';
			number.disabled = false;
			number.value = 'GO';
		};

		o.onpreunload = function() {
			number.disabled = true;
		};

		o.onload = function(params_) {
			params = params_;
			screen.timer.total = params.speed*params.size;
			screen.timer.progress = 1.0;
			left = params.size;
			timestart = 0;
			timecorrection = 0;
			setTimeout(ontick, 500);
		};

		return o;
	})();
	screen.add_view('game', screen_game);


	var screen_verify = (function() {
		var input;
		var output;
		var output_list;
		var verify;
		var back;
		var params;
		var o = {};

		var oncheck = function() {
			var sum = parseInt(input.value);
			if (params.play.sum !== sum) {
				output.className = 'bad';
				output.value = sum + ' != ' + params.play.sum;
			} else {
				output.className = 'good';
				output.value = sum + ' = ' + params.play.sum;
			}
			output_list.innerHTML = '';
			params.play.numbers.forEach(function(e) {
				if ('' == output_list.innerHTML)
					output_list.innerHTML = e;
				else
					output_list.innerHTML += ' + ' + e;
			});
			output_list.innerHTML += ' = ' + params.play.sum;
			input.disabled = true;
			verify.disabled = true;
			back.disabled = false;
		};

		var onback = function() {
			screen.start('menu');
		};

		var onkeypress = function(e) {
			var k = e.which || e.keyCode;
			if (13 == k)
				oncheck();
		};

		o.oncreate = function(sec) {
			sec.className = 'verify';
			input = document.createElement('input');
			input.addEventListener('keypress', onkeypress);
			input.placeholder = 'Suma';
			output = document.createElement('output');
			output_list = document.createElement('output');
			verify = document.createElement('button');
			verify.innerHTML = 'Comprobar';
			verify.addEventListener('click', oncheck);
			back = document.createElement('button');
			back.innerHTML = 'Continuar';
			back.addEventListener('click', onback);
			sec.appendChild(input);
			sec.appendChild(output);
			sec.appendChild(output_list);
			sec.appendChild(verify);
			sec.appendChild(back);
		};

		o.onpreload = function() {
			input.value = '';
			output.value = '';
			output_list.value = '';
			input.disabled = true;
			verify.disabled = true;
			back.disabled = true;
		};

		o.onpreunload = function() {
			input.disabled = true;
			verify.disabled = true;
			back.disabled = true;
		};

		o.onload = function(params_) {
			params = params_;
			input.disabled = false;
			verify.disabled = false;
			back.disabled = true;
			input.focus();
		};

		return o;
	})();
	screen.add_view('verify', screen_verify);


	screen.start('menu');
};


Loader['mult-mxn-cifras'] = function(article) {
	var screen = new CalcScreen(article);
	var delay = 50;
	var total_time = 60000;


	var screen_menu = (function() {
		var h1;
		var m1;
		var m2;
		var start;
		var o = {};

		var start_countdown = function() {
			var _m1 = parseInt(m1.select.value);
			var _m2 = parseInt(m2.select.value);
			var mult_mxn_cifras = new MultCifras(_m1, _m2);
			screen.start('game', {
				m1: _m1,
				m2: _m2,
				play: mult_mxn_cifras,
			});
		};

		o.onpreunload = function() {
			m1.select.disabled = true;
			m2.select.disabled = true;
			start.disabled = true;
		};

		o.onpreload = function() {
			m1.select.disabled = false;
			m2.select.disabled = false;
			start.disabled = false;
		};

		o.oncreate = function(sec) {
			sec.className = 'menu';
			h1 = document.createElement('h1');
			h1.innerHTML = 'Multiplicación cruzada';

			m1 = create_select({
				label: 'Multiplicando',
				options: [
					{value: 2, text: '2 cifras'},
					{value: 3, text: '3 cifras'},
					{value: 4, text: '4 cifras'},
					{value: 5, text: '5 cifras'},
					{value: 6, text: '6 cifras'},
				],
			});

			m2 = create_select({
				label: 'Multiplicador',
				options: [
					{value: 2, text: '2 cifras'},
					{value: 3, text: '3 cifras'},
					{value: 4, text: '4 cifras'},
					{value: 5, text: '5 cifras'},
					{value: 6, text: '6 cifras'},
				],
			});

			start = document.createElement('button');
			start.innerHTML = 'Comenzar';
			start.addEventListener('click', start_countdown);

			sec.appendChild(h1);
			sec.appendChild(m1.label);
			sec.appendChild(m2.label);
			sec.appendChild(start);
		};

		return o;
	})();
	screen.add_view('menu', screen_menu);


	var screen_game = (function() {
		var calc_input;
		var verify;
		var da;
		var db;

		var timeleft;
		var cancel;

		var params;
		var count = 5;
		var canceled = false;
		var timestart = 0;

		var terminated;
		var good_ones;
		var bad_ones;
		var o = {};

		var onnext = function() {
			calc_input.operand_a = params.play.operand_a;
			calc_input.operand_b = params.play.operand_b;
			calc_input.operation = params.play.operation;
			calc_input.result.disabled = false;
			calc_input.result.focus();
			calc_input.stat.max = good_ones + bad_ones;
			calc_input.stat.value = good_ones;
		};

		var ontick = function() {
			if (!timestart)
				timestart = performance.now();
			var diff = -timestart + performance.now();

			if (screen.timer.total < diff) {
				terminated = true;
				screen.timer.progress = 0.0;
				screen.start('stat', {
					points: good_ones,
					total: good_ones + bad_ones,
				}, {
					onprogress: function(p) {
						screen.timer.progress = p;
					},
				});
				return;
			}

			screen.timer.progress = 1 - diff/screen.timer.total;
			setTimeout(ontick, delay);
		};

		var onverify = function() {
			if (terminated)
				return;
			var r = parseInt(calc_input.result.value);
			if (r !== params.play.result) {
				calc_input.result.className = 'bad';
				bad_ones++;
			} else {
				calc_input.result.className = 'good';
				good_ones++;
			}
			calc_input.result.value = '';
			onnext();
		};

		var onkeypress = function(e) {
			var k = e.which || e.keyCode;
			if (13 == k)
				onverify();
		};

		var onplay = function() {
			verify.disabled = false;
			terminated = false;
			timestart = 0;
			good_ones = 0;
			bad_ones = 0;
			onnext();
			setTimeout(ontick, delay);
		};

		var oncountdown = function() {
			if (canceled)
				return;
			if (!timestart)
				timestart = performance.now();
			var diff = -timestart + performance.now();
			var diff_sec = Math.floor(diff/1000);
			var left_sec = -diff_sec + count;

			if (!left_sec) {
				screen.timer.progress = 1.0;
				timeleft.innerHTML = 'GO';
				setTimeout(function() {
					db.style.display = 'none';
					onplay();
				}, 500);
				return;
			}

			timeleft.innerHTML = left_sec;
			screen.timer.progress = diff/(count*1000);
			setTimeout(oncountdown, delay);
		};

		var oncancel = function() {
			canceled = true;
			var l = screen.timer.progress;
			screen.start('menu', {}, {
				onprogress: function(progress) {
					screen.timer.progress =
						l*(1 - progress);
				},
				delay: delay,
			});
		};

		o.onload = function(params_) {
			params = params_;
			canceled = false;
			timestart = 0;
			da.style.display = 'block';
			db.style.display = 'block';
			screen.timer.total = total_time;
			screen.timer.progress = 0.0;
			setTimeout(oncountdown, 300);
		};

		o.oncreate = function(sec) {
			sec.className = 'gameplay';
			da = document.createElement('div');
			db = document.createElement('div');
				db.className = 'count';
			calc_input = new CalcInput(da);

			timeleft = document.createElement('output');
			timeleft.className = 'time';
			cancel = document.createElement('button');
			cancel.className = 'cancel';
			cancel.innerHTML = 'Cancelar';
			cancel.addEventListener('click', oncancel);
				db.className = 'countdown';
				db.appendChild(timeleft);
				db.appendChild(cancel);
				db.style.display = 'block';
				db.style.position = 'absolute';
				db.style.top = '0';
				db.style.left = '0';
				db.style.width = '100%';
				db.style.height = '100%';
				db.style.opacity = 0.8;

			verify = document.createElement('button');
			verify.innerHTML = 'Comprobar';
			verify.addEventListener('click', onverify);
			calc_input.result.addEventListener(
				'keypress', onkeypress);

			sec.appendChild(da);
			sec.appendChild(db);
			sec.appendChild(verify);
		};

		o.onpreload = function(params_) {
			params = params_;

			da.style.display = 'block';
			db.style.display = 'block';
			cancel.style.display = 'block';
			verify.style.display = 'block';

			calc_input.stat.text = 'Puntaje';
			calc_input.stat.max = 0;
			calc_input.stat.value = 0;
			calc_input.operand_a = Array(params.m1 + 1).join('?');
			calc_input.operand_b = Array(params.m2 + 1).join('?');
			calc_input.operation = 'x';
			calc_input.equal_sign = '=';
			calc_input.result.value = '';
			calc_input.result.disabled = true;
			verify.disabled = true;

			timeleft.value = count;
			cancel.disabled = false;
		};

		o.onpreunload = function() {
			da.style.display = 'none';
			db.style.display = 'none';
			cancel.style.display = 'none';
			verify.style.display = 'none';

			calc_input.result.disabled = true;
			verify.disabled = true;
			cancel.disabled = true;
		};

		return o;
	})();
	screen.add_view('game', screen_game);


	var screen_stat = (function() {
		var output;
		var back;
		var params;
		var o = {};

		var onback = function() {
			screen.start('menu');
		};

		o.oncreate = function(sec) {
			sec.className = 'gamestat';
			var l = document.createElement('label');
			var e = document.createElement('em');
				e.innerHTML = 'Puntaje';
			output = document.createElement('output');
				l.appendChild(e);
				l.appendChild(output);
			back = document.createElement('button');
			back.innerHTML = 'Continuar';
			back.addEventListener('click', onback);
			sec.appendChild(l);
			sec.appendChild(back);
		};

		o.onpreload = function(params) {
			output.value = params.points + '/' + params.total;
			back.disabled = true;
		};

		o.onpreunload = function() {
			back.disabled = true;
		};

		o.onload = function() {
			back.disabled = false;
		};

		return o;
	})();
	screen.add_view('stat', screen_stat);


	screen.start('menu');
};


function loader_with_player(article, player) {
	var screen = new CalcScreen(article);
	var delay = 50;
	var total_time = 60000;
	var play = player;


	var screen_menu = (function() {
		var h1;
		var start;
		var o = {};

		var start_countdown = function() {
			screen.start('game', {
				play: play,
			});
		};

		o.onpreunload = function() {
			start.disabled = true;
		};

		o.onpreload = function() {
			start.disabled = false;
		};

		o.oncreate = function(sec) {
			sec.className = 'menu';
			h1 = document.createElement('h1');
			h1.innerHTML = play.name;

			start = document.createElement('button');
			start.innerHTML = 'Comenzar';
			start.addEventListener('click', start_countdown);

			sec.appendChild(h1);
			sec.appendChild(start);
		};

		return o;
	})();
	screen.add_view('menu', screen_menu);


	var screen_game = (function() {
		var calc_input;
		var verify;
		var da;
		var db;

		var timeleft;
		var cancel;

		var params;
		var count = 5;
		var canceled = false;
		var timestart = 0;

		var terminated;
		var o = {};

		var onnext = function() {
			params.play.next();
			calc_input.result.disabled = false;
			calc_input.result.focus();
			calc_input.operand_a = params.play.operand_a;
			calc_input.operand_b = params.play.operand_b;
			calc_input.operation = params.play.operation;
			calc_input.stat.max = params.play.totalplays;
			calc_input.stat.value = params.play.points;
		};

		var ontick = function() {
			if (!timestart)
				timestart = performance.now();
			var diff = -timestart + performance.now();

			if (screen.timer.total < diff) {
				terminated = true;
				screen.timer.progress = 0.0;
				screen.start('stat', params.play, {
					onprogress: function(p) {
						screen.timer.progress = p;
					},
				});
				return;
			}

			screen.timer.progress = 1 - diff/screen.timer.total;
			setTimeout(ontick, delay);
		};

		var onverify = function() {
			if (terminated)
				return;
			var valid;
			if (typeof params.play.validate == 'function')
				valid = params.play.validate(
					calc_input.result.value);
			else
				valid = parseInt(calc_input.result.value) ===
					params.play.result;
			if (!valid) {
				params.play.fail();
				calc_input.result.className = 'bad';
				if (params.play.cleanonerror)
					calc_input.result.value = '';
			} else {
				params.play.succeed();
				calc_input.result.className = 'good';
				calc_input.result.value = '';
			}
			onnext();
		};

		var onkeypress = function(e) {
			var k = e.which || e.keyCode;
			if (13 == k)
				onverify();
		};

		var onplay = function() {
			verify.disabled = false;
			terminated = false;
			timestart = 0;
			params.play.notify('gamestart');
			onnext();
			setTimeout(ontick, delay);
		};

		var oncountdown = function() {
			if (canceled)
				return;
			if (!timestart)
				timestart = performance.now();
			var diff = -timestart + performance.now();
			var diff_sec = Math.floor(diff/1000);
			var left_sec = -diff_sec + count;

			if (!left_sec) {
				screen.timer.progress = 1.0;
				timeleft.innerHTML = 'GO';
				setTimeout(function() {
					db.style.display = 'none';
					onplay();
				}, 500);
				return;
			}

			timeleft.innerHTML = left_sec;
			screen.timer.progress = diff/(count*1000);
			setTimeout(oncountdown, delay);
		};

		var oncancel = function() {
			canceled = true;
			var l = screen.timer.progress;
			screen.start('menu', {}, {
				onprogress: function(progress) {
					screen.timer.progress =
						l*(1 - progress);
				},
				delay: delay,
			});
		};

		o.onload = function(params_) {
			params = params_;
			canceled = false;
			timestart = 0;
			da.style.display = 'block';
			db.style.display = 'block';
			screen.timer.total = total_time;
			screen.timer.progress = 0.0;
			setTimeout(oncountdown, 300);
		};

		o.oncreate = function(sec) {
			sec.className = 'gameplay';
			da = document.createElement('div');
			db = document.createElement('div');
				db.className = 'count';
			calc_input = new CalcInput(da);

			timeleft = document.createElement('output');
			timeleft.className = 'time';
			cancel = document.createElement('button');
			cancel.className = 'cancel';
			cancel.innerHTML = 'Cancelar';
			cancel.addEventListener('click', oncancel);
				db.className = 'countdown';
				db.appendChild(timeleft);
				db.appendChild(cancel);
				db.style.display = 'block';
				db.style.position = 'absolute';
				db.style.top = '0';
				db.style.left = '0';
				db.style.width = '100%';
				db.style.height = '100%';
				db.style.opacity = 0.8;

			verify = document.createElement('button');
			verify.innerHTML = 'Comprobar';
			verify.addEventListener('click', onverify);
			calc_input.result.addEventListener(
				'keypress', onkeypress);

			sec.appendChild(da);
			sec.appendChild(db);
			sec.appendChild(verify);
		};

		o.onpreload = function(params_) {
			params = params_;

			da.style.display = 'block';
			db.style.display = 'block';
			cancel.style.display = 'block';
			verify.style.display = 'block';

			calc_input.stat.text = 'Puntaje';
			calc_input.stat.max = 0;
			calc_input.stat.value = 0;
			calc_input.operand_a = params.play.operand_a_fill;
			calc_input.operand_b = params.play.operand_b_fill;
			calc_input.operation = params.play.operation;
			calc_input.equal_sign = '=';
			calc_input.result.value = '';
			calc_input.result.disabled = true;
			verify.disabled = true;

			timeleft.value = count;
			cancel.disabled = false;
		};

		o.onpreunload = function() {
			da.style.display = 'none';
			db.style.display = 'none';
			cancel.style.display = 'none';
			verify.style.display = 'none';

			calc_input.result.disabled = true;
			verify.disabled = true;
			cancel.disabled = true;
		};

		return o;
	})();
	screen.add_view('game', screen_game);


	var screen_stat = (function() {
		var output;
		var back;
		var params;
		var o = {};

		var onback = function() {
			screen.start('menu');
		};

		o.oncreate = function(sec) {
			sec.className = 'gamestat';
			var l = document.createElement('label');
			var e = document.createElement('em');
				e.innerHTML = 'Puntaje';
			output = document.createElement('output');
				l.appendChild(e);
				l.appendChild(output);
			back = document.createElement('button');
			back.innerHTML = 'Continuar';
			back.addEventListener('click', onback);
			sec.appendChild(l);
			sec.appendChild(back);
		};

		o.onpreload = function(params) {
			output.value =
				play.points + '/' +
				play.totalplays;
			back.disabled = true;
			play.notify('gameover');
		};

		o.onpreunload = function() {
			back.disabled = true;
		};

		o.onload = function() {
			back.disabled = false;
		};

		return o;
	})();
	screen.add_view('stat', screen_stat);


	screen.start('menu');
}


Loader['sumas-2-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;
		var op = '+';

		var reset = function() {
			points = 0;
			totalplays = 0;
		};

		var o = {};

		o.name = 'Sumar números de 2 cifras';
		o.operand_a_fill = '??';
		o.operand_b_fill = '??';
		o.operation = '+';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(2);
			b = Digits.rand(2);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a + b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['sumas-3-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;
		var op = '+';

		var reset = function() {
			points = 0;
			totalplays = 0;
		};

		var o = {};

		o.name = 'Sumar números de 3 cifras';
		o.operand_a_fill = '???';
		o.operand_b_fill = '???';
		o.operation = '+';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(3);
			b = Digits.rand(3);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a + b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['series-fib'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;
		var op = '+';
		var failed;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = 0;
			b = 1;
			failed = true;
		};

		var o = {};

		o.name = 'Series de fibonacci';
		o.operand_a_fill = '0';
		o.operand_b_fill = '1';
		o.operation = '+';

		o.next = function() {
			if (failed)
				return;
			var t = a;
			a = b;
			b = t + b;
		};

		o.fail = function() {
			failed = true;
			totalplays++;
		};

		o.succeed = function() {
			failed = false;
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a + b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['series-fib-ext'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;
		var op = '+';
		var failed;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(2);
			b = Digits.rand(2);
			if (b < a) {
				var t = a;
				a = b;
				b = t;
			}
			failed = true;
		};

		var o = {};

		o.name = 'Otras series de fibonacci';
		o.operand_a_fill = '0';
		o.operand_b_fill = '1';
		o.operation = '+';

		o.next = function() {
			if (failed)
				return;
			var t = a;
			a = b;
			b = t + b;
		};

		o.fail = function() {
			failed = true;
			totalplays++;
		};

		o.succeed = function() {
			failed = false;
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a + b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['doblar-numeros'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var op = '+';
		var failed;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(2);
			failed = true;
		};

		var o = {};

		o.name = 'Doblar números';
		o.operand_a_fill = '??';
		o.operand_b_fill = '??';
		o.operation = '+';

		o.next = function() {
			if (failed)
				return;
			a *= 2;
		};

		o.fail = function() {
			failed = true;
			totalplays++;
		};

		o.succeed = function() {
			failed = false;
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*2;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['resta-2-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(2);
			b = Digits.rand(2);
			if (a < b) {
				var t = a;
				a = b;
				b = t;
			}
		};

		var o = {};

		o.name = 'Restar números de 2 cifras';
		o.operand_a_fill = '??';
		o.operand_b_fill = '??';
		o.operation = '-';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(2);
			b = Digits.rand(2);
			if (a < b) {
				var t = a;
				a = b;
				b = t;
			}
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a - b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['resta-3-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(3);
			b = Digits.rand(3);
			if (a < b) {
				var t = a;
				a = b;
				b = t;
			}
		};

		var o = {};

		o.name = 'Restar números de 3 cifras';
		o.operand_a_fill = '???';
		o.operand_b_fill = '???';
		o.operation = '-';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(3);
			b = Digits.rand(3);
			if (a < b) {
				var t = a;
				a = b;
				b = t;
			}
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a - b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['comp-100'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var b;

		var reset = function() {
			points = 0;
			totalplays = 0;
			b = Digits.rand(2);
		};

		var o = {};

		o.name = 'Complemento a 100';
		o.operand_a_fill = '100';
		o.operand_b_fill = '??';
		o.operation = '-';
		o.cleanonerror = true;

		o.next = function() {
			b = Digits.rand(2);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return 100;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return 100 - b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['mult-2x1-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(2);
			b = Digits.rand(1);
		};

		var o = {};

		o.name = 'Multiplicaciones de 2 cifras por 1 cifra';
		o.operand_a_fill = '??';
		o.operand_b_fill = '?';
		o.operation = 'x';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(2);
			b = Digits.rand(1);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['mult-3x1-cifras'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(3);
			b = Digits.rand(1);
		};

		var o = {};

		o.name = 'Multiplicaciones de 3 cifras por 1 cifra';
		o.operand_a_fill = '???';
		o.operand_b_fill = '?';
		o.operation = 'x';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(3);
			b = Digits.rand(1);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return b;
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['conv-moneda'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;
		var b = 20.71;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(3);
		};

		var o = {};

		o.name = 'Euros a pesos mexicanos';
		o.operand_a_fill = '???';
		o.operand_b_fill = 'aMXN';
		o.operation = 'c';
		o.cleanonerror = true;

		o.validate = function(v) {
			var r = Math.round(parseFloat(v)*100)/100;
			var s = Math.round(a*b*100)/100;
			return r == s;
		};

		o.next = function() {
			a = Digits.rand(3);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return 'aMXN';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*b;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['div-entre-0.5'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(3);
		};

		var o = {};

		o.name = 'Dividir entre 0.5';
		o.operand_a_fill = '???';
		o.operand_b_fill = '0.5';
		o.operation = '÷';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(3);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return '0.5';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*2;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['div-entre-0.25'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;

		var reset = function() {
			points = 0;
			totalplays = 0;
			a = Digits.rand(3);
		};

		var o = {};

		o.name = 'Dividir entre 0.25';
		o.operand_a_fill = '???';
		o.operand_b_fill = '0.25';
		o.operation = '÷';
		o.cleanonerror = true;

		o.next = function() {
			a = Digits.rand(3);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return '0.25';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a*4;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['div-entre-15'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;

		var reset = function() {
			points = 0;
			totalplays = 0;
			do {
				a = Digits.rand(3);
			} while (a%15);
		};

		var o = {};

		o.name = 'Dividir entre 15';
		o.operand_a_fill = '???';
		o.operand_b_fill = '15';
		o.operation = '÷';
		o.cleanonerror = true;

		o.next = function() {
			do {
				a = Digits.rand(3);
			} while (a%15);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return '15';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a/15;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['div-entre-25'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;

		var reset = function() {
			points = 0;
			totalplays = 0;
			do {
				a = Digits.rand(3);
			} while (a%25);
		};

		var o = {};

		o.name = 'Dividir entre 25';
		o.operand_a_fill = '???';
		o.operand_b_fill = '25';
		o.operation = '÷';
		o.cleanonerror = true;

		o.next = function() {
			do {
				a = Digits.rand(3);
			} while (a%25);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return '25';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a/25;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};


Loader['div-entre-5'] = function(article) {
	var player = function() {
		var points = 0;
		var totalplays = 0;
		var a;

		var reset = function() {
			points = 0;
			totalplays = 0;
			do {
				a = Digits.rand(3);
			} while (a%5);
		};

		var o = {};

		o.name = 'Dividir entre 5';
		o.operand_a_fill = '???';
		o.operand_b_fill = '5';
		o.operation = '÷';
		o.cleanonerror = true;

		o.next = function() {
			do {
				a = Digits.rand(3);
			} while (a%5);
		};

		o.fail = function() {
			totalplays++;
		};

		o.succeed = function() {
			points++;
			totalplays++;
		};

		o.notify = function(m) {
			if ('gamestart' == m) {
				reset();
				console.log('gamestart');
			} else if ('gameover') {
				console.log('points', points, '/', totalplays);
			}
		};

		Object.defineProperty(o, 'operand_a', {
			get: function() {
				return a;
			},
		});

		Object.defineProperty(o, 'operand_b', {
			get: function() {
				return '5';
			},
		});

		Object.defineProperty(o, 'result', {
			get: function() {
				return a/5;
			},
		});

		Object.defineProperty(o, 'points', {
			get: function() {
				return points;
			},
		});

		Object.defineProperty(o, 'totalplays', {
			get: function() {
				return totalplays;
			},
		});

		return o;
	};

	loader_with_player(article, player());
};
