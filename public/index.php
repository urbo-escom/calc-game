<?php

require_once(__DIR__.'/../conf/config.php');

use \request\parser;
use \request\router;


class SessionException extends DomainException {
}
class LoginRequiredException extends SessionException {
	public function __construct() {
		parent::__construct(
		"Se necesita iniciar sesión para entrar aquí");
	}
}
class AdminRequiredException extends SessionException {
	public function __construct() {
		parent::__construct(
		"Se necesita sesión como Admin para entrar aquí");
	}
}
class AnonRequiredException extends SessionException {
	public function __construct() {
		parent::__construct(
		"Ya has iniciado sesión y no puedes entrar aquí");
	}
}


$req = new parser([
	'header' => apache_request_headers(),
	'server' => $_SERVER,
	'get' => $_GET,
	'post' => $_POST,
	'files' => $_FILES,
	'input' => 'php://input',
]);
$router = new router();


foreach (glob(APP_ROOT."/routes/*.php") as $file) {
	include $file;
}


try {
	$routed = $router->route([
		'method' => $req->method,
		'url'    => $req->url,
		'query'  => $req->query,
		'input'  => $req->input,
		'output' => $req->output,
	]);

	if (!$routed) {
		$view = $CONFIG['injector']->get("view.{$req->output}");
		$view->id('not-found-error');
		$view->status(404);
		$view->add_error("La url '{$req->url}' no fue encontrada");
		$view->render();
	}
} catch(SessionException $e) {
	$view = $CONFIG['injector']->get("view.{$req->output}");
	$view->id('session-error');
	$view->status(403);
	$view->add_error($e->getMessage());
	$view->render();
}
