<?php

$router->add_route('GET', '/', function($_, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];

	$session = $di->get('session');
	$view = $di->get("view.{$req['output']}");

	if ($session->is_anon()) {
		$view->redirect(303,
			'/registro');
	} else if ($session->is_user()) {
		$view->redirect(303,
			'/alumnos/'.$session->get('session.user.id'));
	} else if ($session->is_admin()) {
		$view->redirect(303,
			'/admin');
	}
});


$router->add_route('GET', '/admin', function($_, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$c = $CONFIG['injector']->get('view.admin.home');
	$c->get($req);
});


$router->add_route('GET', '/alumnos/([0-9]+)', function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if ($session->is_admin()) {
		$di->get('view.html')
			->redirect(303, '/admin/alumno/' + $m[1]);
	}
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$crepo = $CONFIG['injector']->get('repo.codigo');
	$erepo = $CONFIG['injector']->get('repo.escuela');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.home');
	$v->get($alumno,
		$crepo->get_by_id($alumno->get_codigo_id()),
		$erepo->get_by_id($alumno->get_escuela_id()));
});
