<?php

$router->add_route('GET', '/codigos', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();
	$session->close();

	if ('html' === $req['output']) {
		$v = $CONFIG['injector']->get('view.codigo');
		$v->render();
	}

	$view = $CONFIG['injector']->get('view.json');
	$repo = $CONFIG['injector']->get('repo.codigo');
	$arepo = $CONFIG['injector']->get('repo.alumno');

	$offset = 0;
	$limit = 3;
	$usado = null;
	$like = null;

	$in = $req['query'];
	if (isset($in['offset'])) $offset = $in['offset'];
	if (isset($in['limit']))  $limit = $in['limit'];
	if (isset($in['usado']))  $usado = $in['usado'];
	if (isset($in['like']))   $like = $in['like'];

	$query = \codigo\repo_query::builder()
		->offset($offset)
		->limit($limit)
		->usado($usado)
		->like($like)
		->build();
	$col = $repo->get_all($query);

	$items = [];
	foreach ($col->get_items() as $item) {
		$alumno_id = null;
		if (null !== $item->get_email())
			$alumno_id = $arepo->get($item->get_email())->get_id();
		$items[] = [
			'id' => $item->get_id(),
			'codigo' => $item->get_codigo(),
			'alumno_id' => $alumno_id,
			'alumno_email' => $item->get_email(),
		];
	}

	$url = new \request\url('/codigos');
	$links = $url->paginated_links($col->get_total(), [
		'offset' => $offset,
		'limit' => $limit,
		'usado' => $usado,
		'like' => $like,
	]);

	$view->response([
		'links' => $links,
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => $items,
	]);
	$view->render();
});

$router->add_route('POST', '/codigos', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();
	$session->close();

	$view = $CONFIG['injector']->get('view.json');
	$reg = $CONFIG['injector']->get('registro.codigo');

	if (!isset($req['input']['codigo']))
		$cod = '';
	else
		$cod = $req['input']['codigo'];

	try {
		$codigo = $reg->agregar($cod);
		$view->status(201);
		$view->response([
			'id' => $codigo->get_id(),
			'codigo' => $codigo->get_codigo(),
			'email' => $codigo->get_email(),
		]);
	} catch(DomainException $e) {
		$view->status(400);
		$view->add_error($e->getMessage());
	}
	$view->render();
});
