<?php

$router->add_route('GET', '/alumnos/([0-9]+)/entrenamiento',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$jrepo = $CONFIG['injector']->get('repo.juego');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.entrenamiento');
	$v->get($alumno, $jrepo->get_all());
});


$router->add_route('GET',
		'/alumnos/([0-9]+)/entrenamiento/([a-z0-9-_]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$jrepo = $CONFIG['injector']->get('repo.juego');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	if (null === ($juego = $jrepo->get($m[2])))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.entrenamiento.juego');
	$v->get($alumno, $juego);
});
