<?php

$router->add_route('GET',
		'/admin/alumnos/([0-9]+)/evaluaciones/([0-9]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$esc_repo = $CONFIG['injector']->get('repo.escuela');
	$arepo = $CONFIG['injector']->get('repo.alumno');
	$erepo = $CONFIG['injector']->get('repo.evaluacion');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$escuela = $esc_repo->get_by_id($alumno->get_escuela_id());

	if (null === ($evaluacion = $erepo->get_by_id(intval($m[2]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.alumno.evaluacion');
	$v->get($escuela, $alumno, $evaluacion);
});

$router->add_route('GET',
		'/reporte/alumnos/([0-9]+)/evaluaciones/([0-9]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new AdminRequiredException();

	$esc_repo = $CONFIG['injector']->get('repo.escuela');
	$arepo = $CONFIG['injector']->get('repo.alumno');
	$erepo = $CONFIG['injector']->get('repo.evaluacion');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$escuela = $esc_repo->get_by_id($alumno->get_escuela_id());

	if (null === ($evaluacion = $erepo->get_by_id(intval($m[2]))))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.evaluacion.reporte');
	$v->get($escuela, $alumno, $evaluacion);
});

$router->add_route('GET', '/admin/escuelas/([0-9]+)', function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$erepo = $CONFIG['injector']->get('repo.escuela');

	if (null === ($escuela = $erepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.escuela.nivel_list');
	$v->get($escuela);
});

$router->add_route('GET',
		'/admin/escuelas/([0-9]+)/(secundaria|preparatoria)([1-6])',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$erepo = $CONFIG['injector']->get('repo.escuela');

	if (null === ($escuela = $erepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.escuela.grupo_list');
	$v->get($escuela, $m[2], $m[3]);
});

$router->add_route('GET',
		'/admin/escuelas/([0-9]+)/'.
		'(secundaria|preparatoria)([1-6])([a-f])',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$erepo = $CONFIG['injector']->get('repo.escuela');

	if (null === ($escuela = $erepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.escuela.grupo');
	$v->get($escuela, $m[2], $m[3], $m[4]);
});

$router->add_route('GET',
		'/admin/escuelas/([0-9]+)'.
		'/(secundaria|preparatoria)([1-6])([a-f])'.
		'/evaluaciones/([0-9]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$esc_repo = $CONFIG['injector']->get('repo.escuela');
	$eva_repo = $CONFIG['injector']->get('repo.evaluacion');

	if (null === ($escuela = $esc_repo->get_by_id(intval($m[1]))))
		return false;

	if (null === ($evaluacion = $eva_repo->get_by_id(intval($m[5]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.escuela.evaluacion');
	$v->get($escuela, $evaluacion, $m[2], $m[3], $m[4]);
});
