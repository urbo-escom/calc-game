<?php

$router->add_route('GET', '/registro', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_anon())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.registro.controller');
	$c->get($req);
});


$router->add_route('POST', '/registro', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_anon())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.registro.controller');
	$c->post($req);
});
