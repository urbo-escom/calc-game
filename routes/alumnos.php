<?php

$router->add_route('GET', '/admin/alumnos', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	if ('html' === $req['output']) {
		$v = $CONFIG['injector']->get('view.admin.alumno_list');
		$v->render();
	}

	$view = $CONFIG['injector']->get('view.json');
	$repo = $CONFIG['injector']->get('repo.alumno');
	$crepo = $CONFIG['injector']->get('repo.codigo');
	$erepo = $CONFIG['injector']->get('repo.escuela');

	$offset = 0;
	$limit = 3;
	$like = null;

	$in = $req['query'];
	if (isset($in['offset'])) $offset = $in['offset'];
	if (isset($in['limit']))  $limit = $in['limit'];
	if (isset($in['like']))   $like = $in['like'];

	$query = \alumno\repo_query::builder()
		->offset($offset)
		->limit($limit)
		->like($like)
		->build();
	$col = $repo->get_all($query);

	$items = [];
	foreach ($col->get_items() as $item) {
		$datos = $item->get_datos();
		$items[] = [
			'id' => $item->get_id(),
			'codigo_id' => $item->get_codigo_id(),
			'escuela_id' => $item->get_escuela_id(),
			'codigo' =>
				$crepo->get_by_id($item->get_codigo_id())
					->get_codigo(),
			'escuela' =>
				$erepo->get_by_id($item->get_escuela_id())
					->get_nombre(),
			'nombre' => $datos->get_nombre_completo(),
			'email' => $datos->get_email(),
		];
	}

	$url = new \request\url('/admin/alumnos');
	$links = $url->paginated_links($col->get_total(), [
		'offset' => $offset,
		'limit' => $limit,
		'like' => $like,
	]);

	$view->response([
		'links' => $links,
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => $items,
	]);
	$view->render();
});

$router->add_route('GET', '/admin/alumnos/([0-9]+)', function($m, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$view = $CONFIG['injector']->get('view.json');
	$arepo = $CONFIG['injector']->get('repo.alumno');
	$crepo = $CONFIG['injector']->get('repo.codigo');
	$erepo = $CONFIG['injector']->get('repo.escuela');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.admin.alumno');
	$v->get($alumno,
		$crepo->get_by_id($alumno->get_codigo_id()),
		$erepo->get_by_id($alumno->get_escuela_id()));
});


$router->add_route('GET', '/alumnos/([0-9]+)/evaluaciones',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$ereg = $CONFIG['injector']->get('registro.evaluacion');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.evaluacion_list');
	$v->get($alumno,
		$ereg->obtener_pasadas(),
		$ereg->obtener_actuales(),
		$ereg->obtener_por_venir());
});


$router->add_route('GET', '/alumnos/([0-9]+)/evaluaciones/([0-9]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$erepo = $CONFIG['injector']->get('repo.evaluacion');
	$jrepo = $CONFIG['injector']->get('repo.juego');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	if (null === ($evaluacion = $erepo->get_by_id(intval($m[2]))))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.evaluacion');
	$v->get($alumno, $evaluacion, $jrepo->get_all());
});


$router->add_route('GET',
		'/alumnos/([0-9]+)/evaluaciones/([0-9]+)/([a-z0-9-_]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	$arepo = $CONFIG['injector']->get('repo.alumno');
	$erepo = $CONFIG['injector']->get('repo.evaluacion');
	$jrepo = $CONFIG['injector']->get('repo.juego');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	if (null === ($evaluacion = $erepo->get_by_id(intval($m[2]))))
		return false;

	if (null === ($juego = $jrepo->get($m[3])))
		return false;

	$v = $CONFIG['injector']->get('view.alumno.evaluacion.juego');
	$v->get($alumno, $evaluacion, $juego);
});


$router->add_route('POST',
		'/alumnos/([0-9]+)/evaluaciones/([0-9]+)/([a-z0-9-_]+)',
		function($m, $req) {
	global $CONFIG;
	$di = $CONFIG['injector'];
	$session = $di->get('session');
	if (!$session->is_user())
		throw new LoginRequiredException();
	if ($m[1] != $session->get('session.user.id'))
		return false;

	if (!isset($req['input']['aciertos']))
		$aciertos = '';
	else
		$aciertos = $req['input']['aciertos'];

	if (!isset($req['input']['ejercicios']))
		$ejercicios = '';
	else
		$ejercicios = $req['input']['ejercicios'];

	$view = $CONFIG['injector']->get('view.json');
	$arepo = $CONFIG['injector']->get('repo.alumno');
	$erepo = $CONFIG['injector']->get('repo.evaluacion');
	$jrepo = $CONFIG['injector']->get('repo.juego');
	$reg = $CONFIG['injector']->get('registro.puntaje');

	if (null === ($alumno = $arepo->get_by_id(intval($m[1]))))
		return false;

	if (null === ($evaluacion = $erepo->get_by_id(intval($m[2]))))
		return false;

	if (null === ($juego = $jrepo->get($m[3])))
		return false;

	try {
		$puntaje = new \puntaje\puntaje([
			'evaluacion_id' => $evaluacion->get_id(),
			'alumno_id' => $alumno->get_id(),
			'juego_id' => $juego->get_id(),
			'intentos' => $evaluacion->get_intentos(),
			'intento' => 0,
			'ejercicios' => $ejercicios,
			'aciertos' => $aciertos,
		]);
		$puntaje = $reg->agregar($puntaje);
		$view->status(201);
		$view->response([
			'evaluacion_id' => $puntaje->get_evaluacion_id(),
			'alumno_id' => $puntaje->get_alumno_id(),
			'juego_id' => $puntaje->get_juego_id(),
			'intentos' => $puntaje->get_intentos(),
			'intento' => $puntaje->get_intento(),
			'ejercicios' => $puntaje->get_ejercicios(),
			'aciertos' => $puntaje->get_aciertos(),
			'maximo' => $reg->obtener_maximo(
				$puntaje->get_evaluacion_id(),
				$puntaje->get_alumno_id(),
				$puntaje->get_juego_id()
			)->get_puntaje(),
		]);
	} catch(DomainException $e) {
		$view->status(400);
		$view->add_error($e->getMessage());
	}
	$view->render();
});
