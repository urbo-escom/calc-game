<?php

$router->add_route('GET', '/evaluaciones', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	if ('html' === $req['output']) {
		$v = $CONFIG['injector']->get('view.evaluacion');
		$v->render();
	}

	$view = $CONFIG['injector']->get('view.json');
	$repo = $CONFIG['injector']->get('repo.evaluacion');

	$offset = 0;
	$limit = 3;
	$before = null;
	$after = null;
	$in = null;
	$like = null;

	$input = $req['query'];
	if (isset($input['offset'])) $offset = $input['offset'];
	if (isset($input['limit']))  $limit = $input['limit'];
	if (isset($input['before'])) $before = $input['before'];
	if (isset($input['after']))  $after = $input['after'];
	if (isset($input['in']))     $in = $input['in'];
	if (isset($input['like']))   $like = $input['like'];

	$query = \evaluacion\repo_query::builder()
		->offset($offset)
		->limit($limit)
		->before($before)
		->after($after)
		->in($in)
		->like($like);

	try {
		$query = $query->build();
	} catch(Exception $e) {
		$d = null;
		if (1 === preg_match('/.*time string \(([^)]+)\).*/i',
				$e->getMessage(), $m))
			$d = $m[1];
		$view->status(400);
		if (null === $d)
			$view->add_error("Alguna fecha parece errónea");
		else
			$view->add_error("La fecha '{$d}' parece errónea");
		$view->render();
	}
	$col = $repo->get_all($query);

	$items = [];
	foreach ($col->get_items() as $item)
		$items[] = [
			'id' => $item->get_id(),
			'nombre' => $item->get_nombre(),
			'intentos' => $item->get_intentos(),
			'fecha_inicio' => $item->get_periodo()
				->get_inicio()->format('r'),
			'fecha_termino' => $item->get_periodo()
				->get_termino()->format('r'),
		];

	$url = new \request\url('/evaluacions');
	$links = $url->paginated_links($col->get_total(), [
		'offset' => $offset,
		'limit' => $limit,
		'like' => $like,
	]);

	$view->response([
		'links' => $links,
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => $items,
	]);
	$view->render();
});

$router->add_route('POST', '/evaluaciones', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$view = $CONFIG['injector']->get('view.json');
	$reg = $CONFIG['injector']->get('registro.evaluacion');

	$nombre = '';
	$intentos = '';
	$inicio = '';
	$termino = '';

	$input = $req['input'];
	if (isset($input['nombre']))   $nombre = $input['nombre'];
	if (isset($input['intentos'])) $intentos = $input['intentos'];
	if (isset($input['inicio']))   $inicio = $input['inicio'];
	if (isset($input['termino']))  $termino = $input['termino'];

	$periodo = null;
	try {
		$periodo = new \evaluacion\periodo($inicio, $termino);
	} catch(DomainException $e) {
		$view->status(400);
		$view->add_error([
			'periodo' => $e->getMessage(),
		]);
		$view->render();
	}

	try {
		$item = $reg->agregar($nombre, $intentos, $periodo);
		$view->status(201);
		$view->response([
			'id' => $item->get_id(),
			'nombre' => $item->get_nombre(),
			'intentos' => $item->get_intentos(),
			'fecha_inicio' => $item->get_periodo()
				->get_inicio()->format('r'),
			'fecha_termino' => $item->get_periodo()
				->get_termino()->format('r'),
		]);
	} catch(DomainException $e) {
		$view->status(400);
		$view->add_error($e->getMessage());
	}
	$view->render();
});
