<?php

$router->add_route('GET', '/escuelas', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	if ('html' === $req['output']) {
		$v = $CONFIG['injector']->get('view.escuela');
		$v->render();
	}

	$view = $CONFIG['injector']->get('view.json');
	$repo = $CONFIG['injector']->get('repo.escuela');

	$offset = 0;
	$limit = 3;
	$like = null;

	$in = $req['query'];
	if (isset($in['offset'])) $offset = $in['offset'];
	if (isset($in['limit']))  $limit = $in['limit'];
	if (isset($in['like']))   $like = $in['like'];

	$query = \escuela\repo_query::builder()
		->offset($offset)
		->limit($limit)
		->like($like)
		->build();
	$col = $repo->get_all($query);

	$items = [];
	foreach ($col->get_items() as $item)
		$items[] = [
			'id' => $item->get_id(),
			'nombre' => $item->get_nombre(),
			'alumnos' => $item->get_alumnos(),
		];

	$url = new \request\url('/escuelas');
	$links = $url->paginated_links($col->get_total(), [
		'offset' => $offset,
		'limit' => $limit,
		'like' => $like,
	]);

	$view->response([
		'links' => $links,
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => $items,
	]);
	$view->render();
});

$router->add_route('POST', '/escuelas', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_admin())
		throw new AdminRequiredException();

	$view = $CONFIG['injector']->get('view.json');
	$reg = $CONFIG['injector']->get('registro.escuela');

	if (!isset($req['input']['nombre']))
		$esc = '';
	else
		$esc = $req['input']['nombre'];

	try {
		$escuela = $reg->agregar($esc);
		$view->status(201);
		$view->response([
			'id' => $escuela->get_id(),
			'nombre' => $escuela->get_nombre(),
			'alumnos' => $escuela->get_alumnos(),
		]);
	} catch(DomainException $e) {
		$view->status(400);
		$view->add_error($e->getMessage());
	}
	$view->render();
});
