<?php

$router->add_route('GET', '/logout', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	$view    = $CONFIG['injector']->get('view.html');
	$session->logout();
	$view->redirect(302, '/');
});


$router->add_route('GET', '/login-admin', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if ($session->is_user())
		throw new AdminRequiredException();
	if ($session->is_admin())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.login.admin.controller');
	$c->get($req);
});


$router->add_route('POST', '/login-admin', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if ($session->is_user())
		throw new AdminRequiredException();
	if ($session->is_admin())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.login.admin.controller');
	$c->post($req);
});


$router->add_route('GET', '/login', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_anon())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.login.alumno.controller');
	$c->get($req);
});


$router->add_route('POST', '/login', function($_, $req) {
	global $CONFIG;
	$session = $CONFIG['injector']->get('session');
	if (!$session->is_anon())
		throw new AnonRequiredException();

	$c = $CONFIG['injector']->get('view.login.alumno.controller');
	$c->post($req);
});
