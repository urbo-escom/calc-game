<?php
use \escuela\escuela;
use \escuela\coleccion;
use \escuela\registro;
use \escuela\repo;
use \escuela\repo_query;
use \repo\pgsql\escuela_repo;

$client = $CONFIG['injector']->get('temp.database.client');
$client->query('truncate escuela cascade');


$repo = new escuela_repo($client);
$registro = new registro($repo);

$registro->agregar('ESC001');
$registro->agregar('ESC002');
$registro->agregar('ESCXX3');
$registro->agregar('ESCYY4');
$registro->agregar('ESCXX5');

echo $registro->obtener('ESC001'), "\n";
echo $registro->obtener('ESC002'), "\n";


function print_col(coleccion $col) {
	$j = [
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => [],
	];
	foreach ($col->get_items() as $item)
		$j['items'][] = [
			'id' => $item->get_id(),
			'nombre' => $item->get_nombre(),
			'alumnos' => $item->get_alumnos(),
		];
	echo json_encode($j, JSON_PRETTY_PRINT), "\n";
}


echo "escuela all\n";
$query = repo_query::builder()
	->limit(5)
	->build();
print_col($repo->get_all($query));


echo "escuela like 00\n";
$query = repo_query::builder()
	->limit(5)
	->like('00')
	->build();
print_col($repo->get_all($query));


echo "escuela like XX\n";
$query = repo_query::builder()
	->limit(5)
	->like('XX')
	->build();
print_col($repo->get_all($query));


echo "escuela like YY\n";
$query = repo_query::builder()
	->limit(5)
	->like('YY')
	->build();
print_col($repo->get_all($query));


echo "Delete ESC001\n";
$registro->eliminar('ESC001');

echo "escuela like 00\n";
$query = repo_query::builder()
	->limit(5)
	->like('00')
	->build();
print_col($repo->get_all($query));


echo "escuela all\n";
$query = repo_query::builder()
	->limit(2)
	->build();
print_col($repo->get_all($query));
