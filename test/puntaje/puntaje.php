<?php
use \alumno\alumno_datos;
use \alumno\registro as alumno_registro;
use \alumno\repo_query;
use \codigo\registro as codigo_registro;
use \escuela\escolaridad;
use \escuela\registro as escuela_registro;
use \evaluacion\periodo;
use \evaluacion\registro as evaluacion_registro;
use \juego\registro as juego_registro;
use \puntaje\puntaje;
use \puntaje\registro;
use \puntaje\repo;
use \repo\pgsql\alumno_repo;
use \repo\pgsql\codigo_repo;
use \repo\pgsql\escuela_repo;
use \repo\pgsql\evaluacion_repo;
use \repo\pgsql\juego_repo;
use \repo\pgsql\puntaje_repo;


$client = $CONFIG['injector']->get('temp.database.client');
$client->query('truncate codigo cascade');
$client->query('truncate escuela cascade');
$client->query('truncate alumno cascade');
$client->query('truncate evaluacion cascade');
$client->query('truncate puntaje cascade');


$repo = new puntaje_repo($client);
$cod_repo = new codigo_repo($client);
$esc_repo = new escuela_repo($client);
$alu_repo = new alumno_repo($client);
$eva_repo = new evaluacion_repo($client);
$jue_repo = new juego_repo($client);

$registro = new registro($repo, $eva_repo, $alu_repo, $jue_repo);
$cod_registro = new codigo_registro($cod_repo);
$esc_registro = new escuela_registro($esc_repo);
$alu_registro = new alumno_registro($alu_repo, $cod_repo, $esc_repo);
$eva_registro = new evaluacion_registro($eva_repo);
$jue_registro = new juego_registro($jue_repo);


$cod_registro->agregar('COD001');
$esc_registro->agregar('ESC001');
$alu_registro->agregar('COD001', 'ESC001', alumno_datos::builder()
	->email('ccc@gmail.com')
	->password('1234')
	->nombre('Paco')
	->apellido_pat('De la Mar')
	->apellido_mat('Pedro')
	->escolaridad(escolaridad::from_string('preparatoria1c'))
	->build());
$eva_registro->agregar('septiembre-2016', 5,
	new periodo('2016-09-01', '2016-12-01'));

echo $cod_registro->obtener('COD001'), "\n";
echo $esc_registro->obtener('ESC001'), "\n";
echo $alu_registro->obtener('ccc@gmail.com'), "\n";
echo $eva_registro->obtener('septiembre-2015'), "\n";


$alumno = $alu_registro->obtener('ccc@gmail.com');
$evaluacion = $eva_registro->obtener('septiembre-2016');
$aid = $alumno->get_id();
$eid = $evaluacion->get_id();

$juegos = $jue_registro->obtener_todos();
foreach ($juegos as $j)
	echo $j, "\n";

echo "Max ", $registro->obtener_maximo($eid, $aid, $juegos[0]->get_id()), "\n";

try {
	$arr = [10, 9, 4, 3, 15, 2, 4];
	$i = 0;
	while (true) {
		$puntaje = new puntaje([
			'evaluacion_id' => $evaluacion->get_id(),
			'alumno_id' => $alumno->get_id(),
			'juego_id' => $juegos[0]->get_id(),
			'intentos' => $evaluacion->get_intentos(),
			'intento' => 0,
			'ejercicios' => 17,
			'aciertos' => $arr[$i++],
		]);
		$registro->agregar($puntaje);
	}
} catch(DomainException $e) {
	echo "Obtuvimos: ".$e->getMessage()."\n";
}

echo "Max ", $registro->obtener_maximo($eid, $aid, $juegos[0]->get_id()), "\n";
