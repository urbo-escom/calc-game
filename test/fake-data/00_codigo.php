<?php

require(APP_ROOT.'/vendor/autoload.php');
require(__DIR__.'/eta.php');
$faker = Faker\Factory::create();


$codigo_size = 30000;

parse_str(implode('&', array_slice($argv, 1)), $_GET);
if (isset($_GET) && isset($_GET['--size']))
	$codigo_size = intval($_GET['--size'], 10);
echo "# Generating $codigo_size codigos\n";


$client = $CONFIG['injector']->get('database.client');
$client->query("set log_statement = 'none'");
$registro = $CONFIG['injector']->get('registro.codigo');
$eta = new eta($codigo_size);


echo "# Truncating codigo\n";
$client->query('truncate codigo restart identity cascade');


echo "# Generating codigo 1\n";
$client->query('begin');
$eta->start();
for ($i = 0; $i < $codigo_size; $i++) {
	if (1000 < $eta->elapsed()) {
		$eta->progress($i);
		echo "# Generating codigo ", $i + 1, "/", $codigo_size,
			", ETA ", $eta, "\n";
	}
	$registro->agregar(substr($faker->md5, 0, 16));
}
$client->query('commit');
echo "# Generated $codigo_size\n";
