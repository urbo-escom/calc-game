<?php

class eta {
	protected $avg;
	protected $timestart;
	protected $done;
	protected $size;

	public function __construct($size) {
		$this->size = $size;
	}

	public function start() {
		$this->done = 0;
		return $this->timestart = microtime(true)*1000;
	}

	public function elapsed() {
		return -$this->timestart + microtime(true)*1000;
	}

	public function progress($item) {
		if ($item < $this->done)
			throw new Exception("Can't go back!\n");

		if ($this->size < $item)
			throw new Exception("Already finish");

		$delta = -$this->done + $item;
		$delta_time = $this->elapsed();
		if (!$delta_time)
			$delta_time = 1;
		$diff = $delta/$delta_time;

		if (0 === $this->done)
			$this->avg = $diff;
		else
			$this->avg = $diff/2 + $this->avg/2;
		$this->done = $item;

		return $this->timestart = microtime(true)*1000;
	}

	public function eta() {
		return (-$this->done + $this->size)/$this->avg;
	}

	public function __toString() {
		$t = $this->eta()/1000;
		$h = (int)($t/60/60);
		$m = (int)($t/60) - 60*$h;
		$s = (int)($t) - 60*60*$h - 60*$m;
		return ($h <= 9 ? "0$h":"$h").":".
			($m <= 9 ? "0$m":"$m").":".
			($s <= 9 ? "0$s":"$s");
	}

}
