<?php

use \evaluacion\repo_query as equery;
use \juego\repo_query as jquery;
use \alumno\repo_query as aquery;
use \puntaje\repo_query as pquery;
use \puntaje\puntaje as puntaje;

require(__DIR__.'/eta.php');


$alumno_size = 100;

parse_str(implode('&', array_slice($argv, 1)), $_GET);
if (isset($_GET) && isset($_GET['--size']))
	$alumno_size = intval($_GET['--size'], 10);
echo "# Generating puntaje for $alumno_size alumnos\n";


$client = $CONFIG['injector']->get('database.client');
$client->query("set log_statement = 'none'");
$erepo = $CONFIG['injector']->get('repo.evaluacion');
$jrepo = $CONFIG['injector']->get('repo.juego');
$arepo = $CONFIG['injector']->get('repo.alumno');
$prepo = $CONFIG['injector']->get('repo.puntaje');


echo "# Truncating puntaje\n";
$client->query('truncate puntaje restart identity cascade');


$random_puntaje = function($o, $intento, $max) use ($client) {
	$ejercicios = rand(ceil($max/2), $max);
	$aciertos = rand(0, ceil($max/2));
	$client->insert([
		'insert' => 'puntaje',
		'column' => [
			'evaluacion_id',
			'alumno_id',
			'juego_id',
			'intentos',
			'intento',
			'ejercicios',
			'aciertos',
			'fecha_inicio',
			'fecha_termino',
			'fecha',
		],
		'values' => [
			'evaluacion_id' => $o['e.id'],
			'alumno_id' => $o['a.id'],
			'juego_id' => $o['j.id'],
			'intentos' => $o['intentos'],
			'intento' => $intento,
			'ejercicios' => $ejercicios,
			'aciertos' => $aciertos,
			'fecha_inicio' => $o['inicio'],
			'fecha_termino' => $o['termino'],
			'fecha' => rand(
				$o['inicio'] + 1,
				$o['termino'] - 1),
		],
	]);
};


$random_puntaje_max = function($o) use ($random_puntaje) {
	$intentos = rand(ceil($o['intentos']/2), $o['intentos']);
	$puntaje_max = rand(10, 40);
	for ($i = 0; $i < $intentos; ++$i)
		$random_puntaje($o, $i, $puntaje_max);
};


$evaluaciones = $erepo->get_all(equery::builder()
	->limit(1000)
	->before(time())
	->build());
$juegos       = $jrepo->get_all();
$alumnos      = $arepo->get_all(aquery::builder()
	->limit($alumno_size)
	->build());


$eitems = $evaluaciones->get_items();
$jitems = $juegos;
$aitems = $alumnos->get_items();
$x = count($eitems);
$y = count($jitems);
$z = count($aitems);
$eta = new eta($x*$y*$z);
echo "# Generating puntaje $x evaluacion\n";
echo "# Generating puntaje $y juego\n";
echo "# Generating puntaje $z alumno\n";

$eta->start();
$client->query("begin");
for ($i = 0; $i < $x; ++$i) {
	$p = $eitems[$i]->get_periodo();
	$o = [
		'e.id' => $eitems[$i]->get_id(),
		'intentos' => $eitems[$i]->get_intentos(),
		'inicio' => $p->get_inicio()->getTimestamp(),
		'termino' => $p->get_termino()->getTimestamp(),
	];
	for ($j = 0; $j < $y; ++$j) {
		$o['j.id'] = $jitems[$j]->get_id();
		for ($k = 0; $k < $z; ++$k) {
			$o['a.id'] = $aitems[$k]->get_id();
			if (1000 < $eta->elapsed()) {
				$n = $i*$y*$z + $j*$z + $k;
				$eta->progress($n);
				echo "# Generating puntaje ",
					$n + 1, "/", $x*$y*$z,
					", ETA ", $eta, "\n";
			}
			$random_puntaje_max($o);
		}
	}
}
$client->query("commit");

foreach ($aitems as $a)
	echo "Alumno ".$a->get_id()." ".$a->get_datos()->get_email()."\n";
