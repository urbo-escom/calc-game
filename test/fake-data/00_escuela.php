<?php

require(APP_ROOT.'/vendor/autoload.php');
require(__DIR__.'/eta.php');
$faker = Faker\Factory::create('es_ES');


$escuela_size = 200;

parse_str(implode('&', array_slice($argv, 1)), $_GET);
if (isset($_GET) && isset($_GET['--size']))
	$escuela_size = intval($_GET['--size'], 10);
echo "# Generating $escuela_size escuelas\n";


$client = $CONFIG['injector']->get('database.client');
$client->query("set log_statement = 'none'");
$registro = $CONFIG['injector']->get('registro.escuela');
$eta = new eta($escuela_size);


echo "# Truncating escuela\n";
$client->query('truncate escuela restart identity cascade');


echo "# Generating escuela 1\n";
$client->query('begin');
$eta->start();
for ($i = 0; $i < $escuela_size; $i++) {
	if (1000 < $eta->elapsed()) {
		$eta->progress($i);
		echo "# Generating escuela ", $i + 1, "/", $escuela_size,
			", ETA ", $eta, "\n";
	}
	$company = $faker->company." ".$faker->companySuffix;
	$registro->agregar($company." #".$faker->buildingNumber);
}
$client->query('commit');
echo "# Generated $escuela_size\n";
