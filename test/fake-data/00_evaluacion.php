<?php

use \evaluacion\periodo;

$client = $CONFIG['injector']->get('database.client');
$client->query("set log_statement = 'none'");
$registro = $CONFIG['injector']->get('registro.evaluacion');
echo "# Truncating evaluacion\n";
$client->query('truncate evaluacion restart identity cascade');

$es = [
	'enero-2016'      => new periodo('2016-01-01', '2016-02-01'),
	'febrero-2016'    => new periodo('2016-02-01', '2016-03-01'),
	'marzo-2016'      => new periodo('2016-03-01', '2016-04-01'),
	'abril-2016'      => new periodo('2016-04-01', '2016-05-01'),
	'mayo-2016'       => new periodo('2016-05-01', '2016-06-01'),
	'junio-2016'      => new periodo('2016-06-01', '2016-07-01'),
	'julio-2016'      => new periodo('2016-07-01', '2016-08-01'),
	'agosto-2016'     => new periodo('2016-08-01', '2016-09-01'),
	'septiembre-2016' => new periodo('2016-09-01', '2016-10-01'),
	'octubre-2016'    => new periodo('2016-10-01', '2016-11-01'),
	'noviembre-2016'  => new periodo('2016-11-01', '2016-12-01'),
	'diciembre-2016'  => new periodo('2016-12-01', '2017-01-01'),
];
foreach ($es as $k => $v)
	echo $registro->agregar($k, 5, $v)->get_nombre(), "\n";
