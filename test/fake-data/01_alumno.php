<?php

use \database\DatabaseQueryException;
use \alumno\alumno_datos;
use \codigo\repo_query as cquery;
use \escuela\escolaridad;
use \escuela\repo_query as equery;

require(APP_ROOT.'/vendor/autoload.php');
require(__DIR__.'/eta.php');
$faker = Faker\Factory::create('es_ES');


$alumno_size = 20000;
$alumno_password = 'codswallop';

parse_str(implode('&', array_slice($argv, 1)), $_GET);
if (isset($_GET) && isset($_GET['--size']))
	$alumno_size = intval($_GET['--size'], 10);
if (isset($_GET) && isset($_GET['--password']))
	$alumno_password = $_GET['--password'];
echo "# Generating $alumno_size alumnos, password = '$alumno_password'\n";


$client = $CONFIG['injector']->get('database.client');
$client->query("set log_statement = 'none'");
$repo = $CONFIG['injector']->get('repo.alumno');
$eta = new eta($alumno_size);


echo "# Truncating alumno\n";
$client->query('truncate alumno restart identity cascade');


$codigos = [];
$random_codigo = function() use ($client, $faker, &$codigos) {
	$c = array_pop($codigos);
	if (null == $c) {
		$codigos = $client->select([
			'select' => 'c.id as id',
			'from' => [[
				'LEFT_JOIN',
				'codigo as c',
				'alumno as a',
				'ON' => 'c.id = a.codigo_id',
			]],
			'where' => 'a.email IS NULL',
			'limit' => 1000,
		]);
		$c = array_pop($codigos);
	}
	return $c['id'];
};


$escuela_size = $client->select([
	'select' => 'count(*) as total',
	'from' => 'escuela'
])[0]['total'];
$random_escuela = function() use ($client, $faker, $escuela_size) {
	$offset = $faker->numberBetween(0, $escuela_size - 1);
	return $client->select([
		'select' => 'id',
		'from' => 'escuela',
		'offset' => $offset,
		'limit' => 1,
	])[0]['id'];
};


$niveles = ['secundaria', 'preparatoria'];
$grupos = ['a', 'b', 'c', 'd', 'e', 'f'];
$random_escolaridad = function() use ($niveles, $grupos) {
	$i = rand(0, 1);
	if (!$i)
		$j = rand(1, 3);
	else
		$j = rand(1, 6);
	$k = rand(0, 5);
	return new escolaridad($niveles[$i], $j, $grupos[$k]);
};


$password = password_hash($alumno_password, PASSWORD_BCRYPT, ['cost' => 13]);
echo "# Generating alumno 1\n";
$eta->start();
$client->query("begin");
for ($i = 0; $i < $alumno_size; $i++) {
	if (1000 < $eta->elapsed()) {
		$eta->progress($i);
		echo "# Generating alumno ", $i + 1, "/", $alumno_size,
			", ETA ", $eta, "\n";
	}
	$email = $faker->userName."-".$faker->email;
	$codigo_id = $random_codigo();
	$escuela_id = $random_escuela();
	$datos = alumno_datos::builder()
		->email($email)
		->password($password)
		->nombre($faker->firstName)
		->apellido_pat($faker->lastName)
		->apellido_mat($faker->lastName)
		->escolaridad($random_escolaridad())
		->build();
	$repo->add($codigo_id, $escuela_id, $datos);
}
$client->query("commit");
echo "# Generated $alumno_size\n";
