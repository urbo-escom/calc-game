<?php
use \alumno\alumno;
use \alumno\alumno_datos;
use \alumno\coleccion;
use \alumno\periodo;
use \alumno\registro;
use \alumno\repo;
use \alumno\repo_query;
use \codigo\registro as codigo_registro;
use \escuela\escolaridad;
use \escuela\registro as escuela_registro;
use \repo\pgsql\alumno_repo;
use \repo\pgsql\codigo_repo;
use \repo\pgsql\escuela_repo;


$client = $CONFIG['injector']->get('temp.database.client');
$client->query('truncate codigo cascade');
$client->query('truncate escuela cascade');
$client->query('truncate alumno cascade');


$repo = new alumno_repo($client);
$crepo = new codigo_repo($client);
$erepo = new escuela_repo($client);

$registro = new registro($repo, $crepo, $erepo);
$cregistro = new codigo_registro($crepo);
$eregistro = new escuela_registro($erepo);


$cregistro->agregar('COD001');
$cregistro->agregar('COD002');
$cregistro->agregar('COD003');
$cregistro->agregar('COD004');
$cregistro->agregar('COD005');
$cregistro->agregar('COD006');
$cregistro->agregar('COD007');

$eregistro->agregar('ESC001');
$eregistro->agregar('ESC002');
$eregistro->agregar('ESC003');

$alumnos = [

	alumno_datos::builder()
		->email('aaa@gmail.com')
		->password('1234')
		->nombre('José')
		->apellido_pat('Alfredo')
		->apellido_mat('Jimenez')
		->escolaridad(escolaridad::from_string('preparatoria1c'))
		->build(),

	alumno_datos::builder()
		->email('bbb@gmail.com')
		->password('1234')
		->nombre('Petey')
		->apellido_pat('Pablo')
		->apellido_mat('Jimenez')
		->escolaridad(escolaridad::from_string('preparatoria1c'))
		->build(),

	alumno_datos::builder()
		->email('ccc@gmail.com')
		->password('1234')
		->nombre('Paco')
		->apellido_pat('De la Mar')
		->apellido_mat('Pedro')
		->escolaridad(escolaridad::from_string('preparatoria1c'))
		->build(),

];

$registro->agregar('COD001', 'ESC001', $alumnos[0]);
$registro->agregar('COD002', 'ESC002', $alumnos[1]);
$registro->agregar('COD003', 'ESC003', $alumnos[2]);


echo $registro->obtener('aaa@gmail.com'), "\n";
echo $registro->obtener('ccc@gmail.com'), "\n";


echo "alumno all\n";
$query = repo_query::builder()->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";


echo "alumno like pa\n";
$query = repo_query::builder()
	->like('pa')
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";


try {
echo "Delete ccc@gmail.com\n";
$registro->eliminar('ccc@gmail.com');
echo "alumno like 00\n";
$query = repo_query::builder()
	->like('00')
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";
} catch(Exception $e) {
	echo $e->getMessage(), "\n";
}
