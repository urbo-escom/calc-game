<?php
use \codigo\codigo;
use \codigo\coleccion;
use \codigo\registro;
use \codigo\repo;
use \codigo\repo_query;
use \repo\pgsql\codigo_repo;

$client = $CONFIG['injector']->get('temp.database.client');
$client->query('truncate codigo cascade');


$repo = new codigo_repo($client);
$registro = new registro($repo);

$registro->agregar('COD001');
$registro->agregar('COD002');
$registro->agregar('CODXX3');
$registro->agregar('CODYY4');
$registro->agregar('CODXX5');

echo $registro->obtener('COD001'), "\n";
echo $registro->obtener('COD002'), "\n";


function print_col(coleccion $col) {
	$j = [
		'total' => $col->get_total(),
		'count' => $col->get_count(),
		'items' => [],
	];
	foreach ($col->get_items() as $item)
		$j['items'][] = [
			'id' => $item->get_id(),
			'codigo' => $item->get_codigo(),
			'email' => $item->get_email(),
		];
	echo json_encode($j, JSON_PRETTY_PRINT), "\n";
}


echo "codigo all\n";
$query = repo_query::builder()
	->limit(5)
	->build();
print_col($repo->get_all($query));


echo "codigo like 00\n";
$query = repo_query::builder()
	->limit(5)
	->like('00')
	->build();
print_col($repo->get_all($query));


echo "codigo like XX\n";
$query = repo_query::builder()
	->limit(5)
	->like('XX')
	->build();
print_col($repo->get_all($query));


echo "codigo like YY\n";
$query = repo_query::builder()
	->limit(5)
	->like('YY')
	->build();
print_col($repo->get_all($query));


echo "codigo email not null like YY\n";
$query = repo_query::builder()
	->limit(5)
	->usado(true)
	->like('YY')
	->build();
print_col($repo->get_all($query));


echo "Delete COD001\n";
$registro->eliminar('COD001');


echo "codigo like 00\n";
$query = repo_query::builder()
	->limit(5)
	->like('00')
	->build();
print_col($repo->get_all($query));


echo "codigo all with limit\n";
$query = repo_query::builder()
	->limit(2)
	->build();
print_col($repo->get_all($query));
