<?php
use \evaluacion\coleccion;
use \evaluacion\evaluacion;
use \evaluacion\periodo;
use \evaluacion\registro;
use \evaluacion\repo;
use \evaluacion\repo_query;
use \repo\pgsql\evaluacion_repo;


$client = $CONFIG['injector']->get('temp.database.client');
$client->query('truncate evaluacion cascade');


$repo = new evaluacion_repo($client);
$registro = new registro($repo);

$registro->agregar('EV001', 3, new periodo('2016-09-30', '2016-12-31'));
$registro->agregar('EV002', 1, new periodo('2016-02-30', '2016-12-31'));
$registro->agregar('EVXX3', 4, new periodo('2016-08-30', '2017-09-02'));
$registro->agregar('EVYY4', 3, new periodo('2017-09-30', '2017-10-31'));
$registro->agregar('EVXX5', 1, new periodo('2017-09-30', '2017-10-31'));


echo $registro->obtener('EV001'), "\n";
echo $registro->obtener('EV002'), "\n";


echo "evaluacion all\n";
$query = repo_query::builder()->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";


echo "evaluacion past\n";
$query = repo_query::builder()
	->before(time())
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";

echo "evaluacion present\n";
$query = repo_query::builder()
	->in(time())
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";

echo "evaluacion future\n";
$query = repo_query::builder()
	->after(time())
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";


echo "evaluacion like 00\n";
$query = repo_query::builder()
	->like('00')
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";


echo "Delete EV001\n";
$registro->eliminar('EV001');
echo "evaluacion like 00\n";
$query = repo_query::builder()
	->like('00')
	->build();
$coleccion = $repo->get_all($query);
echo "=== count {$coleccion->get_count()}/{$coleccion->get_total()}\n";
foreach ($coleccion->get_items() as $e)
	echo $e, "\n";
