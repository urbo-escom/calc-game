<?php

$CONFIG['injector']->add('database.client', function($c) {
	$conn = $c->get('database.connection');
	if ('debug' === $c->get('mode'))
		$conn->query("set log_statement = 'all'");
	return new \database\client($conn);
});

$CONFIG['injector']->add('database.connection', function($c) use ($CONFIG) {
	return new \database\connection($CONFIG['db']['postgres']);
});


$CONFIG['injector']->add('temp.database.client', function($c) {
	$conn = $c->get('temp.database.connection');
	if ('debug' === $c->get('mode'))
		$conn->query("set log_statement = 'all'");
	return new \database\client($conn);
});

$CONFIG['injector']->add('temp.database.connection',
		function($c) use ($CONFIG) {
	return new \database\connection($CONFIG['temp.db']['postgres']);
});
