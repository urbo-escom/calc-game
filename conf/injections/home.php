<?php

$CONFIG['injector']->add('view.admin.home', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\home($v);
});

$CONFIG['injector']->add('view.alumno.home', function($c) {
	$v = $c->get('view.html');
	return new \view\alumno\home($v);
});
