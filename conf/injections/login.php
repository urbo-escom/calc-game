<?php

$CONFIG['injector']->add('view.login.admin.controller', function($c) {
	$s = $c->get('session');
	$v = $c->get('view.html');
	$f = new \view\login\admin\form($v);
	return new \view\login\admin\controller($s, $f);
});

$CONFIG['injector']->add('view.login.alumno.controller', function($c) {
	$s = $c->get('session');
	$v = $c->get('view.html');
	$f = new \view\login\alumno\form($v);
	$r = $c->get('registro.alumno');
	return new \view\login\alumno\controller($s, $f, $r);
});
