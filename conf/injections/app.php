<?php

$CONFIG['injector']->add('repo.codigo', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\codigo_repo($client);
});

$CONFIG['injector']->add('repo.escuela', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\escuela_repo($client);
});

$CONFIG['injector']->add('repo.alumno', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\alumno_repo($client);
});

$CONFIG['injector']->add('repo.evaluacion', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\evaluacion_repo($client);
});

$CONFIG['injector']->add('repo.juego', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\juego_repo($client);
});

$CONFIG['injector']->add('repo.puntaje', function($c) {
	$client = $c->get('database.client');
	return new \repo\pgsql\puntaje_repo($client);
});


$CONFIG['injector']->add('registro.codigo', function($c) {
	$repo = $c->get('repo.codigo');
	return new \codigo\registro($repo);
});

$CONFIG['injector']->add('registro.escuela', function($c) {
	$repo = $c->get('repo.escuela');
	return new \escuela\registro($repo);
});

$CONFIG['injector']->add('registro.alumno', function($c) {
	$repo = $c->get('repo.alumno');
	$crepo = $c->get('repo.codigo');
	$erepo = $c->get('repo.escuela');
	return new \alumno\registro($repo, $crepo, $erepo);
});

$CONFIG['injector']->add('registro.evaluacion', function($c) {
	$repo = $c->get('repo.evaluacion');
	if ('debug' === $c->get('mode'))
		return new \evaluacion\registro($repo, false);
	return new \evaluacion\registro($repo);
});

$CONFIG['injector']->add('registro.juego', function($c) {
	$repo = $c->get('repo.juego');
	return new \juego\registro($repo);
});

$CONFIG['injector']->add('registro.puntaje', function($c) {
	$repo = $c->get('repo.puntaje');
	$erepo = $c->get('repo.evaluacion');
	$arepo = $c->get('repo.alumno');
	$jrepo = $c->get('repo.juego');
	return new \puntaje\registro($repo, $erepo, $arepo, $jrepo);
});


$CONFIG['injector']->add('reporte.alumno.evaluacion', function($c) {
	$jrepo = $c->get('repo.juego');
	$prepo = $c->get('repo.puntaje');
	return new \reporte\alumno\evaluacion($jrepo, $prepo);
});

$CONFIG['injector']->add('reporte.escuela', function($c) {
	$client = $c->get('database.client');
	return new \reporte\escuela\reporte($client);
});
