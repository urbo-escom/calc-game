<?php

$CONFIG['injector']->add('view.html', function($c) {
	return new \view\view_html($c->get('session'));
});

$CONFIG['injector']->add('view.json', function($c) {
	return new \view\view_json($c->get('session'));
});


$CONFIG['injector']->add('view.codigo', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\codigo\codigo($v);
});

$CONFIG['injector']->add('view.escuela', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\escuela\escuela($v);
});

$CONFIG['injector']->add('view.admin.alumno_list', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\alumno\alumno_list($v);
});

$CONFIG['injector']->add('view.admin.alumno', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\alumno\alumno($v);
});

$CONFIG['injector']->add('view.admin.alumno.evaluacion', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('reporte.alumno.evaluacion');
	return new \view\admin\alumno\evaluacion($v, $r);
});

$CONFIG['injector']->add('view.evaluacion', function($c) {
	$v = $c->get('view.html');
	return new \view\admin\evaluacion\evaluacion($v);
});

$CONFIG['injector']->add('view.alumno.evaluacion_list', function($c) {
	$v = $c->get('view.html');
	return new \view\alumno\evaluacion\evaluacion_list($v);
});

$CONFIG['injector']->add('view.alumno.evaluacion', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('repo.puntaje');
	return new \view\alumno\evaluacion\evaluacion($v, $r);
});

$CONFIG['injector']->add('view.alumno.evaluacion.juego', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('repo.puntaje');
	return new \view\alumno\evaluacion\juego($v, $r);
});

$CONFIG['injector']->add('view.alumno.evaluacion.reporte', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('reporte.alumno.evaluacion');
	return new \view\alumno\evaluacion\reporte($v, $r);
});

$CONFIG['injector']->add('view.admin.escuela.nivel_list', function($c) {
	$v = $c->get('view.html');
	$c = $c->get('database.client');
	$r = new \reporte\escuela\nivel_list($c);
	return new \view\admin\escuela\nivel_list($v, $r);
});

$CONFIG['injector']->add('view.admin.escuela.grupo_list', function($c) {
	$v = $c->get('view.html');
	$c = $c->get('database.client');
	$r = new \reporte\escuela\grupo_list($c);
	return new \view\admin\escuela\grupo_list($v, $r);
});

$CONFIG['injector']->add('view.admin.escuela.grupo', function($c) {
	$v = $c->get('view.html');
	$c = $c->get('database.client');
	$r = new \reporte\escuela\grupo($c);
	return new \view\admin\escuela\grupo($v, $r);
});

$CONFIG['injector']->add('view.admin.escuela.evaluacion', function($c) {
	$v = $c->get('view.html');
	$c = $c->get('database.client');
	$r = new \reporte\escuela\evaluacion($c);
	return new \view\admin\escuela\evaluacion($v, $r);
});
