<?php

$CONFIG['injector']->add('view.registro.controller', function($c) {
	$r = $c->get('registro.alumno');
	$e = $c->get('repo.escuela');
	$v = new \view\registro\alumno($c->get('view.html'), $e);
	return new \view\registro\controller($r, $v);
});
