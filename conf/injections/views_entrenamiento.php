<?php

$CONFIG['injector']->add('view.alumno.entrenamiento', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('repo.puntaje');
	return new \view\alumno\entrenamiento\index($v, $r);
});

$CONFIG['injector']->add('view.alumno.entrenamiento.juego', function($c) {
	$v = $c->get('view.html');
	$r = $c->get('repo.puntaje');
	return new \view\alumno\entrenamiento\juego($v, $r);
});
