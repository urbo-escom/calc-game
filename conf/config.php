<?php

define('APP_ROOT',   __DIR__.'/..');
define('APP_SRC',    __DIR__.'/../app');
define('APP_VIEW',   __DIR__.'/../view');
define('APP_CONF',   __DIR__.'/../conf');
define('APP_PUBLIC', __DIR__.'/../public');


spl_autoload_register(function($class) {
	$file = str_replace('\\', '/', $class).'.php';

	if (file_exists(APP_SRC.'/'.$file)) {
		require(APP_SRC.'/'.$file);
		return;
	}

	$pre = substr($class, 0, strlen("view"));
	if ("view" === $pre) {
		if (file_exists(APP_ROOT.'/'.$file)) {
			require(APP_ROOT.'/'.$file);
			return;
		}
	}
});


$CONFIG = [
	'db' => [
		'postgres' => [
			'database' => getenv('PGDATABASE'),
			'username' => getenv('PGUSER'),
			'password' => getenv('PGPASSWORD'),
		],
	],
	'temp.db' => [
		'postgres' => [
			'database' => 'temp_'.getenv('PGDATABASE'),
			'username' => getenv('PGUSER'),
			'password' => getenv('PGPASSWORD'),
		],
	],
	'mode' => 'debug',
];
$CONFIG['injector'] = new \dependency\injector();
$CONFIG['injector']->add('mode', $CONFIG['mode']);


$debug_file = __DIR__.'/php_error.php';
if ('debug' === $CONFIG['mode'] && file_exists($debug_file)) {
	define('PHPERROR_WRAP_AJAX', false);
	require($debug_file);
	\php_error\reportErrors();
}


if (!function_exists('apache_request_headers')) {
	function apache_request_headers() {
		$arr = array();
		foreach ($_SERVER as $k => $v) {
			if (substr($k, 0, 5) <> 'HTTP_')
				continue;
			$h = str_replace(' ', '-', ucwords(str_replace(
				'_', ' ', strtolower(substr($k, 5)))));
			$arr[$h] = $v;
		}
		if (isset($_SERVER['CONTENT_TYPE']))
			$arr['Content-Type'] = $_SERVER['CONTENT_TYPE'];
		if (isset($_SERVER['CONTENT_LENGTH']))
			$arr['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
		return $arr;
	}
}


foreach (glob(__DIR__."/injections/*.php") as $file) {
	include $file;
}
