<?php
namespace view;

use \request\session;

class view_html extends view {
	protected $css = [
		'https://fonts.googleapis.com/css?family=Mitr',
		'/css/normalize.min.css',
		'/css/base.css',
	];
	protected $js = [];
	protected $menu = [];
	protected $breadcrumb = [];
	protected $title = null;
	protected $body = null;

	public function __construct(session $session) {
		parent::__construct($session);
		$this->content_type = "text/html; charset=utf-8";
	}

	public function add_css($css) {
		if (!is_array($css))
			return $this->add_css([$css]);
		foreach ($css as $v)
			array_push($this->css, $v);
	}

	public function add_js($js) {
		if (!is_array($js))
			return $this->add_js([$js]);
		foreach ($js as $v)
			array_push($this->js, $v);
	}

	public function add_menu($href, $menu) {
		$this->menu[$href] = $menu;
	}

	public function add_breadcrumb($href, $menu) {
		$this->breadcrumb[$href] = $menu;
	}

	public function title($t) {
		$this->title = $t;
	}

	protected function render_body() {
		$PARAMS = [
			'id'         => $this->id,
			'title'      => $this->title,
			'js'         => $this->js,
			'css'        => $this->css,
			'menu'       => $this->menu,
			'breadcrumb' => $this->breadcrumb,
			'session'    => $this->session,
			'response'   => $this->response,
			'error'      => $this->errors,
		];
		if (0 < count($this->errors)) {
			$PARAMS['_status'] = [
				'code' => $this->status,
				'message' => $this->http_status[$this->status],
			];
			$PARAMS['_error'] = $this->errors;
		}
		require_once(__DIR__.'/view.html');
	}

}
