<?php
namespace view;

use \request\session;
use \request\url;

abstract class view {
	/* Lista de los status http más comunes */
	public $http_status = array(
		"200" => "OK",
		"201" => "Created",
		"302" => "Found",
		"303" => "See other",
		"307" => "Temporary Redirect",
		"400" => "Bad Request",
		"401" => "Unauthorized",
		"403" => "Forbidden",
		"404" => "Not Found",
		"405" => "Method Not Allowed",
		"500" => "Internal Server Error"
	);

	public $http_status_msg = array(
		"200" => "OK",
		"201" => "Created",
		"302" => "Found",
		"303" => "See other",
		"307" => "Temporary Redirect",
		"400" => "La petición fue incorrecta",
		"401" => "Se necesita una autenticación",
		"403" => "No está permitido acceder a esta url",
		"404" => "La url no fue encontrada",
		"405" => "El método usado no es aplicable a esta url",
		"500" => "Error del servidor"
	);

	public function __construct(session $session) {
		$this->session = $session;
	}

	protected $id = "none";
	protected $session;
	protected $status = 200;
	protected $errors = [];
	protected $response;
	protected $content_type = "text/html; charset=utf-8";

	public function status($c) {
		$this->status = $c;
	}

	public function add_error($ss) {
		if (!is_array($ss))
			$ss = [$ss];
		foreach ($ss as $k => $s) {
			if (is_int($k))
				$this->errors[] = $s;
			else
				$this->errors[$k] = $s;
		}
	}

	public function id($id) {
		$this->id = $id;
	}

	public function response($response) {
		$this->response = $response;
	}

	public function redirect($status, $url, $query = []) {
		$abs_url = new url($url);
		header("HTTP/1.1 $status ".
			$this->http_status[$this->status]);
		header("Location: ".$abs_url->generate_url($query));
	}

	public function render() {
		header("HTTP/1.1 $this->status ".
			$this->http_status[$this->status]);
		header("Content-Type: $this->content_type");
		$this->render_body();
		exit(0);
	}

	abstract protected function render_body();

}
