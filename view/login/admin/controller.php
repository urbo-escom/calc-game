<?php
namespace view\login\admin;

use \view\login\admin\form as view_form;

use \request\session;

use \Exception;

class controller {
	protected $session;
	protected $view;

	protected $hardcoded_credentials = [
		'mario@agilidadmental.mx' =>
		'$2a$12$.hC9rq7/1T09bNUvA07iN./WOorWYtYzB8JVvmOi/nOzzsH3617bi',
		'enrique@agilidadmental.mx' =>
		'$2a$12$MmLl3xS3wfhhXr/mYHqiU.WeHlDBMnaHpV3hNC6H8Wb2CwxdzHwh6',
	];

	public function __construct(session $s, view_form $v) {
		$this->session = $s;
		$this->view = $v;
	}

	public function get($req) {
		$this->view->render();
	}

	private function verify($in) {
		if (isset($in['email']))
			$email = $in['email'];
		else
			$email = '';

		if (isset($in['password']))
			$password = $in['password'];
		else
			$password = '';

		if (!strlen($email))
			throw new Exception("El email está vacío");

		if (!strlen($password))
			throw new Exception("La contraseña está vacía");

		if (!isset($this->hardcoded_credentials[$email]))
			throw new Exception("El email es erróneo");

		$pass2 = $this->hardcoded_credentials[$email];
		if (!password_verify($password, $pass2))
			throw new Exception("La contraseña es errónea");
	}

	public function post($req) {
		try {
			$this->verify($req['input']);
			$this->session->login_as_admin();
			$this->view->succeed();
		} catch(Exception $e) {
			$this->session->logout();
			$this->view->fail($e->getMessage(), $req['input']);
		}
	}

}
