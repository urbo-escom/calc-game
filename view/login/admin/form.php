<?php
namespace view\login\admin;

use \view\view_html;

class form {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
		$this->view->id('login-admin');
	}

	public function fail($message, $params) {
		$this->view->status(400);
		$PARAMS = [
			'email' => $params['email'],
			'password' => '',
			'_error' => $message,
		];
		ob_start();
		require(__DIR__.'/form.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

	public function render() {
		$this->view->status(200);
		$PARAMS = [
			'email' => '',
			'password' => '',
		];
		ob_start();
		require(__DIR__.'/form.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

	public function succeed() {
		$this->view->redirect(302, '/admin');
	}

}
