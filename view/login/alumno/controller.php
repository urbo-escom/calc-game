<?php
namespace view\login\alumno;

use \view\login\alumno\form as view_form;

use \request\session;
use \alumno\registro as m_registro;

use \Exception;

class controller {
	protected $session;
	protected $view;
	protected $registro;

	public function __construct(session $s, view_form $v, m_registro $m) {
		$this->session = $s;
		$this->view = $v;
		$this->registro = $m;
	}

	public function get($req) {
		$this->view->render();
	}

	private function verify($in) {
		$email    = $in['email'];
		$password = $in['password'];
		return $this->registro->autenticar($email, $password);
	}

	public function post($req) {
		try {
			$alumno = $this->verify($req['input']);
			$this->session->login_as_user();
			$this->session->set('session.user.id',
				$alumno->get_id());
			$this->view->succeed();
		} catch(Exception $e) {
			$this->session->logout();
			$this->view->fail($e->getMessage(), $req['input']);
		}
	}

}
