<?php
namespace view;

use \request\session;

class view_json extends view {

	public function __construct(session $session) {
		parent::__construct($session);
		$this->content_type = "application/json; charset=utf-8";
	}

	protected function render_body() {
		if (0 < count($this->errors)) {
			$this->response['_status'] = [
				'code' => $this->status,
				'message' => $this->http_status[$this->status],
			];
			$this->response['_error'] = $this->errors;
		}
		echo json_encode($this->response, 0
			| JSON_PRETTY_PRINT
			| JSON_UNESCAPED_SLASHES
			| JSON_UNESCAPED_UNICODE);
	}

}
