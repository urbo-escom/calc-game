<?php
namespace view\registro;

use \view\registro\alumno as view_alumno;

use \alumno\alumno       as m_alumno;
use \alumno\alumno_datos as m_datos;
use \alumno\registro     as m_registro;
use \escuela\escolaridad as m_escolaridad;

use \DomainException;

class controller {
	protected $registro;
	protected $view;

	public function __construct(m_registro $r, view_alumno $v) {
		$this->registro = $r;
		$this->view = $v;
	}

	public function get($req) {
		$this->view->render();
	}

	protected function registrar($in) {
		$this->registro->verificar_passwords(
			$in['password'],
			$in['password2']);
		$datos = m_datos::builder()
			->email($in['email'])
			->password($in['password'])
			->nombre($in['nombre'])
			->apellido_pat($in['apellido_pat'])
			->apellido_mat($in['apellido_mat'])
			->escolaridad(m_escolaridad::from_string(
				"{$in['nivel-grado']}-{$in['grupo']}"))
			->build();
		return $this->registro->agregar(
			$in['codigo'], $in['escuela'], $datos);
	}

	public function post($req) {
		try {
			$alumno = $this->registrar($req['input']);
			$this->view->succeed($alumno);
		} catch(DomainException $e) {
			$this->view->fail($e->getMessage(), $req['input']);
		}
	}

}
