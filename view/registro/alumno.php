<?php
namespace view\registro;

use \alumno\alumno as m_alumno;
use \escuela\repo as erepo;
use \escuela\repo_query as erepo_query;
use view\view_html;

class alumno {
	protected $view;
	protected $erepo;

	public function __construct(view_html $view, erepo $repo) {
		$this->view = $view;
		$this->view->id('registro-alumno');
		$this->view->add_css('/css/awesomplete.css');
		$this->view->add_js('/js/awesomplete.min.js');
		$this->erepo = $repo;
	}

	private function get_all_escuelas() {
		return $this->erepo->get_all(erepo_query::builder()
			->limit(1000)
			->build())->get_items();
	}

	public function fail($message, $params) {
		$this->view->status(400);
		$PARAMS = [
			'codigo' => $params['codigo'],
			'email' => $params['email'],
			'password' => '',
			'password2' => '',
			'nombre' => $params['nombre'],
			'apellido_pat' => $params['apellido_pat'],
			'apellido_mat' => $params['apellido_mat'],
			'escuela' => $params['escuela'],
			'escuelas' => $this->get_all_escuelas(),
			'nivel-grado' => $params['nivel-grado'],
			'grupo' => $params['grupo'],
			'_error' => $message,
		];
		ob_start();
		require(__DIR__.'/alumno.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

	public function succeed(m_alumno $alumno) {
		ob_start();
		require(__DIR__.'/registrado.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

	public function render($params = array()) {
		$this->view->status(200);
		$PARAMS = [
			'codigo' => '',
			'email' => '',
			'password' => '',
			'password2' => '',
			'nombre' => '',
			'apellido_pat' => '',
			'apellido_mat' => '',
			'escuelas' => $this->get_all_escuelas(),
			'nivel-grado' => 'primaria-1',
			'grupo' => 'a',
		];
		ob_start();
		require(__DIR__.'/alumno.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

}
