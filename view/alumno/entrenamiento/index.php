<?php
namespace view\alumno\entrenamiento;

use \view\view_html;
use \puntaje\repo as prepo;

class index {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
	}

	public function get($alumno, $juegos) {
		$this->view->status(200);
		$PARAMS = [
			'alumno' => $alumno,
			'juegos' => $juegos,
		];
		ob_start();
		require(__DIR__.'/index.html');
		$res = ob_get_clean();
		$this->view->id('alumno-entrenamiento');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/entrenamiento",
			"Entrenamiento"
		);
		$this->view->response($res);
		$this->view->render();
	}

}
