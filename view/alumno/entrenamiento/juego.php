<?php
namespace view\alumno\entrenamiento;

use \view\view_html;

class juego {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
		$this->view->add_js('/js/juegos.js');
	}

	public function get($alumno, $juego) {
		$this->view->status(200);
		$PARAMS = [
			'alumno' => $alumno,
			'juego' => $juego,
		];
		ob_start();
		require(__DIR__.'/juego.html');
		$res = ob_get_clean();
		$this->view->id('alumno-entrenamiento-juego');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/entrenamiento",
			"Entrenamiento"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/entrenamiento/".
			$juego->get_nombre(),
			$juego->get_nombre_completo()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
