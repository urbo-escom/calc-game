<?php
namespace view\alumno\evaluacion;

use \view\view_html;
use \puntaje\repo as prepo;

class juego {
	protected $view;
	protected $prepo;

	public function __construct(view_html $view, prepo $repo) {
		$this->view = $view;
		$this->prepo = $repo;
		$this->view->add_js('/js/juegos.js');
	}

	public function get($alumno, $evaluacion, $juego) {
		$this->view->status(200);
		$ps = $this->prepo->get_all(
			$evaluacion->get_id(),
			$alumno->get_id(),
			$juego->get_id());
		$PARAMS = [
			'alumno' => $alumno,
			'evaluacion' => $evaluacion,
			'juego' => $juego,
			'puntaje' => $this->prepo->get_max(
				$evaluacion->get_id(),
				$alumno->get_id(),
				$juego->get_id()),
			'intentos' => $evaluacion->get_intentos(),
			'intento' => count($ps),
		];
		ob_start();
		require(__DIR__.'/juego.html');
		$res = ob_get_clean();
		$this->view->id('alumno-evaluacion-juego');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones",
			"Evaluaciones"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones/".
			$evaluacion->get_id(),
			$evaluacion->get_nombre()
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones/".
			$evaluacion->get_id()."/".
			$juego->get_nombre(),
			$juego->get_nombre_completo()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
