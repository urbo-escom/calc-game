<?php
namespace view\alumno\evaluacion;

use \view\view_html;

class evaluacion_list {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
	}

	public function get($alumno, $past, $present, $future) {
		$this->view->status(200);
		$PARAMS = [
			'alumno' => $alumno,
			'past' => $past,
			'present' => $present,
			'future' => $future,
		];
		ob_start();
		require(__DIR__.'/evaluacion_list.html');
		$res = ob_get_clean();
		$this->view->id('alumno-evaluacion-list');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones",
			"Evaluaciones"
		);
		$this->view->response($res);
		$this->view->render();
	}

}
