<?php
namespace view\alumno\evaluacion;

use \view\view_html;
use \reporte\alumno\evaluacion;

class reporte {
	protected $view;
	protected $evaluacion;

	public function __construct(view_html $view, evaluacion $e) {
		$this->view = $view;
		$this->evaluacion = $e;
		$this->view->add_js('/js/jspdf.debug.js');
		$this->view->add_js('/js/jspdf.plugin.autotable.js');
	}

	public function get($escuela, $alumno, $evaluacion) {
		$this->view->status(200);
		$puntajes = $this->evaluacion->get_reporte(
			$evaluacion->get_id(),
			$alumno->get_id());
		$PARAMS = [
			'alumno' => $alumno,
			'escuela' => $escuela,
			'evaluacion' => $evaluacion,
			'puntajes' => $puntajes,
		];
		ob_start();
		require(__DIR__.'/reporte.html');
		$res = ob_get_clean();
		$this->view->id('alumno-evaluacion-reporte');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones",
			"Evaluaciones"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones/".
			$evaluacion->get_id(),
			$evaluacion->get_nombre()
		);
		$this->view->add_breadcrumb(
			"/reporte".
			"/alumnos/".$alumno->get_id()."/evaluaciones/".
			$evaluacion->get_id(),
			"Reporte"
		);
		$this->view->response($res);
		$this->view->render();
	}

}
