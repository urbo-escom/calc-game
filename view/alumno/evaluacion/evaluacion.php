<?php
namespace view\alumno\evaluacion;

use \view\view_html;
use \puntaje\repo as prepo;

class evaluacion {
	protected $view;
	protected $prepo;

	public function __construct(view_html $view, prepo $repo) {
		$this->view = $view;
		$this->prepo = $repo;
	}

	public function get($alumno, $evaluacion, $juegos) {
		$this->view->status(200);
		$PARAMS = [
			'alumno' => $alumno,
			'evaluacion' => $evaluacion,
			'juegos' => [],
		];
		foreach ($juegos as $j) {
			$ps = $this->prepo->get_all(
				$evaluacion->get_id(),
				$alumno->get_id(),
				$j->get_id());
			$PARAMS['juegos'][] = [
				'juego' => $j,
				'puntaje' => $this->prepo->get_max(
					$evaluacion->get_id(),
					$alumno->get_id(),
					$j->get_id()),
				'intentos' => $evaluacion->get_intentos(),
				'intento' => count($ps),
			];
		}
		ob_start();
		require(__DIR__.'/evaluacion.html');
		$res = ob_get_clean();
		$this->view->id('alumno-evaluacion');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones",
			"Evaluaciones"
		);
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id()."/evaluaciones/".
			$evaluacion->get_id(),
			$evaluacion->get_nombre()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
