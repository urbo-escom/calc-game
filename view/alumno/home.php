<?php
namespace view\alumno;

use \view\view_html;

use \alumno\alumno       as m_alumno;
use \alumno\alumno_datos as m_datos;
use \alumno\registro     as m_registro;
use \escuela\escuela     as m_escuela;
use \escuela\escolaridad as m_escolaridad;

class home {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
		$this->view->id('admin-home');
	}

	public function get($alumno, $codigo, $escuela) {
		$this->view->status(200);
		$PARAMS = [
			'alumno' => $alumno,
			'codigo' => $codigo,
			'escuela' => $escuela,
		];
		ob_start();
		require(__DIR__.'/home.html');
		$res = ob_get_clean();
		$this->view->id('alumno-home');
		$this->view->add_breadcrumb(
			"/alumnos/".$alumno->get_id(),
			"Inicio"
		);
		$this->view->response($res);
		$this->view->render();
	}

}
