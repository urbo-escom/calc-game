<?php
namespace view\admin\codigo;

use view\view_html;

class codigo {
	protected $view;

	public function __construct(view_html $v) {
		$this->view = $v;
		$this->view->id('codigos');
		$this->view->add_js('/js/table_pager.js');
		$this->view->add_js('/js/xlsx.core.min.js');
		$this->view->add_js('/js/xlsx2arr.js');
	}

	public function render() {
		ob_start();
		require(__DIR__.'/codigo.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

}
