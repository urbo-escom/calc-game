<?php
namespace view\admin\evaluacion;

use view\view_html;

class evaluacion {
	protected $view;

	public function __construct(view_html $v) {
		$this->view = $v;
		$this->view->id('evaluaciones');
		$this->view->add_js('/js/table_pager.js');
		$this->view->add_js('/js/tiny-date-picker.js');
		$this->view->add_css('/css/tiny-date-picker.css');
	}

	public function render() {
		ob_start();
		require(__DIR__.'/evaluacion.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

}
