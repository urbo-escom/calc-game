<?php
namespace view\admin\escuela;

use \view\view_html;

use \reporte\escuela\evaluacion as e;

class evaluacion {
	protected $view;
	protected $r;

	public function __construct(view_html $view, e $r) {
		$this->view = $view;
		$this->view->id('admin-home');
		$this->view->add_js('/js/jspdf.debug.js');
		$this->view->add_js('/js/jspdf.plugin.autotable.js');
		$this->view->add_js('/js/table_pager.js');
		$this->r = $r;
	}

	public function get($escuela, $evaluacion, $nivel, $grado, $grupo) {
		$this->view->status(200);
		$PARAMS = [
			'escuela' => $escuela,
			'evaluacion' => $evaluacion,
			'nivel' => $nivel,
			'grado' => $grado,
			'grupo' => ucwords("$grupo"),
			'list' => $this->r->get_list(
				$escuela->get_id(),
				$evaluacion->get_id(),
				$nivel, $grado, $grupo
			),
		];
		ob_start();
		require(__DIR__.'/evaluacion.html');
		$res = ob_get_clean();
		$this->view->id('admin-escuela-evaluacion-grupo-list');
		$this->view->add_breadcrumb(
			"/escuelas",
			"Escuelas"
		);
		$this->view->add_breadcrumb(
			"/admin/escuelas/".$escuela->get_id(),
			$escuela->get_nombre()
		);
		$this->view->add_breadcrumb(
			"/admin/escuelas/".$escuela->get_id().
			"/".$nivel.$grado,
			ucwords($nivel." ".$grado)
		);
		$this->view->add_breadcrumb(
			"/admin/escuelas/".$escuela->get_id().
			"/".$nivel.$grado.strtolower($grupo),
			"Grupo ".$grupo
		);
		$this->view->add_breadcrumb(
			"/admin/escuelas/".$escuela->get_id().
			"/".$nivel.$grado.strtolower($grupo).
			"/evaluaciones/".$evaluacion->get_id(),
			"Evaluación ".$evaluacion->get_nombre()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
