<?php
namespace view\admin\escuela;

use \view\view_html;

use \reporte\escuela\nivel_list as nlist;

class nivel_list {
	protected $view;
	protected $r;

	public function __construct(view_html $view, nlist $r) {
		$this->view = $view;
		$this->view->id('admin-home');
		$this->view->add_js('/js/table_pager.js');
		$this->r = $r;
	}

	public function get($escuela) {
		$this->view->status(200);
		$PARAMS = [
			'escuela' => $escuela,
			'grupos' => $this->r->get_list(
				$escuela->get_id()
			),
		];
		ob_start();
		require(__DIR__.'/nivel_list.html');
		$res = ob_get_clean();
		$this->view->id('admin-escuela-evaluacion-grupo-list');
		$this->view->add_breadcrumb(
			"/escuelas",
			"Escuelas"
		);
		$this->view->add_breadcrumb(
			"/admin/escuelas/".$escuela->get_id(),
			$escuela->get_nombre()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
