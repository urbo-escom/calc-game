<?php
namespace view\admin;

use \view\view_html;

class home {
	protected $view;

	public function __construct(view_html $view) {
		$this->view = $view;
		$this->view->id('admin-home');
	}

	public function get($req) {
		$this->view->status(200);
		ob_start();
		require(__DIR__.'/home.html');
		$res = ob_get_clean();
		$this->view->id('admin-home');
		$this->view->response($res);
		$this->view->render();
	}

}
