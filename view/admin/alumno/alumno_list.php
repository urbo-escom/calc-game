<?php
namespace view\admin\alumno;

use view\view_html;

class alumno_list {
	protected $view;

	public function __construct(view_html $v) {
		$this->view = $v;
		$this->view->id('alumnos');
		$this->view->add_js('/js/table_pager.js');
	}

	public function render() {
		ob_start();
		require(__DIR__.'/alumno_list.html');
		$res = ob_get_clean();
		$this->view->response($res);
		$this->view->render();
	}

}
