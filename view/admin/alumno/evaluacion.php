<?php
namespace view\admin\alumno;

use \view\view_html;
use \reporte\alumno\evaluacion as r_evaluacion;

class evaluacion {
	protected $view;
	protected $evaluacion;

	public function __construct(view_html $view, r_evaluacion $e) {
		$this->view = $view;
		$this->evaluacion = $e;
		$this->view->add_js('/js/jspdf.debug.js');
		$this->view->add_js('/js/jspdf.plugin.autotable.js');
	}

	public function get($escuela, $alumno, $evaluacion) {
		$this->view->status(200);
		$puntajes = $this->evaluacion->get_reporte(
			$evaluacion->get_id(),
			$alumno->get_id());
		$PARAMS = [
			'alumno' => $alumno,
			'escuela' => $escuela,
			'evaluacion' => $evaluacion,
			'puntajes' => $puntajes,
		];
		ob_start();
		require(__DIR__.'/evaluacion.html');
		$res = ob_get_clean();
		$this->view->id('admin-alumno-evaluacion');
		$this->view->add_breadcrumb(
			"/admin/alumnos",
			"Alumnos"
		);
		$this->view->add_breadcrumb(
			"/admin/alumnos/".$alumno->get_id(),
			$alumno->get_datos()->get_email()
		);
		$this->view->add_breadcrumb(
			"/admin/alumnos/".$alumno->get_id().
				"/evaluaciones/".$evaluacion->get_id(),
			$evaluacion->get_nombre()
		);
		$this->view->response($res);
		$this->view->render();
	}

}
