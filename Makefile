self := $(patsubst %/,%,$(dir $(lastword ${MAKEFILE_LIST})))

%.gitadd:    % ; git add $<
%.cachediff: % ; git diff --cached $<
%.diff:      % ; git diff $<

PHP_RUN := php -d auto_prepend_file=${self}/conf/config.php
%.php.run:   %.php ; ${PHP_RUN} $<
%.php.check: %.php ; ${PHP_RUN} -l $<
%.php.run:   PHP_RUN := ${PHP_RUN}
%.php.check: PHP_RUN := ${PHP_RUN}

%/:
	$(if $(wildcard $@.),@echo $@ exists,mkdir $@)

.PHONE: ${self}/install
.PHONE: ${self}/install-database
.PHONE: ${self}/install-database-temp
${self}/install: ${self}/install-database
	make -f ${self}/test/Makefile ${self}/test/install-fake

.PHONE: ${self}/test
${self}/test: ${self}/install-database-temp
${self}/test: self := ${self}
${self}/test: .FORCE
	make -f ${self}/test/Makefile ${self}/test/run-test
.FORCE:

${self}/install-database: self := ${self}
${self}/install-database:
	${self}/database/install-database.sh

${self}/install-database-temp: self := ${self}
${self}/install-database-temp:
	${self}/database/install-database-temp.sh

.PHONE: ${self}/all
.PHONE: ${self}/clean
.DEFAULT_GOAL := ${self}/all


.PHONE: ${self}/composer-install
${self}/composer-install: ${self}/composer.lock
${self}/composer.lock:    ${self}/composer.json
	$(strip $(if $(and \
		$(findstring .,${self}), \
		$(findstring ${self},.)),, \
	cd ${self} &&) \
	composer install)


.PHONE: ${self}/composer-update
${self}/composer-update:
	$(strip $(if $(and \
		$(findstring .,${self}), \
		$(findstring ${self},.)),, \
	cd ${self} &&) \
	composer update)


.PHONY: ${self}/download
${self}/all: ${self}/download


wget = \
	$(strip \
	$(if $(wildcard $@),@echo $@ already downloaded, \
		wget -O"$@" "${URL}"))


download_file :=
download_file += ${self}/conf/php_error.php
download_file += ${self}/public/css/awesomplete.css
download_file += ${self}/public/css/normalize.min.css
download_file += ${self}/public/css/tiny-date-picker.css
download_file += ${self}/public/js/awesomplete.min.js
download_file += ${self}/public/js/jspdf.debug.js
download_file += ${self}/public/js/jspdf.plugin.autotable.js
download_file += ${self}/public/js/tiny-date-picker.js
download_file += ${self}/public/js/xlsx.core.min.js
${self}/download: ${download_file}
$(foreach d, ${download_file}, \
	$(eval ${self}/clean:: ; rm -f $d))
${self}/clean: self := ${self}
${self}/clean::
	rm -rf ${self}/tmp


f := php_error.php
${self}/conf/$f: URL := \
	https://github.com/kenorb-contrib/PHP-Error/raw/master/src/$f
${self}/conf/$f:
	$(call wget)


f1 := awesomplete.css
f2 := awesomplete.min.js
f3 := awesomplete.zip

${self}/public/css/${f1}: ${self}/tmp/${f3}
	unzip -p $< awesomplete-gh-pages/$(notdir $@) >$@

${self}/public/js/${f2}: ${self}/tmp/${f3}
	unzip -p $< awesomplete-gh-pages/$(notdir $@) >$@

${self}/tmp/${f3}: URL := \
	https://github.com/LeaVerou/awesomplete/archive/gh-pages.zip
${self}/tmp/${f3}: | ${self}/tmp/
	$(call wget)


f := normalize.min.css
${self}/public/css/$f: URL := \
	https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/$f
${self}/public/css/$f:
	$(call wget)


f1 := tiny-date-picker.js
f2 := tiny-date-picker.css
f3 := tiny-date-picker-1.1.4.zip

${self}/public/js/${f1}: ${self}/tmp/${f3} ${self}/public/js/${f1}.patch
	unzip -p $(word 1, $^) tiny-date-picker-1.1.4/$(notdir $@) >$@ && \
	patch $@ $(word 2, $^)

${self}/public/css/${f2}: ${self}/tmp/${f3}
	unzip -p $< tiny-date-picker-1.1.4/$(notdir $@) >$@

${self}/tmp/${f3}: URL := \
	https://github.com/chrisdavies/tiny-date-picker/archive/v1.1.4.zip
${self}/tmp/${f3}: | ${self}/tmp/
	$(call wget)


f := jspdf.debug.js
${self}/public/js/$f: URL := \
	https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/$f
${self}/public/js/$f:
	$(call wget)


f := jspdf.plugin.autotable.js
${self}/public/js/$f: URL := \
	https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.0.33/$f
${self}/public/js/$f:
	$(call wget)


f := xlsx.core.min.js
${self}/public/js/$f: URL := \
	https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/$f
${self}/public/js/$f:
	$(call wget)
