drop database if exists temp_calc;
create database temp_calc
	encoding = 'UTF8'
	lc_collate = 'es_MX.UTF-8'
	lc_ctype   = 'es_MX.UTF-8'
	template = template0;
