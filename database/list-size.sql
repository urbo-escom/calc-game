select
	table_name,
	pg_size_pretty(table_size) as table_size,
	pg_size_pretty(indexes_size) as indexes_size,
	pg_size_pretty(total_size) as total_size
from (
	select
		table_name,
		pg_table_size(table_name) as table_size,
		pg_indexes_size(table_name) as indexes_size,
		pg_total_relation_size(table_name) as total_size
	from (
		select
			('"' ||
			table_schema || '"."' ||
			table_name || '"') as table_name
		from information_schema.tables
	) as all_tables
	order by total_size desc
) as pretty_sizes
where table_name like '%public%'
order by total_size desc;
