#!/bin/sh

self="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)"
cd "$self"

psql -ae -c 'show server_encoding'
psql -ae -c 'show client_encoding'
PGDATABASE= psql -ae -f create_database_temp.sql

for i in *.table.sql; do
	echo \
	psql -d temp_calc -f $i >&2
	psql -d temp_calc -f $i
done
