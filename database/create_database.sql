drop database if exists calc;
create database calc
	encoding = 'UTF8'
	lc_collate = 'es_MX.UTF-8'
	lc_ctype   = 'es_MX.UTF-8'
	template = template0;
