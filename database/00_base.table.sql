set client_encoding to 'UTF8';


/* Los códigos son simples strings únicos */
drop table if exists codigo cascade;
create table         codigo (
	id     serial not null, -- serial = auto increment
	codigo varchar(63) not null,
	--
	primary key(id),
	unique(codigo)
);


/* Las escuelas solo son nombres únicos */
drop table if exists escuela cascade;
create table         escuela (
	id     serial not null,
	nombre varchar(127) not null,
	--
	primary key(id),
	unique(nombre)
);


drop type  if exists escuela_nivel cascade;
create type          escuela_nivel as enum('secundaria', 'preparatoria');
drop type  if exists escuela_grupo cascade;
create type          escuela_grupo as enum('a', 'b', 'c', 'd', 'e', 'f');

drop table if exists escuela_nivel_grado cascade;
create table         escuela_nivel_grado as
	select
		cast(n.nivel as escuela_nivel) as nivel,
		g.grado as grado
	from (select unnest(enum_range(null::escuela_nivel)) as nivel) as n
	join (select t as grado from generate_series(1, 6) as t) as g
	on (
		'preparatoria' = n.nivel or
		('secundaria' = n.nivel and
			1 <= g.grado and g.grado <= 3));


drop table if exists alumno cascade;
create table         alumno (
	id       serial not null,
	email    varchar(127) not null,
	password varchar(255) not null,
	--
	nombre       varchar(255) not null, /* estos campos pueden proveer */
	apellido_pat varchar(255) not null, /* de búsquedas más avanzadas  */
	apellido_mat varchar(255) not null,
	--
	codigo_id  serial not null,
	escuela_id serial not null,
	--
	nivel escuela_nivel not null,
	grado smallint not null,
	grupo escuela_grupo not null,
	--
	fecha_registro integer default extract(epoch from current_timestamp),
	--
	primary key(id),
	unique(email),
	unique(codigo_id),
	foreign key(codigo_id) references codigo(id),
	foreign key(escuela_id) references escuela(id),
	--
	check (
		'preparatoria' = nivel and 1 <= grado and grado <= 6
		or
		'secundaria' = nivel and 1 <= grado and grado <= 3
	)
);
