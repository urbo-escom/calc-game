set client_encoding to 'UTF8';


-- El nombre sirve para la URL
drop table if exists juego cascade;
create table         juego (
	id              serial not null,
	nombre          varchar(127) not null,
	nombre_completo varchar(127) not null,
	--
	primary key(id),
	unique(nombre)
);
insert into juego (nombre, nombre_completo) values
	('sumas-consec',    'Simulador básico'),
	('sumas-2-cifras',  'Sumar números de 2 cifras'),
	('sumas-3-cifras',  'Sumar números de 3 cifras'),
	('series-fib',      'Series de fibonacci'),
	('series-fib-ext',  'Otras series de fibonacci'),
	('doblar-numeros',  'Doblar números'),
	('resta-2-cifras',  'Restar números de 2 cifras'),
	('resta-3-cifras',  'Restar números de 3 cifras'),
	('comp-100',        'Complemento a 100'),
	('mult-2x1-cifras', 'Multiplicaciones de 2 cifras por 1 cifra'),
	('mult-3x1-cifras', 'Multiplicaciones de 3 cifras por 1 cifra'),
	('mult-mxn-cifras', 'Multiplicación cruzada'),
	('conv-moneda',     'Euros a pesetas'),
	('div-entre-0.5',   'Dividir entre 0.5'),
	('div-entre-0.25',  'Dividir entre 0.25'),
	('div-entre-15',    'Dividir entre 15'),
	('div-entre-25',    'Dividir entre 25'),
	('div-entre-5',     'Dividir entre 5')
;


drop table if exists evaluacion cascade;
create table         evaluacion (
	id            serial not null,
	nombre        varchar(127) not null,
	intentos      smallint not null, -- máximo de intentos permitidos
	fecha_inicio  integer not null,
	fecha_termino integer not null,
	--
	primary key(id),
	--
	-- En teoría esto es una superllave y es redundante, en
	-- la tabla puntaje se entenderá por qué es necesaria
	-- esta restricción.
	--
	unique(id, intentos, fecha_inicio, fecha_termino),
	unique(nombre),
	check(0 < intentos),
	check(fecha_inicio < fecha_termino)
);


drop table if exists puntaje cascade;
create table         puntaje (
	evaluacion_id serial not null,
	alumno_id     serial not null,
	juego_id      serial not null,
	intentos      smallint not null, -- maximo de intentos (fijo)
	intento       smallint not null, -- intento actual
	--
	ejercicios    smallint not null, -- cuantos hizo en un intento
	aciertos      smallint not null, -- cuantos buenos
	fecha_inicio  integer not null,
	fecha_termino integer not null,
	fecha         integer default extract(epoch from current_timestamp),
	--
	constraint puntaje_valido
		check (0 <= ejercicios
		and 0 <= ejercicios and aciertos <= ejercicios),

	--
	-- ¿Cómo hacemos para aplicar la restricción de un número máximo de
	-- intentos por evaluación?
	--
	-- 1. evaluacion(id) es una llave, entonces evaluacion(id, intentos)
	-- es una superllave, esto los usamos para mantener la col intentos
	-- como fijo, intentos no puede tener otro valor más que el que le
	-- corresponde a la tabla evaluacion.
	--
	--         ---------------- --------------------------
	--            evaluacion             puntaje
	--         -----+---------- ---------------+----------
	--           id | intentos   evaluacion_id | intentos
	--         -----|---------- ---------------|----------
	--            0 |    1             0       |    1
	--            1 |    3             0       |    1  <- no puede ser otro
	--          ... |   ...            1       |    3     valor más que 1,
	--           99 |    8             0       |    5  <- esta fila es
	--                                 0       |    1     imposible (1 != 5)
	--
	-- Usando este concepto se puede aplicar también la restricción de
	-- las fechas usando la superllave evaluacion(id, ..., fechas...).
	--
	foreign key(evaluacion_id, intentos, fecha_inicio, fecha_termino)
		references evaluacion(id, intentos,
			fecha_inicio, fecha_termino),
	constraint puntaje_fecha_valida
		check (fecha_inicio <= fecha and fecha <= fecha_termino),

	--
	-- 2. intento indica el número de intento en cada fila y por eso debe
	-- ser único en cada juego de algún alumno y evaluación, con lo anterior
	-- y el número máximo de intentos constante podemos hacer la restricción
	-- de que 0 <= intento < intentos.
	--
	--         -----------------------------------------------------------
	--            Ejemplo de intentos, las filas marcadas con '!=' son
	--            imposibles por repetición, filas marcadas con '!!' son
	--            imposibles por violaciones a restricciones.
	--         -----------------------------------------------------------
	--                                   puntaje
	--         ---------------+-----------+----------+---------+----------
	--          evaluacion_id | alumno_id | juego_id | intento | intentos
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    0    |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    1    |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    2    |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    2!=  |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    3!!  |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |    3    |     4!!
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     0    |   -1!!  |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     0     |     2    |    2    |     3
	--         ---------------|-----------|----------|---------|----------
	--                1       |     1     |     2    |    2    |     3
	--         ---------------|-----------|----------|---------|----------
	--               99       |     0     |     0    |    5    |     8
	--
	unique(evaluacion_id, alumno_id, juego_id, intento),
	check(0 <= intento and intento < intentos),
	--
	-- La desventaja es que se debe obtener el total de intentos hechos
	-- antes de hacer un insert, pero esto se hace de todos modos a nivel
	-- de aplicación para reportar cuando ya se hicieron todos los intentos
	-- posibles.
	--
	foreign key(alumno_id) references alumno(id),
	foreign key(juego_id) references juego(id)
);
drop view if exists puntaje_maximo cascade;
create view         puntaje_maximo as
	select
		evaluacion_id   as evaluacion_id,
		alumno_id       as alumno_id,
		juego_id        as juego_id,
		count(intento)  as intentos_hechos,
		min(intentos)   as intentos_permitidos,
		avg(ejercicios) as ejercicios_promedio,
		max(aciertos)   as puntaje,
		max(fecha)      as fecha_ultimo_intento
	from puntaje
	group by
		evaluacion_id,
		alumno_id,
		juego_id;
