<?php
namespace escuela;

use \DomainException;

class escolaridad {
	private $nivel;
	private $grado;
	private $grupo;

	public function __construct($nivel, $grado, $grupo) {
		$nivel = strtolower($nivel);
		$grado = strtolower($grado);
		$grupo = strtolower($grupo);

		if (!strlen($nivel))
			throw new DomainException(
			"El nivel está vacío");

		if (!strlen($grado))
			throw new DomainException(
			"El grado está vacío");

		if (!strlen($grupo))
			throw new DomainException(
			"El grupo está vacío");

		if (!in_array($nivel, self::$niveles))
			throw new DomainException(
			"El nivel debe ser secundaria o preparatoria");

		if (!in_array($grado, self::$grados[$nivel]))
			throw new DomainException(
			"El grado debe ser correcto");

		if (!in_array($grupo, self::$grupos))
			throw new DomainException(
			"El grupo debe estar entre la A y la F");

		$this->nivel = $nivel;
		$this->grado = $grado;
		$this->grupo = $grupo;
	}

	public function get_nivel() { return $this->nivel; }
	public function get_grado() { return $this->grado; }
	public function get_grupo() { return $this->grupo; }

	public function __toString() {
		return $this->nivel." ".$this->grado.$this->grupo;
	}

	private static $from_string_regex = '/'.
		'(secundaria|preparatoria)\s*-*\s*'.
		'([1-6])\s*-*\s*(?:ro|do|to)?\s*-*\s*'.
		'([a-fA-Z])/i';

	public static function from_string($string) {
		$m = [];
		$s = preg_match(self::$from_string_regex, $string, $m);
		return new self($m[1], $m[2], $m[3]);
	}

	private static $niveles = [
		'secundaria',
		'preparatoria',
	];

	private static $grados = [
		'secundaria' => [
			'1',
			'2',
			'3',
		],
		'preparatoria' => [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
		],
	];

	private static $grupos = [
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
	];

}
