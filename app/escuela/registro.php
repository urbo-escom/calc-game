<?php
namespace escuela;

use \DomainException;

class registro {
	private $repo;

	public function __construct(repo $repo) {
		$this->repo = $repo;
	}

	public function obtener_con_id($id) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");
		return $this->repo->get_by_id($id);
	}


	public function obtener($escuela) {
		if (!strlen($escuela))
			throw new DomainException(
			"La escuela está vacía");
		return $this->repo->get($escuela);
	}


	public function agregar($escuela) {
		if (!strlen($escuela))
			throw new DomainException(
			"La escuela está vacía");

		if (null !== $this->repo->get($escuela))
			throw new DomainException(
			"La escuela '$escuela' ya existe");

		return $this->repo->add($escuela);
	}


	public function renombrar($escuela, $nuevo_nombre) {
		if (!strlen($escuela))
			throw new DomainException(
			"La escuela está vacía");

		/* race condition issues below!!! */

		if (null === $this->repo->get($escuela))
			return null;

		$this->repo->update($escuela, $nuevo_nombre);
	}

	public function eliminar($escuela) {
		if (!strlen($escuela))
			throw new DomainException(
			"La escuela está vacía");

		if (null === ($c = $this->repo->get($escuela)))
			return null;

		if (0 < ($a = $c->get_alumnos()))
			throw new DomainException(
			"La escuela '$escuela' ya tiene ".
			(1 < $a ? "$a alumnos":"$a alumno"));

		$this->repo->remove($escuela);
	}

}
