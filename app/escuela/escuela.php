<?php
namespace escuela;

use \DomainException;

class escuela {
	private $id;
	private $nombre;
	private $alumnos;

	public function __construct($id, $nombre, $alumnos = 0) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");

		if (!strlen($nombre))
			throw new DomainException(
			"El nombre está vacío");

		if ($alumnos < 0)
			throw new DomainException(
			"La cantidad de alumnos es negativa");

		$this->id      = intval($id);
		$this->nombre  = trim(preg_replace('/\s+/', ' ', $nombre));
		$this->alumnos = intval($alumnos);
	}

	public function get_id()      { return $this->id; }
	public function get_nombre()  { return $this->nombre; }
	public function get_alumnos() { return $this->alumnos; }

	public function __toString() {
		return "escuela:{$this->id}, ".
			"'{$this->nombre}', ".
			"{$this->alumnos} alumnos";
	}

}
