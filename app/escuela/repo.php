<?php
namespace escuela;

interface repo {
	public function get_all(repo_query $q):coleccion;
	public function add($escuela);
	public function get_by_id($id);
	public function get($escuela);
	public function update($escuela, $nuevo_nombre);
	public function remove($escuela);
}
