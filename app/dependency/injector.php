<?php
namespace dependency;

class injector {
	protected $container;
	protected $cache;

	public function __construct() {
		$this->container = [];
		$this->cache = [];
	}

	public function add($name, $f) {
		$this->container[$name] = $f;
	}

	public function get($name) {
		if (!isset($this->container[$name]))
			throw new DependencyNotFoundException($name);

		if (!is_callable($this->container[$name]))
			return $this->container[$name];

		if (isset($this->cache[$name]))
			return $this->cache[$name];

		return $this->cache[$name] = $this->container[$name]($this);
	}

	public function uncache($name) {
		if (!isset($this->cache[$name]))
			return null;
		$c = $this->cache[$name];
		unset($this->cache[$name]);
		return $c;
	}

}
