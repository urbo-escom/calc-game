<?php
namespace dependency;

$di = new injector();


$di->add('house', function($c) {
	echo "Building a house\n";
	$window = $c->get('house.window');
	$door   = $c->get('house.door');
	return new house($window, $door);
});

echo "SHOULD FAIL...\n";
try {
	$house = $di->get('house');
} catch (DependencyNotFoundException $e) {
	echo json_encode([
		'message'    => $e->getMessage(),
		'dependency' => $e->dependency,
	], JSON_PRETTY_PRINT), "\n";
}


$di->add('house.window', function($c) {
	echo "Building a window\n";
	$size = $c->get('house.window.size');
	return new window($size);
});

echo "SHOULD FAIL...\n";
try {
	$window = $di->get('house.window');
} catch (DependencyNotFoundException $e) {
	echo json_encode([
		'message'    => $e->getMessage(),
		'dependency' => $e->dependency,
	], JSON_PRETTY_PRINT), "\n";
}


$di->add('house.door', function($c) {
	echo "Building a door\n";
	$color = $c->get('house.door.color');
	return new door($color);
});

echo "SHOULD FAIL...\n";
try {
	$door = $di->get('house.door');
} catch (DependencyNotFoundException $e) {
	echo json_encode([
		'message'    => $e->getMessage(),
		'dependency' => $e->dependency,
	], JSON_PRETTY_PRINT), "\n";
}


class door {
	protected $color;
	public function __construct($color) { $this->color = $color; }
	public function __toString() { return "door (color {$this->color})"; }
}
class window {
	protected $size;
	public function __construct($size) { $this->size = $size; }
	public function __toString() { return "window (size {$this->size})"; }
}
class house {
	protected $window;
	protected $door;
	public function __construct($window, $door) {
		$this->window = $window;
		$this->door = $door;
	}
	public function __toString() {
		return "house ({$this->window}) ({$this->door})";
	}
}


echo "SHOULD NOT FAIL...\n";
$di->add('house.window.size', 20);
$di->add('house.door.color', 'white');
echo $house = $di->get('house'), "\n";


echo "SHOULD NOT recreate house...\n";
$di->add('house.window.size', 37);
echo $house = $di->get('house'), "\n";


echo "SHOULD recreate only house...\n";
$di->uncache('house');
echo $house = $di->get('house'), "\n";


echo "SHOULD NOT recreate house...\n";
$di->uncache('house.window');
echo $house = $di->get('house'), "\n";


echo "SHOULD recreate window and house...\n";
$di->uncache('house');
echo $house = $di->get('house'), "\n";
