<?php
namespace dependency;

use \Exception;

class DependencyNotFoundException extends Exception {
	public $dependency;
	public function __construct($dependency) {
		parent::__construct("Dependency $dependency not found");
		$this->dependency = $dependency;
	}
}
