<?php
namespace database;

use \Exception;

class DatabaseQueryException extends Exception {
	public $state;
	public $query;
	public function __construct($state, $errmsg, $query) {
		parent::__construct($errmsg);
		$this->query = $query;
		$this->state = $state;
	}
}
