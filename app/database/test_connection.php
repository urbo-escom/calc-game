<?php
namespace database;


echo "SHOULD fail...\n";
try {
	$conn_fail = new connection([
		'database' => '111111111111111',
		'username' => '333333333333333',
		'password' => '555555555555555',
	]);
} catch (DatabaseConnectionException $e) {
	echo "database_connection: ".$e->getMessage().
		", credentials: ".json_encode($e->credentials)."\n\n";
}


echo "SHOULD NOT fail!!!\n";
$conn = new connection($CONFIG['db']['postgres']);
echo "YES!!!\n";


$client = new client($conn);
$query = function($q) use ($client) {
	echo "$q => ";
	echo json_encode($r = $client->query($q), JSON_PRETTY_PRINT), "\n";
	return $r;
};
$select = function($q) use ($client) {
	echo json_encode($q, JSON_PRETTY_PRINT), " => ";
	echo json_encode($r = $client->select($q), JSON_PRETTY_PRINT), "\n";
	return $r;
};
$insert = function($q) use ($client) {
	echo json_encode($q, JSON_PRETTY_PRINT), " => ";
	echo json_encode($r = $client->insert($q), JSON_PRETTY_PRINT), "\n";
	return $r;
};
$update = function($q) use ($client) {
	echo json_encode($q, JSON_PRETTY_PRINT), " => ";
	echo json_encode($r = $client->update($q), JSON_PRETTY_PRINT), "\n";
	return $r;
};
$delete = function($q) use ($client) {
	echo json_encode($q, JSON_PRETTY_PRINT), " => ";
	echo json_encode($r = $client->delete($q), JSON_PRETTY_PRINT), "\n";
	return $r;
};


echo "Testing select on a missing table...\n";
try {
	$select([
		'select' => '*',
		'from' => 'foo',
		'limit' => 4,
	]);
} catch (DatabaseConstructedQueryException $e) {
	echo "error: ", json_encode([
		'message' => $e->getMessage(),
		'state'   => $e->state,
		'query'   => $e->query,
		'opt'     => $e->constructed_query,
	], JSON_PRETTY_PRINT), "\n";
}


echo "Testing select on a missing column...\n";
try {
	$select([
		'select' => 'n*n',
	]);
} catch (DatabaseConstructedQueryException $e) {
	echo "error: ", json_encode([
		'message' => $e->getMessage(),
		'state'   => $e->state,
		'query'   => $e->query,
		'opt'     => $e->constructed_query,
	], JSON_PRETTY_PRINT), "\n";
}


echo "Testing select on a missing function...\n";
try {
	$select([
		'select' => 'noww()',
	]);
} catch (DatabaseConstructedQueryException $e) {
	echo "error: ", json_encode([
		'message' => $e->getMessage(),
		'state'   => $e->state,
		'query'   => $e->query,
		'opt'     => $e->constructed_query,
	], JSON_PRETTY_PRINT), "\n";
}


echo "The following queries should not fail...\n";
$select([
	'select' => 'cast(extract(epoch from now()) as int) as now',
]);
$query('create temporary table foo (i serial primary key, j smallint)');
$insert([
	'insert' => 'foo',
	'column' => ['i', 'j'],
	'values' => [
		['j' => 101],
		['j' => 102],
		['j' => 103],
		['j' => 104],
	],
	'returning' => ['i', 'j', 'i*j as "i x j"'],
]);
$insert([
	'insert' => 'foo',
	'column' => 'j',
	'query' => [
		'select' => 'j*2',
		'from' => 'foo',
	],
	'returning' => ['i', 'j', 'i*j as "i x j"'],
]);

$all = $select(['select' => 'i, j', 'from' => 'foo']);
foreach ($all as &$v) {
	if (0 === $v['i']%2)
		$v['j'] += 1000;
	else
		$v['j'] -= 1000;
}
unset($v);
foreach ($all as $k => $v) {
	echo "\nUpdate i = {$v['i']}\n";
	$update([
		'update' => 'foo',
		'column' => 'j',
		'values' => ['j' => $v['j']],
		'where' => "i = {$v['i']}",
		'returning' => 'i, j',
	]);
}
