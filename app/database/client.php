<?php
namespace database;

use \InvalidArgumentException;

/*
 * query: select_col
 *         [from_list]
 *         [where]
 *         [group_by]
 *         [having]
 *         [limit l]
 *         [offset o]
 *
 * select_col: select_col_item [, select_col_item]...
 * select_col_item:
 *         string [as alias] |
 *         query as alias
 *
 * from_list: table_item [, table_item]...
 * table_item:
 *         '(' table_item ')' |
 *         string [as alias] |
 *         query as alias |
 *         table_item join table_item [on cond]
 *
 * where: cond
 * cond: expr [[AND|OR] expr]...
 * expr: ...
 *
 * group_by: col_list
 * col_list: col_item [, col_item]...
 *
 * having: cond
 *
 *
 * insert: insert into table [col_list]
 *         (default values|values (col_list)[, (col_list)]...|query)
 *         [returning col_list]
 *
 * update: update table [as alias]
 *         set col_list = col_list
 *         [from_list]
 *         [where]
 *         [returning col_list]
 *
 * delete: delete from table [as alias]
 *         [where]
 *         [returning col_list]
 *
 */

class client {
	protected $conn;

	public function __construct(connection $conn) {
		$this->conn = $conn;
	}

	public function query($query) {
		return $this->conn->query($query);
	}

	public function query_dispatch($type, $opt) {
		try {
			switch ($type) {
			case 'select':
				$r = $this->query($this->create_select($opt));
				return $this->conn->fetch_all($r);

			case 'insert':
				$r = $this->query($this->create_insert($opt));
				if (isset($opt['returning']))
					return $this->conn->fetch_all($r);
				return $this->conn->affected_rows($r);

			case 'update':
				$r = $this->query($this->create_update($opt));
				if (isset($opt['returning']))
					return $this->conn->fetch_all($r);
				return $this->conn->affected_rows($r);

			case 'delete':
				$r = $this->query($this->create_delete($opt));
				if (isset($opt['returning']))
					return $this->conn->fetch_all($r);
				return $this->conn->affected_rows($r);
			}
		} catch (DatabaseQueryException $e) {
			throw new DatabaseConstructedQueryException($e, $opt);
		}
	}

	public function select($opt) {
		return $this->query_dispatch('select', $opt);
	}

	public function insert($opt) {
		return $this->query_dispatch('insert', $opt);
	}

	public function update($opt) {
		return $this->query_dispatch('update', $opt);
	}

	public function delete($opt) {
		return $this->query_dispatch('delete', $opt);
	}


	/* Helper functions beware!! */

	public function escape($o) {
		if (is_float($o)
		||  is_int($o))
			return "$o";

		if (is_bool($o))
			return $o ? 'true':'false';

		if (is_null($o))
			return 'null';

		if (is_string($o))
			return $this->conn->escape_literal($o);

		if (is_array($o))
			return array_map(function($i) {
				return $this->escape($i);
			}, $o);

		throw new InvalidArgumentException(
			"Unsupported type '".gettype($of)."'");
	}

	public function col_list($cols) {
		if (!is_array($cols))
			$cols = [$cols];
		return array_reduce($cols, function($carry, $item) {
			if (null === $carry)
				return $item;
			return $carry.", ".$item;
		});
	}

	public function boolean_reduce($expr) {
		if (!is_array($expr))
			return "$expr";
		if ('OR' === $expr[0]) {
			$s = $this->boolean_reduce($expr[1]);
			for ($i = 2; $i < count($expr); $i++)
				$s .= ' OR '.$this->boolean_reduce($expr[$i]);
			return "($s)";
		}
		if ('AND' === $expr[0]) {
			$s = $this->boolean_reduce($expr[1]);
			for ($i = 2; $i < count($expr); $i++)
				$s .= ' AND '.$this->boolean_reduce($expr[$i]);
			return "($s)";
		}
		array_unshift($expr, 'AND');
		return $this->boolean_reduce($expr);
	}

	public function col_value_reduce($cols, $values) {
		if (!is_array($cols))
			$cols = [$cols];
		if (!is_array($values))
			$values = [];
		if (!isset($values[0]) || !is_array($values[0]))
			$values = [$values];

		$q = null;
		foreach ($values as $row) {
			$reduce = function($carry, $item) use ($row) {
				if (!isset($row[$item]))
					$v = 'default';
				else
					$v = $row[$item];
				if (null === $carry)
					return $v;
				return $carry.", ".$v;
			};
			$t = "(".array_reduce($cols, $reduce).")";
			if (null === $q) $q  = "$t";
			else             $q .= ", $t";
		}
		return $q;
	}

	protected function tab($n) {
		$s = '';
		for ($i = 0; $i < $n; $i++)
			$s .= "\t";
		return $s;
	}

	public function select_col($s, $tab = 0) {
		if (!is_array($s))
			$s = [$s];
		$q = null;
		foreach ($s as $k => $v) {
			if (is_string($k) && !is_array($v))
				$c = "$v as $k";
			else if (!is_array($v))
				$c = "$v";
			else if (is_int($k) && is_array($v))
				throw new InvalidArgumentException(
					"subqueries must have aliases");
			else if (is_array($v))
				$c = "({$this->create_select($v, $tab + 1)}".
					") as $k";

			if (null === $q) $q  =  "\n".$this->tab($tab).$c;
			else             $q .= ",\n".$this->tab($tab).$c;
		}
		return $q;
	}

	public function table_item($k, $v, $tab) {
		if (is_int($k) && !is_array($v))
			return "$v";
		if (!is_array($v))
			return "$v as $k";
		if (is_int($k) && isset($v['select']))
			throw new InvalidArgumentException(
				"subqueries must have aliases");
		if (isset($v['select']))
			return "(\n".$this->tab($tab + 1).
				$this->create_select($v, $tab + 1).
				") as $k";

		$type = null;
		$on = null;
		if ('JOIN' === $v[0])
			$type = 'JOIN';
		if ('INNER_JOIN' === $v[0])
			$type = 'INNER JOIN';
		if ('LEFT_JOIN' === $v[0])
			$type = 'LEFT JOIN';
		if ('RIGHT_JOIN' === $v[0])
			$type = 'RIGHT JOIN';
		if ('FULL_JOIN' === $v[0])
			$type = 'FULL JOIN';
		if ('CROSS_JOIN' === $v[0])
			$type = 'CROSS JOIN';
		if (isset($v['ON'])) {
			$on = $v['ON'];
			unset($v['ON']);
		}

		if (null === $type) {
			array_unshift($v, 'CROSS_JOIN');
			return $this->table_item($k, $v, $tab);
		}

		$q = null;
		array_shift($v);
		foreach ($v as $kk => $vv) {
			if (is_int($kk))
				$c = "$vv";
			else if (!is_array($vv))
				$c = "$kk ON $vv";
			else if (isset($vv['select']))
				$c = "(\n".$this->tab($tab + 1).
					$this->create_select($vv, $tab + 1).
					") as $kk";
			else if (false
			|| isset($vv['JOIN'])
			|| isset($vv['INNER_JOIN'])
			|| isset($vv['LEFT_JOIN'])
			|| isset($vv['RIGHT_JOIN'])
			|| isset($vv['FULL_JOIN'])
			|| isset($vv['CROSS_JOIN']))
				$c = "({$this->table_item(0, $vv)}) as $kk";
			else if (isset($vv['ON']))
				$c = "$kk ON ".$this->boolean_reduce($vv['ON']);

			if (null === $q) $q  = "\n".$this->tab($tab).$c;
			else             $q .= "\n".$this->tab($tab)."$type $c";
		}
		if (null !== $on)
			$q .= " ON ".$this->boolean_reduce($on);
		return $q;
	}

	public function from_list($f, $tab = 0) {
		if (!is_array($f))
			$f = [$f];
		$q = null;
		foreach ($f as $k => $v) {
			$i = $this->table_item($k, $v, $tab);
			if (null === $q) $q  =   "$i";
			else             $q .= ", $i";
		}
		return $q;
	}

	public function create_select($opt, $tab = 0) {
		$q = 'SELECT '.$this->select_col($opt['select'], $tab + 1);

		if (isset($opt['from']))
			$q .= "\n".$this->tab($tab).
				'FROM '.$this->from_list(
					$opt['from'], $tab + 1);

		if (isset($opt['where']))
			$q .= "\n".$this->tab($tab).
				'WHERE '.$this->boolean_reduce($opt['where']);

		if (isset($opt['group_by']))
			$q .= "\n".$this->tab($tab).
				'GROUP BY '.$this->col_list($opt['group_by']);

		if (isset($opt['having']))
			$q .= "\n".$this->tab($tab).
				'HAVING '.$this->boolean_reduce($opt['having']);

		if (isset($opt['order_by']))
			$q .= "\n".$this->tab($tab).
				'ORDER BY '.$this->col_list($opt['order_by']);

		if (isset($opt['limit']))
			$q .= "\n".$this->tab($tab + 1).
				'LIMIT '.$opt['limit'];
		if (isset($opt['offset']))
			$q .= "\n".$this->tab($tab + 1).
				'OFFSET '.$opt['offset'];

		return $q;
	}

	public function create_insert($opt, $tab = 0) {
		$q = 'INSERT INTO '.$opt['insert'];
		$q .= "({$this->col_list($opt['column'])})";

		if (isset($opt['query']))
			$q .= "\n".$this->tab($tab + 1).
				$this->create_select($opt['query']);
		else if (isset($opt['values']))
			$q .= "\n".$this->tab($tab + 1).
				' VALUES '.$this->col_value_reduce(
				$opt['column'], $opt['values']);
		else
			$q .= "\n".$this->tab($tab + 1).
				' DEFAULT VALUES';

		if (isset($opt['returning']))
			$q .= "\n".$this->tab($tab + 1).
				' RETURNING '.$this->col_list(
				$opt['returning']);

		return $q;
	}

	public function create_update($opt = array(), $tab = 0) {
		$q = 'UPDATE '.$opt['update'];
		$q .= " SET ({$this->col_list($opt['column'])})";

		if (!isset($opt['values']))
			$opt['values'] = [];

		$q .= " = ".$this->col_value_reduce($opt['column'],
			$opt['values']);

		if (isset($opt['from']))
			$q .= "\n".$this->tab($tab).
				'FROM '.$this->from_list(
					$opt['from'], $tab + 1);

		if (isset($opt['where']))
			$q .= "\n".$this->tab($tab + 1).
				'WHERE '.$this->boolean_reduce($opt['where']);

		if (isset($opt['returning']))
			$q .= "\n".$this->tab($tab + 1).
				'RETURNING '.$this->col_list(
				$opt['returning']);

		return $q;
	}

	public function create_delete($opt = array(), $tab = 0) {
		$q = 'DELETE FROM '.$opt['delete'];

		if (isset($opt['where']))
			$q .= "\n".$this->tab($tab + 1).
				'WHERE '.$this->boolean_reduce($opt['where']);

		if (isset($opt['returning']))
			$q .= "\n".$this->tab($tab + 1).
				'RETURNING '.$this->col_list(
				$opt['returning']);

		return $q;
	}

}
