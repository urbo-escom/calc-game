<?php
namespace database;

use \InvalidArgumentException;

class connection {
	public function __get($p) {
		if ('last_error' !== $p)
			throw new InvalidArgumentException(
				"No $p property in class ".__CLASS__);
		return 'No error';
	}
	public function __set($k, $v) {
		throw new InvalidArgumentException(
			"You should not set $k or any property in ".__CLASS__);
	}
	public function __call($m, $p) {
		if (!in_array($m, array('query', 'fetch_all')))
			throw new InvalidArgumentException(
				"No method $m in class ".__CLASS__);
		if ('fetch_all' === $m)
			return "{$p[0]};\n\n";
		if ('query' === $m)
			return "{$p[0]}";
	}
}

$db_conn = new connection();
$db_client = new client($db_conn);

echo $db_client->query("select * from escuela"), "\n\n";

function echo_select($c, $q) {
	echo json_encode($q, JSON_PRETTY_PRINT), "\n";
	echo $c->select($q);
}

function echo_insert($c, $q) {
	echo json_encode($q, JSON_PRETTY_PRINT), "\n";
	echo $c->insert($q);
}

function echo_update($c, $q) {
	echo json_encode($q, JSON_PRETTY_PRINT), "\n";
	echo $c->update($q);
}

function echo_delete($c, $q) {
	echo json_encode($q, JSON_PRETTY_PRINT), "\n";
	echo $c->delete($q);
}

echo_select($db_client, [
	'select' => 'generate_series(1, 3) as n',
]);

echo_select($db_client, [
	'select' => ['foo as bar', 'quz'],
]);

echo_select($db_client, [
	'select' => [
		'foo as bar',
		'quz' => '(select count(*) from maj)',
		'qux' => [
			'select' => 'max(id)',
			'from' => 'foo',
			'group_by' => 'id',
		],
		'quuz' => [
			'select' => 'min(id)',
			'from' => 'foo',
			'group_by' => 'id',
			'limit' => 1,
		],
	],
	'from' => 'foo',
]);

echo_select($db_client, [
	'select' => ['foo as bar', 'quz'],
	'from' => 'foobar',
	'where' => 'foo = 23',
	'limit' => 4,
	'offset' => 8,
]);

echo_select($db_client, [
	'select' => ['foo as bar', 'quz'],
	'from' => 'foobar',
	'where' => [
		'1 <= foo',
		'foo <= 20',
	],
	'limit' => 4,
	'offset' => 8,
]);

echo_select($db_client, [
	'select' => ['sum(foo) as bar', 'quz'],
	'from' => 'foobar',
	'group_by' => [
		'quz',
		'quux',
	],
	'limit' => 4,
	'offset' => 8,
]);

echo_select($db_client, [
	'select' => ['foo as bar', 'quz'],
	'from' => 'foobar',
	'where' => [
		'1 <= foo',
		'foo <= 20',
	],
	'group_by' => 'id',
	'having' => [
		'OR',
		'id < 24',
		'30 < id',
		['id <> 300', 'id <> 400'],
		[
			'AND',
			'id + 200 < 30',
			'id/200 = 30',
		],
	],
	'limit' => 4,
	'offset' => 8,
]);

echo_select($db_client, [
	'select' => '*',
	'from' => ['a', 'b', 'c'],
]);

echo_select($db_client, [
	'select' => '*',
	'from' => ['a', ['b', 'c']],
]);

echo_select($db_client, [
	'select' => '*',
	'from' => [['a'], ['b', 'c']],
]);

echo_select($db_client, [
	'select' => '*',
	'from' => [['a', 'b'], 'c'],
]);

echo_select($db_client, [
	'select' => '*',
	'from' => [['a', 'b', 'c']],
]);

echo_select($db_client, [
	'select' => '*',
	'from' => [
		'a',
		'b' => '(select * from foo)',
		'c' => [
			'select' => ['x', 'y'],
			'from' => 'bar',
		],
		'd' => [
			'JOIN',
			'table1 as t1',
			'table3 as t2',
			'table5 as t3',
			'ON' => 't1.id = t2.id and t2.id = t3.id',
		],
		[
			'LEFT_JOIN',
			'table0',
			'table6' => 'table6.id = 25',
			'table2' => ['ON' => 'table2.id = t1.id'],
			'table2 ON table2.id < 34' => [
				'select' => '*',
				'from' => 'foobar',
			],
			'alias1' => [
				'JOIN',
				'table2 as v',
				'table3 as w',
				'ON' => 'v.id <> w.id',
			],
			'table4 as t4' => [
				'ON' => [
					't2.id = t4.id',
					't2.id < 30',
					[
						'OR',
						'50 <= t2.id',
						't2.id < t2.id',
					],
				],
			],
		],
	],
]);


echo_insert($db_client, [
	'insert' => 'foo',
	'column' => ['i', 'j'],
	'values' => ['j' => 3],
	'returning' => ['i', 'j', "i*j as 'i x j'"],
]);

echo_insert($db_client, [
	'insert' => 'foo',
	'column' => ['i', 'j'],
	'values' => [
		['j' => 101],
		['j' => 102],
		['j' => 103],
		['j' => 104],
	],
	'returning' => ['i', 'j', "i*j as 'i x j'"],
]);

echo_insert($db_client, [
	'insert' => 'foo',
	'column' => ['i', 'j'],
	'query' => [
		'select' => 'j*2',
		'from' => 'foo',
	],
	'returning' => ['i', 'j', "i*j as 'i x j'"],
]);

echo_update($db_client, [
	'update' => 'foo',
	'column' => 'j',
	'values' => ['j' => 1],
	'where' => 'id = 3',
	'returning' => 'i, j',
]);

echo_update($db_client, [
	'update' => 'foo',
	'column' => ['j', 'k'],
	'values' => ['j' => 1],
	'where' => 'id = 3',
	'returning' => 'i, j',
]);

echo_delete($db_client, [
	'delete' => 'foo',
	'where' => ['id = 3'],
	'returning' => ['i', 'j'],
]);
