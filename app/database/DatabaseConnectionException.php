<?php
namespace database;

use \Exception;

class DatabaseConnectionException extends Exception {
	public $credentials;
	public function __construct($credentials) {
		parent::__construct("Could not connect to postgresql DB with ".
			json_encode($credentials, 0
			| JSON_PRETTY_PRINT
			| JSON_UNESCAPED_SLASHES
			| JSON_UNESCAPED_UNICODE));
		$this->credentials = $credentials;
	}
}
