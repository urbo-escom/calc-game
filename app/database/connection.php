<?php
namespace database;

class connection {
	protected $cred;
	protected $conn;

	public function __construct($opt = array()) {
		$s = '';

		if (isset($opt['database']))
			$s .= " dbname={$opt['database']}";

		if (isset($opt['username']))
			$s .= " user={$opt['username']}";

		if (isset($opt['password']))
			$s .= " password={$opt['password']}";

		$this->cred = $opt;
		$this->conn = @pg_connect($s);
		if (false !== $this->conn)
			return;

		throw new DatabaseConnectionException($this->cred);
	}

	public function query($query) {
		if (false === pg_send_query($this->conn, $query))
			throw new DatabaseConnectionException($this->cred);

		if (false === ($res = pg_get_result($this->conn)))
			throw new DatabaseConnectionException($this->cred);

		$state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
		if (0 === strlen($state))
			return $res;
		$errmsg = pg_result_error($res);
		throw new DatabaseQueryException($state, $errmsg, $query);
	}

	public function fetch_all($res) {
		if (-1 === ($n = pg_num_rows($res)))
			throw new DatabaseConnectionException($this->cred);
		if (0 === $n)
			return [];
		return pg_fetch_all($res);
	}

	public function affected_rows($res) {
		return pg_affected_rows($res);
	}

	public function escape_literal($str) {
		return pg_escape_literal($this->conn, $str);
	}

}
