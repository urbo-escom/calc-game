<?php
namespace database;

class DatabaseConstructedQueryException extends DatabaseQueryException {
	public $constructed_query;
	public function __construct(DatabaseQueryException $e,
			$constructed_query) {
		parent::__construct($e->state, $e->getMessage(), $e->query);
		$this->constructed_query = $constructed_query;
	}
}
