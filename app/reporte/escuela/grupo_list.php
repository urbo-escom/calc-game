<?php
namespace reporte\escuela;

use \database\client;

use \escuela\escolaridad;

class grupo_list {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private $select_grupo = [
		'select' => [
			'n.nivel as nivel',
			'n.grado as grado',
			'a.grupo as grupo',
			'coalesce(avg(p.aciertos), 0.0) as promedio',
			'coalesce(max(p.aciertos), 0) as puntaje',
		],
		'from' => [[
			'LEFT_JOIN',
			'escuela_nivel_grado as n',
			'alumno  as a' =>
				'a.nivel = n.nivel and a.grado = n.grado',
			'puntaje as p' =>
				'a.id = p.alumno_id',
		]],
		'group_by' => [
			'a.escuela_id',
			'n.nivel',
			'n.grado',
			'a.grupo',
		],
		'order_by' => [
			'n.nivel',
			'n.grado',
			'a.grupo',
		],
	];

	private $select_count = [
		'select' => [
			'n.nivel as nivel',
			'n.grado as grado',
			'a.grupo as grupo',
			'count(a.id) as alumnos',
		],
		'from' => [[
			'LEFT_JOIN',
			'escuela_nivel_grado as n',
			'alumno  as a' =>
				'a.nivel = n.nivel and a.grado = n.grado',
		]],
		'group_by' => [
			'a.escuela_id',
			'n.nivel',
			'n.grado',
			'a.grupo',
		],
		'order_by' => [
			'n.nivel',
			'n.grado',
			'a.grupo',
		],
	];

	public function get_list(int $esc_id, string $nivel, string $grado) {
		$s1 = array_merge([], $this->select_grupo);
		$s1['having'] = [
			'a.escuela_id = '.$esc_id,
			'n.nivel = '.$this->db->escape($nivel),
			'n.grado = '.$this->db->escape($grado),
		];

		$s2 = array_merge([], $this->select_count);
		$s2['having'] = [
			'a.escuela_id = '.$esc_id,
			'n.nivel = '.$this->db->escape($nivel),
			'n.grado = '.$this->db->escape($grado),
		];

		return [
			'grupos' => $this->db->select($s1),
			'alumnos' => $this->db->select($s2),
		];
	}

}
