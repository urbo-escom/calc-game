<?php
namespace reporte\escuela;

use \database\client;

use \escuela\escolaridad;

class nivel_list {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private $select_grupo = [
		'select' => [
			'n.nivel as nivel',
			'n.grado as grado',
			'count(*) as alumnos',
		],
		'from' => [[
			'LEFT_JOIN',
			'escuela_nivel_grado as n',
			'alumno  as a' =>
				'a.nivel = n.nivel and a.grado = n.grado',
		]],
		'group_by' => [
			'a.escuela_id',
			'n.nivel',
			'n.grado',
		],
		'order_by' => [
			'n.nivel',
			'n.grado',
		],
	];

	private $select_puntaje = [
		'select' => [
			'n.nivel as nivel',
			'n.grado as grado',
			'coalesce(avg(p.aciertos), 0.0) as promedio',
			'coalesce(max(p.aciertos), 0) as puntaje',
		],
		'from' => [[
			'LEFT_JOIN',
			'escuela_nivel_grado as n',
			'alumno  as a' =>
				'a.nivel = n.nivel and a.grado = n.grado',
			'puntaje as p' =>
				'a.id = p.alumno_id',
		]],
		'group_by' => [
			'a.escuela_id',
			'n.nivel',
			'n.grado',
		],
		'order_by' => [
			'n.nivel',
			'n.grado',
		],
	];

	public function get_list(int $esc_id) {
		$s1 = array_merge([], $this->select_grupo);
		$s1['having'] = 'a.escuela_id = '.$esc_id;
		$s2 = array_merge([], $this->select_puntaje);
		$s2['having'] = 'a.escuela_id = '.$esc_id;
		return [
			'grupos' => $this->db->select($s1),
			'puntajes' => $this->db->select($s2)
		];
	}

}
