<?php
namespace reporte\escuela;

use \database\client;

use \escuela\escolaridad;

class evaluacion {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private $select_alumno = [
		'select' => [
			'a.id as id',
			'a.email as email',
			'a.nombre as nombre',
			'a.apellido_pat as apellido_pat',
			'a.apellido_mat as apellido_mat',
			'coalesce(max(p.aciertos), 0) as puntaje',
		],
		'from' => [[
			'LEFT_JOIN',
			'alumno as a',
			'puntaje as p' =>
				'a.id = p.alumno_id',
		]],
		'group_by' => [
			'a.id',
			'a.email',
			'a.nombre',
			'a.apellido_pat',
			'a.apellido_mat',
			'p.evaluacion_id',
		],
		'order_by' => [
			'a.nombre',
			'a.apellido_pat',
			'a.apellido_mat',
			'a.email',
		],
	];

	public function get_list(int $esc_id, int $eva_id,
			string $nivel,
			string $grado,
			string $grupo) {
		$s3 = array_merge([], $this->select_alumno);
		$s3['having'] = [
			'a.escuela_id = '.$esc_id,
			[
				'OR',
				'p.evaluacion_id is null',
				'p.evaluacion_id = '.$eva_id,
			],
			'a.nivel = '.$this->db->escape($nivel),
			'a.grado = '.$this->db->escape($grado),
			'a.grupo = '.$this->db->escape($grupo),
		];
		return $this->db->select($s3);
	}

}
