<?php
namespace reporte\escuela;

use \database\client;

use \escuela\escolaridad;

class grupo {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private $select_alumno = [
		'select' => [
			'a.id as id',
			'a.email as email',
			'a.nombre as nombre',
			'a.apellido_pat as apellido_pat',
			'a.apellido_mat as apellido_mat',
		],
		'from' => 'alumno as a',
		'order_by' => [
			'a.nombre',
			'a.apellido_pat',
			'a.apellido_mat',
			'a.email',
		],
	];

	public function get_list(int $esc_id,
			string $nivel,
			string $grado,
			string $grupo) {
		$s3 = array_merge([], $this->select_alumno);
		$s3['where'] = [
			'a.escuela_id = '.$esc_id,
			'a.nivel = '.$this->db->escape($nivel),
			'a.grado = '.$this->db->escape($grado),
			'a.grupo = '.$this->db->escape($grupo),
		];

		return $this->db->select($s3);
	}

}
