<?php
namespace reporte\alumno;

use \juego\repo as jrepo;
use \puntaje\repo as prepo;

class evaluacion {
	private $jrepo;
	private $prepo;

	public function __construct(jrepo $j, prepo $p) {
		$this->jrepo = $j;
		$this->prepo = $p;
	}

	public function get_reporte($evaluacion_id, $alumno_id) {
		$juegos = $this->jrepo->get_all();
		$puntajes = [];
		foreach ($juegos as $j)
			$puntajes[] = [
				'juego' => $j,
				'puntaje' => $this->prepo->get_max(
					$evaluacion_id,
					$alumno_id,
					$j->get_id()),
			];
		return $puntajes;
	}

}
