<?php
namespace codigo;

interface repo {
	public function get_all(repo_query $q):coleccion;
	public function get_by_id($id);
	public function add($codigo);
	public function get($codigo);
	public function remove($codigo);
}
