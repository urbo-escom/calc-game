<?php
namespace codigo;

use \DomainException;

class registro {
	private $repo;

	public function __construct(repo $repo) {
		$this->repo = $repo;
	}

	public function obtener($codigo) {
		if (!strlen($codigo))
			throw new DomainException(
			"El código está vacío");
		return $this->repo->get($codigo);
	}


	public function agregar($codigo) {
		if (!strlen($codigo))
			throw new DomainException(
			"El código está vacío");

		if (null !== $this->repo->get($codigo))
			throw new DomainException(
			"El código '$codigo' ya existe");

		return $this->repo->add($codigo);
	}


	public function eliminar($codigo) {
		if (!strlen($codigo))
			throw new DomainException(
			"El código está vacío");

		if (null === ($c = $this->repo->get($codigo)))
			return null;

		if (null !== ($e = $c->get_email()))
			throw new DomainException(
			"El código '$codigo' ya está asociado a '$e'");

		return $this->repo->remove($codigo);
	}

}
