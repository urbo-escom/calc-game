<?php
namespace codigo;

use \DomainException;

class codigo {
	private $id;
	private $codigo;
	private $email;

	public function __construct($id, $codigo, $email = null) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");

		if (!strlen($codigo))
			throw new DomainException(
			"El codigo está vacío");

		if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL))
			throw new DomainException(
			"El email '{$email}' parece incorrecto");

		$this->id     = intval($id);
		$this->codigo = $codigo;
		$this->email  = 0 < strlen($email) ? $email:null;
	}

	public function get_id()     { return $this->id; }
	public function get_codigo() { return $this->codigo; }
	public function get_email()  { return $this->email; }

	public function __toString() {
		return "codigo:{$this->id}, ".
			"{$this->codigo}, ".
			($this->email ?
				"{$this->email}":
				"no usado"
			);
	}

}
