<?php
namespace alumno;

use \DomainException;

class alumno {
	private $id;
	private $codigo_id;
	private $escuela_id;
	private $datos;

	public function __construct($id, $codigo_id, $escuela_id,
			alumno_datos $datos) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");

		if (!strlen($codigo_id))
			throw new DomainException(
			"El codigo_id está vacío");

		if (!strlen($escuela_id))
			throw new DomainException(
			"El escuela_id está vacío");

		if (null === $datos)
			throw new DomainException(
			"Los datos están vacíos");

		$this->id         = intval($id);
		$this->codigo_id  = intval($codigo_id);
		$this->escuela_id = intval($escuela_id);
		$this->datos      = $datos;
	}

	public function get_id()         { return $this->id; }
	public function get_codigo_id()  { return $this->codigo_id; }
	public function get_escuela_id() { return $this->escuela_id; }
	public function get_datos()      { return $this->datos; }

	public function __toString() {
		return "alumno:{$this->id}, ".
			"codigo_id:{$this->codigo_id}, ".
			"escuela_id:{$this->escuela_id}, ".
			"datos:{$this->datos}";
	}

}
