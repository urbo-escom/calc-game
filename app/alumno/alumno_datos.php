<?php
namespace alumno;

use \escuela\escolaridad;

use \BadMethodCallException;
use \DomainException;

class alumno_datos {
	private $email;
	private $password;
	private $nombre;
	private $apellido_pat;
	private $apellido_mat;
	private $escolaridad;

	protected function __construct($a) {
		if (!isset($a['email']) || !$a['email'])
			throw new DomainException(
			"Falta el email o está vacío");

		if (!isset($a['password']) || !$a['password'])
			throw new DomainException(
			"Falta la contraseña está vacía");

		if (!isset($a['nombre']) || !$a['nombre'])
			throw new DomainException(
			"Falta el nombre o está vacío");

		if (!isset($a['apellido_pat']) || !$a['apellido_pat'])
			throw new DomainException(
			"Falta el apellido paterno o está vacío");

		if (!isset($a['apellido_mat']) || !$a['apellido_mat'])
			throw new DomainException(
			"Falta el apellido materno o está vacío");

		if (!isset($a['escolaridad'])
		|| !($a['escolaridad'] instanceof escolaridad))
			throw new DomainException(
			"Falta la escolaridad o está vacía");

		$this->email        = $a['email'];
		$this->password     = $a['password'];
		$this->nombre       = $a['nombre'];
		$this->apellido_pat = $a['apellido_pat'];
		$this->apellido_mat = $a['apellido_mat'];
		$this->escolaridad  = $a['escolaridad'];
	}

	public function get_email()        { return $this->email; }
	public function get_password()     { return $this->password; }
	public function get_nombre()       { return $this->nombre; }
	public function get_apellido_pat() { return $this->apellido_pat; }
	public function get_apellido_mat() { return $this->apellido_mat; }
	public function get_escolaridad()  { return $this->escolaridad; }

	public function get_nombre_completo() {
		return $this->nombre.' '.
			$this->apellido_pat.' '.
			$this->apellido_mat;
	}

	public function __toString() {
		return "{$this->email} {$this->password}, ".
			"nombre {$this->nombre} ".
				"{$this->apellido_pat} ".
				"{$this->apellido_mat}, ".
			"{$this->escolaridad}";
	}

	public static function builder(alumno_datos $datos = null) {
		return new class($datos) {
			protected $a = [
				'email' => null,
				'password' => null,
				'nombre' => null,
				'apellido_pat' => null,
				'apellido_mat' => null,
				'escolaridad' => null,
			];

			public function __construct($q) {
				if (null === $q)
					return;
				$b = &$this->a;
				$b['email'] = $q->get_email();
				$b['password'] = $q->get_password();
				$b['nombre'] = $q->get_nombre();
				$b['apellido_pat'] = $q->get_apellido_pat();
				$b['apellido_mat'] = $q->get_apellido_mat();
				$b['escolaridad'] = $q->get_escolaridad();
			}

			public function build() {
				return new class($this->a) extends
						alumno_datos {
					public function __construct($a) {
						parent::__construct($a);
					}
				};
			}

			public function __call($k, $vs) {
				if (!in_array($k, array_keys($this->a)))
					throw new BadMethodCallException();
				$this->a[$k] = $vs[0];
				return $this;
			}

		};
	}

}
