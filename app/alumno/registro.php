<?php
namespace alumno;

use \codigo\repo  as codigo_repo;
use \escuela\repo as escuela_repo;

use \DomainException;

class registro {
	private $repo;
	private $crepo;
	private $erepo;

	public function __construct(repo $repo,
			codigo_repo $crepo,
			escuela_repo $erepo) {
		$this->repo = $repo;
		$this->crepo = $crepo;
		$this->erepo = $erepo;
	}


	public function verificar_passwords($p1, $p2) {
		$enc1 = password_hash($p1, PASSWORD_BCRYPT, ['cost' => 10]);
		if (!password_verify($p2, $enc1))
			throw new DomainException(
			"Las constraseñas no coinciden");
		return true;
	}


	public function autenticar($email, $password) {
		$alumno = $this->repo->get($email);
		if (!$alumno)
			throw new DomainException(
			"No existe el alumno con email '{$email}'");

		if (!password_verify($password, $alumno->get_datos()
				->get_password()))
			throw new DomainException(
			"La constraseña es incorrecta");

		return $alumno;
	}


	public function agregar($codigo, $escuela, alumno_datos $datos) {
		if (!strlen($codigo)
		|| !($c = $this->crepo->get($codigo))
		|| null !== $c->get_email())
			throw new DomainException(
			"El código '$codigo' es incorrecto");

		if (!strlen($escuela)
		|| !($e = $this->erepo->get($escuela)))
			throw new DomainException(
			"La escuela '$escuela' no existe");

		if ($u = $this->obtener($email = $datos->get_email()))
			throw new DomainException(
			"El email '$email' ya existe");

		$datos = alumno_datos::builder($datos)
			->password(password_hash(
				$datos->get_password(),
				PASSWORD_BCRYPT,
				['cost' => 10]))
			->build();

		return $this->repo->add($c->get_id(), $e->get_id(), $datos);
	}


	public function obtener($email) {
		if (!strlen($email))
			throw new DomainException(
			"El email '{$email}' está vacío");

		return $this->repo->get($email);
	}


	public function actualizar($email, alumno_datos $datos, $what) {
		if (!strlen($email))
			throw new DomainException(
			"El email '{$email}' está vacío");

		if (null === $datos)
			throw new DomainException(
			"Faltan los datos a actualizar");

		if (null === $what)
			throw new DomainException(
			"Faltan los datos a actualizar");

		return $this->repo->update($email, $datos, $what);
	}


	public function eliminar($email) {
		if (!strlen($email))
			throw new DomainException(
			"El alumno con email '{$email}' no existe");

		return $this->repo->remove($email);
	}

}
