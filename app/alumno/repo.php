<?php
namespace alumno;

interface repo {
	public function get_all(repo_query $q):coleccion;
	public function add($codigo_id, $escuela_id, alumno_datos $datos);
	public function get($email);
	public function update($email, alumno_datos $datos, $what);
	public function remove($email);
}
