<?php
namespace alumno;

use \BadMethodCallException;

class repo_query {
	protected $offset = 0;
	protected $limit = 10;
	protected $like;

	public function get_offset() { return $this->offset; }
	public function get_limit() { return $this->limit; }
	public function get_like() { return $this->like; }

	protected function __construct($a) {
		if (isset($a['offset']))
			$this->offset = intval($a['offset']);

		if (isset($a['limit']))
			$this->limit = intval($a['limit']);

		if (isset($a['like']))
			$this->like = $a['like'];
	}

	public static function builder(repo_query $q = null) {
		return new class($q) {
			protected $a = [
				'offset' => 0,
				'limit' => 5,
				'like' => null,
			];

			public function __construct($q) {
				if (null === $q)
					return;
				$this->a['offset'] = $q->get_offset();
				$this->a['limit'] = $q->get_limit();
				$this->a['like'] = $q->get_offset();
			}

			public function build() {
				return new class($this->a) extends repo_query {
					public function __construct($a) {
						parent::__construct($a);
					}
				};
			}

			public function __call($k, $vs) {
				if (!in_array($k, array_keys($this->a)))
					throw new BadMethodCallException();
				$this->a[$k] = $vs[0];
				return $this;
			}

		};
	}

}
