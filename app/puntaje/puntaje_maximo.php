<?php
namespace puntaje;

use \DomainException;

class puntaje_maximo {
	private $evaluacion_id;
	private $alumno_id;
	private $juego_id;
	private $puntaje;

	public function __construct($params) {
		if (!strlen($params['evaluacion_id']))
			throw new DomainException(
			"Falta el id de la evaluación");

		if (!strlen($params['alumno_id']))
			throw new DomainException(
			"Falta el id del alumno");

		if (!strlen($params['juego_id']))
			throw new DomainException(
			"Falta el id del juego");

		if (!strlen($params['puntaje']))
			throw new DomainException(
			"Falta el puntaje");

		$this->evaluacion_id = intval($params['evaluacion_id']);
		$this->alumno_id = intval($params['alumno_id']);
		$this->juego_id = intval($params['juego_id']);
		$this->puntaje = intval($params['puntaje']);
	}

	public function get_evaluacion_id() { return $this->evaluacion_id; }
	public function get_alumno_id()     { return $this->alumno_id; }
	public function get_juego_id()      { return $this->juego_id; }
	public function get_puntaje()       { return $this->puntaje; }

	public function __toString() {
		return "evaluacion_id:{$this->evaluacion_id}, ".
			"alumno_id:{$this->alumno_id}, ".
			"juego_id:{$this->juego_id}, ".
			"puntaje {$this->puntaje}";
	}

}
