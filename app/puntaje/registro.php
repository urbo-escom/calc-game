<?php
namespace puntaje;

use \evaluacion\repo as evaluacion_repo;
use \alumno\repo as alumno_repo;
use \juego\repo as juego_repo;

use \DomainException;

class registro {
	private $repo;
	private $erepo;
	private $arepo;
	private $jrepo;

	public function __construct(repo $repo,
			evaluacion_repo $erepo,
			alumno_repo     $arepo,
			juego_repo      $jrepo) {
		$this->repo = $repo;
		$this->erepo = $erepo;
		$this->arepo = $arepo;
		$this->jrepo = $jrepo;
	}


	public function obtener_maximo($evaluacion_id, $alumno_id, $juego_id) {
		if (!strlen($evaluacion_id))
			throw new DomainException(
			"Falta el id de la evaluación");

		if (!strlen($alumno_id))
			throw new DomainException(
			"Falta el id del alumno");

		if (!strlen($juego_id))
			throw new DomainException(
			"Falta el id del juego");

		return $this->repo->get_max(
			$evaluacion_id,
			$alumno_id,
			$juego_id);
	}


	public function agregar(puntaje $p) {
		$e = $this->erepo->get_by_id($eid = $p->get_evaluacion_id());
		$a = $this->arepo->get_by_id($aid = $p->get_alumno_id());
		$j = $this->jrepo->get_by_id($jid = $p->get_juego_id());
		if (!$e)
			throw new DomainException(
			"La evaluación con ID $eid no existe");
		if (!$a)
			throw new DomainException(
			"El alumno con ID $aid no existe");
		if (!$j)
			throw new DomainException(
			"El juego con ID $jid no existe");

		$inicio = $e->get_periodo()->get_inicio()->getTimestamp();
		$termino = $e->get_periodo()->get_termino()->getTimestamp();

		if (time() < $inicio)
			throw new DomainException(
			"{$e->get_nombre()}: Aún no puedes acceder ".
			"a esta evaluación");

		if ($termino < time())
			throw new DomainException(
			"{$e->get_nombre()}: Esta evaluación ya ha ".
			"concluido");

		$ps = $this->repo->get_all($eid, $aid, $jid);
		if ($e->get_intentos() <= count($ps))
			throw new DomainException(
			"Ya has excedido el máximo número de intentos ".
			"({$e->get_intentos()}) para la evaluación ".
			"{$e->get_nombre()}");

		return $this->repo->add($p);
	}

}
