<?php
namespace puntaje;

use \DomainException;

class puntaje {
	private $evaluacion_id;
	private $alumno_id;
	private $juego_id;
	private $intentos;
	private $intento;
	private $ejercicios;
	private $aciertos;

	public function __construct($params) {
		if (!strlen($params['evaluacion_id']))
			throw new DomainException(
			"Falta el id de la evaluación");

		if (!strlen($params['alumno_id']))
			throw new DomainException(
			"Falta el id del alumno");

		if (!strlen($params['juego_id']))
			throw new DomainException(
			"Falta el id del juego");

		if (!strlen($params['intentos']))
			throw new DomainException(
			"Falta el número de intentos permitidos");

		if (!strlen($params['intento']))
			throw new DomainException(
			"Falta el intento");

		if (!strlen($params['ejercicios']))
			throw new DomainException(
			"Falta el número de ejercicios");

		if (!strlen($params['aciertos']))
			throw new DomainException(
			"Falta el número de aciertos");

		if ($params['intentos'] <= 0)
			throw new DomainException(
			"Los intentos deben ser positivos");

		if ($params['intento'] < 0
		|| $params['intentos'] <= $params['intento'])
			throw new DomainException(
			"El intento debe estar entre 0 e intentos-1");

		if ($params['ejercicios'] < 0)
			throw new DomainException(
			"Los ejercicios deben ser no negativos");

		if ($params['aciertos'] < 0)
			throw new DomainException(
			"Los aciertos deben ser no negativos");

		if ($params['ejercicios'] < $params['aciertos'])
			throw new DomainException(
			"El número de aciertos es mayor que los ejercicios");

		$this->evaluacion_id = intval($params['evaluacion_id']);
		$this->alumno_id = intval($params['alumno_id']);
		$this->juego_id = intval($params['juego_id']);
		$this->intentos = intval($params['intentos']);
		$this->intento = intval($params['intento']);
		$this->ejercicios = intval($params['ejercicios']);
		$this->aciertos = intval($params['aciertos']);
	}

	public function get_evaluacion_id() { return $this->evaluacion_id; }
	public function get_alumno_id()     { return $this->alumno_id; }
	public function get_juego_id()      { return $this->juego_id; }
	public function get_intentos()      { return $this->intentos; }
	public function get_intento()       { return $this->intento; }
	public function get_ejercicios()    { return $this->ejercicios; }
	public function get_aciertos()      { return $this->aciertos; }

	public function __toString() {
		return "evaluacion_id:{$this->evaluacion_id}, ".
			"alumno_id:{$this->alumno_id}, ".
			"juego_id:{$this->juego_id}, ".
			"intento {$this->intento}/{$this->intentos}, ".
			"puntaje {$this->aciertos}/{$this->ejercicios}";
	}

}
