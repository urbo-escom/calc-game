<?php
namespace puntaje;

interface repo {
	public function get_all($evaluacion_id, $alumno_id, $juego);
	public function get_max($evaluacion_id, $alumno_id, $juego);
	public function add(puntaje $puntaje);
}
