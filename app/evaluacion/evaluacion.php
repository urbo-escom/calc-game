<?php
namespace evaluacion;

use \DomainException;

class evaluacion {
	private $id;
	private $nombre;
	private $intentos;
	private $periodo;
	private $jugados;

	public function __construct($id, $nombre, $intentos, periodo $p,
			$jugados = 0) {
		if (!strlen($id))
			throw new DomainException(
			"Falta el id");

		if (!strlen($nombre))
			throw new DomainException(
			"El nombre está vacío");

		if (!strlen($intentos))
			throw new DomainException(
			"Falta el número de intentos");

		if (!$p)
			throw new DomainException(
			"Falta el periodo");

		if ($intentos <= 0)
			throw new DomainException(
			"Los intentos deben ser posistivos");

		$this->id       = intval($id);
		$this->nombre   = $nombre;
		$this->intentos = intval($intentos);
		$this->periodo  = $p;
		$this->jugados  = intval($jugados);
	}

	public function get_id()       { return $this->id; }
	public function get_nombre()   { return $this->nombre; }
	public function get_intentos() { return $this->intentos; }
	public function get_periodo()  { return $this->periodo; }
	public function get_jugados()  { return $this->jugados; }

	public function __toString() {
		return "evaluacion:{$this->get_id()}, ".
			"{$this->get_nombre()}, ".
			"jugados {$this->get_jugados()}, ".
			"intentos {$this->get_intentos()}, ".
			"periodo {$this->get_periodo()}";
	}

}
