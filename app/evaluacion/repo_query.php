<?php
namespace evaluacion;

use \BadMethodCallException;
use \DateTime;

class repo_query {
	protected $offset = 0;
	protected $limit = 10;
	protected $before;
	protected $after;
	protected $in;
	protected $like;

	public function get_offset() { return $this->offset; }
	public function get_limit() { return $this->limit; }
	public function get_before() { return $this->before; }
	public function get_after() { return $this->after; }
	public function get_in() { return $this->in; }
	public function get_like() { return $this->like; }

	private function date2unix($p) {
		if (null === $p)
			return $p;
		if (is_int($p))
			return $p;
		if ($p instanceof DateTime)
			return $p->getTimestamp();
		return (new Datetime($p))->getTimestamp();
	}

	protected function __construct($a) {
		if (isset($a['offset']))
			$this->offset = intval($a['offset']);

		if (isset($a['limit']))
			$this->limit = intval($a['limit']);

		if (isset($a['before']))
			$this->before = $this->date2unix($a['before']);

		if (isset($a['after']))
			$this->after = $this->date2unix($a['after']);

		if (isset($a['in']))
			$this->in = $this->date2unix($a['in']);

		if (isset($a['like']))
			$this->like = $a['like'];
	}

	public static function builder(repo_query $q = null) {
		return new class($q) {
			protected $a = [
				'offset' => 0,
				'limit' => 5,
				'before' => null,
				'after' => null,
				'in' => null,
				'like' => null,
			];

			public function __construct($q) {
				if (null === $q)
					return;
				$this->a['offset'] = $q->get_offset();
				$this->a['limit'] = $q->get_limit();
				$this->a['before'] = $q->get_before();
				$this->a['after'] = $q->get_after();
				$this->a['in'] = $q->get_in();
				$this->a['like'] = $q->get_like();
			}

			public function build() {
				return new class($this->a) extends repo_query {
					public function __construct($a) {
						parent::__construct($a);
					}
				};
			}

			public function __call($k, $vs) {
				if (!in_array($k, array_keys($this->a)))
					throw new BadMethodCallException();
				$this->a[$k] = $vs[0];
				return $this;
			}

		};
	}

}
