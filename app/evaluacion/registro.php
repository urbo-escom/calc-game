<?php
namespace evaluacion;

use \DomainException;

class registro {
	private $repo;
	private $strict;

	public function __construct(repo $repo, $strict = true) {
		$this->repo = $repo;
		$this->strict = $strict;
	}

	public function obtener_pasadas(repo_query $query = null) {
		$q = repo_query::builder($query)
			->before(time())
			->build();
		return $this->repo->get_all($q);
	}

	public function obtener_por_venir(repo_query $query = null) {
		$q = repo_query::builder($query)
			->after(time())
			->build();
		return $this->repo->get_all($q);
	}

	public function obtener_actuales(repo_query $query = null) {
		$q = repo_query::builder($query)
			->in(time())
			->build();
		return $this->repo->get_all($q);
	}

	public function obtener_por_id($id) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");
		return $this->repo->get_by_id(intval($id));
	}

	public function obtener($nombre) {
		if (!strlen($nombre))
			throw new DomainException(
			"El nombre está vacío");
		return $this->repo->get($nombre);
	}


	public function agregar($nombre, $intentos, periodo $p) {
		if (!strlen($nombre))
			throw new DomainException(
			"El nombre de la evaluación está vacío");

		if (!strlen($intentos)
		|| $intentos <= 0)
			throw new DomainException(
			"Falta el números de intentos o es negativo");

		$inicio = $p->get_inicio()->getTimestamp();
		$termino = $p->get_termino()->getTimestamp();

		if ($this->strict && $termino <= time())
			throw new DomainException(
			"El término de la evaluación ya pasó");

		if (null !== $this->repo->get($nombre))
			throw new DomainException(
			"La evaluación con nombre '$nombre' ya existe");

		return $this->repo->add($nombre, $intentos, $p);
	}

	public function eliminar($evaluacion) {
		if (!strlen($evaluacion))
			throw new DomainException(
			"La evaluacion está vacía");

		if (null === ($c = $this->repo->get($evaluacion)))
			return null;

		if (0 < ($j = $c->get_jugados()))
			throw new DomainException(
			"La evaluacion '$evaluacion' ya tiene ".
			(1 < $j ? "$j juegos jugados":"$j juego jugado"));

		$this->repo->remove($evaluacion);
	}

}
