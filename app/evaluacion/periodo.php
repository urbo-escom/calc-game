<?php
namespace evaluacion;

use \DateTime;
use \DomainException;

class periodo {
	protected $inicio;
	protected $termino;

	private function arg2date($p) {
		if (is_int($p)) {
			$d = new DateTime();
			$d->setTimestamp($p);
			return $d;
		}
		if ($p instanceof DateTime) {
			$d = new DateTime();
			$d->setTimestamp($p->getTimestamp());
			return $d;
		}
		return new Datetime($p);
	}

	public function __construct($inicio, $termino) {
		if (!$inicio)
			throw new DomainException(
			"El inicio está vacío");

		if (!$termino)
			throw new DomainException(
			"El termino está vacío");

		$this->inicio = $this->arg2date($inicio);
		$this->termino = $this->arg2date($termino);

		$i = $this->inicio->getTimestamp();
		$j = $this->termino->getTimestamp();

		if ($j <= $i)
			throw new DomainException(
			"El término debe suceder después del inicio");

		if (-$i + $j < 20*60)
			throw new DomainException(
			"La duración del periódo es menor a 20 minutos");
	}

	public function get_inicio()  {
		return $this->arg2date($this->inicio);
	}

	public function get_termino() {
		return $this->arg2date($this->termino);
	}

	public function __toString() {
		return "{$this->inicio->format('Y-m-d H:i:s')} - ".
			"{$this->termino->format('Y-m-d H:i:s')}";
	}

}
