<?php
namespace evaluacion;

use \InvalidArgumentException;

class coleccion {
	protected $items;
	protected $count;
	protected $total;

	public function get_items() { return $this->items; }
	public function get_count() { return $this->count; }
	public function get_total() { return $this->total; }

	public function __construct($items, $total) {
		if (!is_array($items))
			throw new InvalidArgumentException(
			"Items is not an array");

		foreach ($items as $i)
			if (!($i instanceof evaluacion))
				throw new InvalidArgumentException(
				"Item is not instance of evaluacion");

		if ($total < 0)
			throw new InvalidArgumentException(
			"Total cannot be negative");

		$this->items = $items;
		$this->count = count($items);
		$this->total = intval($total);
	}

}
