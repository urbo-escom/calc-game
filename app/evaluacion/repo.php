<?php
namespace evaluacion;

interface repo {
	public function get_all(repo_query $q):coleccion;

	public function add($nombre, $intentos, periodo $p);
	public function get_by_id($id);
	public function get($nombre);
	public function remove_by_id($id);
	public function remove($nombre);
}
