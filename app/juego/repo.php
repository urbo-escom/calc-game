<?php
namespace juego;

interface repo {
	public function get_by_id($id);
	public function get_all();
	public function get($nombre);
}
