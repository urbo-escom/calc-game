<?php
namespace juego;

use \DomainException;

class juego {
	private $id;
	private $nombre;
	private $nombre_completo;

	public function __construct($id, $nombre, $nombre_completo) {
		if (!strlen($id))
			throw new DomainException(
			"Falta el id");

		if (!strlen($nombre))
			throw new DomainException(
			"Falta el nombre");

		if (!strlen($nombre_completo))
			throw new DomainException(
			"Falta el nombre completo o largo");

		$this->id = intval($id);
		$this->nombre = $nombre;
		$this->nombre_completo = $nombre_completo;
	}

	public function get_id()              { return $this->id; }
	public function get_nombre()          { return $this->nombre; }
	public function get_nombre_completo() { return $this->nombre_completo; }

	public function __toString() {
		return "juego:{$this->id}, ".
			"nombre {$this->nombre}, ".
			"{$this->nombre_completo}";
	}

}
