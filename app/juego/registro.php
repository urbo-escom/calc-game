<?php
namespace juego;

use \DomainException;

class registro {
	private $repo;

	public function __construct(repo $repo) {
		$this->repo = $repo;
	}


	public function obtener_todos() {
		return $this->repo->get_all();
	}


	public function obtener_por_id($id) {
		if (!strlen($id))
			throw new DomainException(
			"El id está vacío");
		return $this->repo->get_by_id($id);
	}


	public function obtener($nombre) {
		if (!strlen($nombre))
			throw new DomainException(
			"El nombre está vacío");
		return $this->repo->get($nombre);
	}

}
