<?php
namespace request;

class router {
	protected $routes;

	public function __construct() {
		$this->routes = [
			'GET'    => [],
			'POST'   => [],
			'PUT'    => [],
			'DELETE' => [],
		];
	}

	public function add_route($method, $url, $callback) {
		$this->routes[$method][$url] = $callback;
	}

	public function route($request, $data = array()) {
		$r = $request['method'];
		$url = $request['url'];
		foreach ($this->routes[$r] as $k => $v) {
			$rgx = $this->url2rgx($k);
			$matches = [];
			if (1 !== preg_match($rgx, $url, $matches))
				continue;
			$res = $v($matches, $request, $data);
			if (false === $res)
				return $res;
			return true;
		}
		return false;
	}

	/*
	 * url2route($url_pseudo_regex)
	 *
	 * Convierte un pseudo  patrón en regex, como  '/users/([0-9]+)', a un
	 * patrón propio, como '/^\/users\/([0-9]+)$/'. El propósito es que se
	 * puedan escribir expresiones  regulares para URLs con  '/' sin tener
	 * que escribir el escape completo como '\/'. La desventaja de esto es
	 * que no es posible usar el  caracter '/' para otro propósito; eso no
	 * importa porque de todos modos éste caracter no se usa mucho.
	 */
	protected function url2rgx($url) {
		return '/^'.str_replace('/', '\/', $url).'$/';
	}

}
