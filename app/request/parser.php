<?php
namespace request;

/*
 * Headers que pueden alterar el tipo de petición
 *
 * Accept:
 *         Indica el  tipo de  contenido que espera,  normalmente es  un html,
 *         pero es posible que se quiera  recibir json o xml, sobretodo cuando
 *         se usa AJAX.
 *
 * Content-Type: (solo POST)
 *          Indica el tipo de contenido a recibir, normalmente es un form pero
 *          también se puede recibir json o xml.
 *
 * X-HTTP-Method: (solo POST)
 * X-HTTP-Override:
 * X-Method-Override:
 *          Los headers que  empiezan con X- son no estandar,  es decir pueden
 *          ser usados para agregar mayor  información a una petición. En este
 *          caso los tres headers son los más usados para hacer peticiones PUT
 *          y DELETE,  la razón  es que  los navegadores  solo permiten  GET y
 *          POST. Estos headers sobreescriben el  tipo de petición solo cuando
 *          la original es POST.
 *
 */

class parser {
	public $method;
	public $url;
	public $query;
	public $input;
	public $output;

	protected $accept;
	protected $content_type;
	protected $method_override;

	private $param_list = [
		'header', /* los headers recibidos en la petición */
		'server', /* variables de php $_SERVER */
		'get',    /* query de la URL */
		'post',   /* arreglo asociativo de un form */
		'files',  /* arreglo asociativo de los archivos dwl post */
		'input',  /* nombre del archivo del cuerpo de la petición */
	];

	public function __construct($params = array()) {
		foreach ($this->param_list as $k)
			if (!isset($params[$k]))
				throw new InvalidArgumentException(
					"Missing $k");

		$this->method = $params['server']['REQUEST_METHOD'];
		$this->url    = strtok($params['server']['REQUEST_URI'], '?');
		$this->query  = $params['get'];
		$this->input  = false;
		$this->output = 'html';

		$this->parse_headers($params['header']);

		if ('POST'   === $this->method
		||  'PUT'    === $this->method
		||  'DELETE' === $this->method) {
			$this->parse_input_type(
				$params['post'],
				$params['files'],
				$params['input']);
			$this->parse_method_override();
		}

		$this->parse_output_type();
	}

	public function parse_headers($headers) {
		$arr = [];
		foreach ($headers as $k => $v)
			$arr[strtolower($k)] = $v;

		if (isset($arr['accept']))
			$this->accept = $arr['accept'];
		if (isset($arr['content-type']))
			$this->content_type = $arr['content-type'];

		if (isset($arr[$h = 'x-http-method']))
			$this->method_override = strtoupper($arr[$h]);
		if (isset($arr[$h = 'x-http-method-override']))
			$this->method_override = strtoupper($arr[$h]);
		if (isset($arr[$h = 'x-method-override']))
			$this->method_override = strtoupper($arr[$h]);
	}


	public function parse_input_type($post, $files, $input) {
		/*
		 * Los  tres tipos  de contenido  para un  POST|PUT|DELETE son
		 * form normal, form con archivos y json:
		 */
		$form      = 'application/x-www-form-urlencoded';
		$multipart = 'multipart/form-data';
		$json      = 'application/json';

		/*
		 * Los  tres tipos  de contenido  se normalizan  a un  arreglo
		 * asociativo,  la  finalidad es  que  se  puedan acceder  los
		 * parámetros de  igual forma  sin importar  que vengan  de un
		 * form o de un json.
		 */

		/* para forms con o sin archivos usa $_POST */
		if (false !== strpos($this->content_type, $form)
		||  false !== strpos($this->content_type, $multipart))
			$this->input = $post;

		/* agrega las variables de los archivos para un multipart */
		if (false !== strpos($this->content_type, $multipart))
			$this->input['_files'] = $files;

		/* para json el cuerpo de la petición se escanea manualmente */
		if (FALSE !== strpos($this->content_type, $json))
			$this->input = json_decode(
				file_get_contents($input), true);
	}

	public function parse_method_override() {
		if ('GET' === $this->method)
			return;

		if (null !== $this->method_override && (false
		|| $this->method_override === 'POST'
		|| $this->method_override === 'PUT'
		|| $this->method_override === 'DELETE'))
			$this->method = $this->method_override;

		if (isset($this->input['_method']))
			$this->method = strtoupper($this->input['_method']);
	}

	public function parse_output_type() {
		$h = $this->accept;
		$this->output = 'html';

		if (null === $h || 1 === preg_match('/html|\*\/\*/i', $h))
			return;

		if (1 === preg_match('/application\/json/i', $h))
			$this->output = 'json';
	}

}
