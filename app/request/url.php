<?php
namespace request;

class url {
	protected $base;
	protected $full;

	public function __construct($base) {
		$this->base = $base;
		$this->full = true;
	}

	public function set_base($base) {
		$this->base = $base;
	}

	public function set_full($full) {
		$this->full = $full;
	}

	public function generate_url($query = []) {
		if ($this->full)
			$s = $this->full_url($_SERVER, $this->base);
		else
			$s = $base;
		if (0 === sizeof($query))
			return $s;
		$q = [];
		foreach ($query as $k => $v) {
			if (null === $v)
				continue;
			$q[] = urlencode($k).'='.urlencode($v);
		}
		return $s.array_reduce($q, function($acc, $p) {
			if (null === $acc)
				return "?$p";
			return "$acc&$p";
		});
	}

	public function paginated_links($total, $query) {
		$offset = $query['offset'];
		$limit = $query['limit'];

		$query['offset'] = 0;
		$first = $this->generate_url($query);

		$r = $total%$limit;
		if (0 === $r)
			$query['offset'] = $total - $limit;
		else
			$query['offset'] = $total - $r;
		$last = $this->generate_url($query);

		$prev = false;
		if (0 <= $offset - $limit) {
			$query['offset'] = $offset - $limit;
			$prev = $this->generate_url($query);
		}

		$next = false;
		if ($offset + $limit < $total) {
			$query['offset'] = $offset + $limit;
			$next = $this->generate_url($query);
		}

		return [
			'first' => $first,
			'prev'  => $prev,
			'next'  => $next,
			'last'  => $last,
		];
	}

	protected function protocol($s) {
		$has_ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');
		$t    = strtolower($s['SERVER_PROTOCOL']);
		$proto = substr($t, 0, strpos($t, '/'));
		if ($has_ssl)
			$proto = $proto.'s';
		return $proto;
	}

	protected function host($s, $forwarded = false) {
		$has_ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');
		$port = $s['SERVER_PORT'];
		if ((!$has_ssl && $port == '80')
		|| ($has_ssl && $port == '443'))
			$port = '';
		else
			$port = ':'.$port;
		if ($forwarded && isset($s['HTTP_X_FORWARDED_HOST']))
			$host = $s['HTTP_X_FORWARDED_HOST'];
		else if (isset($s['HTTP_HOST']))
			$host = $s['HTTP_HOST'];
		else
			$host = $s['SERVER_NAME'].$port;
		return $host;
	}

	protected function origin($s, $forwarded = false) {
		$has_ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');

		$sp    = strtolower($s['SERVER_PROTOCOL']);
		$proto = substr($sp, 0, strpos($sp, '/'));
		if ($has_ssl)
			$proto = $proto.'s';

		$port  = $s['SERVER_PORT'];
		if ((!$has_ssl && $port == '80')
		|| ($has_ssl && $port == '443'))
			$port = '';
		else
			$port = ':'.$port;

		if ($forwarded && isset($s['HTTP_X_FORWARDED_HOST']))
			$host = $s['HTTP_X_FORWARDED_HOST'];
		else if (isset($s['HTTP_HOST']))
			$host = $s['HTTP_HOST'];
		else
			$host = $s['SERVER_NAME'].$port;

		return $proto.'://'.$host;
	}

	protected function full_url($s, $path = '', $forwarded = false) {
		return $this->origin($s, $forwarded).$path;
	}

}
