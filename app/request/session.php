<?php
namespace request;

use \Exception;

class session {
	private $is_anon  = true;
	private $is_user  = false;
	private $is_admin = false;
	private $is_closed = true;
	protected $special_vars = array(
		'session.user',
		'session.admin',
	);

	public function is_anon()  { return $this->is_anon; }
	public function is_user()  { return $this->is_user; }
	public function is_admin() { return $this->is_admin; }

	public function __construct() {
		if (PHP_SESSION_NONE === session_status())
			session_start();
		if (PHP_SESSION_DISABLED === session_status())
			throw new Exception("disabled sessions");
		$this->is_user =
			isset($_SESSION['session.user']) &&
			$_SESSION['session.user'] == '1';
		$this->is_admin =
			isset($_SESSION) &&
			isset($_SESSION['session.admin']) &&
			$_SESSION['session.admin'] == '1';
		$this->is_anon = !$this->is_user && !$this->is_admin;
		$this->is_closed = false;
	}

	private function unset_specials() {
		foreach ($this->special_vars as $s)
			unset($_SESSION[$s]);
		$this->is_anon  = true;
		$this->is_user  = false;
		$this->is_admin = false;
	}

	public function login_as_user() {
		$this->unset_specials();
		$_SESSION['session.user'] = '1';
		return $this->is_user = true;
	}

	public function login_as_admin() {
		$this->unset_specials();
		$_SESSION['session.admin'] = '1';
		return $this->is_admin = true;
	}

	public function logout() {
		$this->unset_specials();
		$p = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$p['path'], $p['domain'],
			$p['secure'], $p['httponly']
		);
		$r = session_destroy();
		$this->unset_specials();
		if (isset($_SESSION))
			foreach ($_SESSION as $k)
				unset($_SESSION[$k]);
		return $r;
	}

	public function close() {
		session_write_close();
		$this->is_closed = true;
	}

	public function set($var, $value) {
		if ($this->is_closed)
			throw new Exception("setting session var after close");
		foreach ($this->special_vars as $s)
			if ($s == $var)
				return null;
		return $_SESSION[$var] = $value;
	}

	public function get($var) {
		if (isset($_SESSION[$var]))
			return $_SESSION[$var];
		return null;
	}

	public function unset($var) {
		foreach ($this->special_vars as $s)
			if ($s == $var)
				return null;
		if (!isset($_SESSION[$var]))
			return null;
		$v = $_SESSION[$var];
		unset($_SESSION[$var]);
		return $v;
	}

}
