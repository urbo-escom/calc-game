<?php
namespace repo\pgsql;

use \codigo\codigo;
use \codigo\coleccion;
use \codigo\repo;
use \codigo\repo_query;
use \database\client;

class codigo_repo implements repo {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private function create_from_row($r) {
		return new codigo(
			$r['id'],
			$r['codigo'],
			isset($r['email']) ? $r['email']:null);
	}

	protected $select = [
		'select' => [
			'c.id     as id',
			'c.codigo as codigo',
			'a.email  as email',
		],
		'from' => [[
			'LEFT_JOIN',
			'codigo as c',
			'alumno as a',
			'ON' => 'c.id = a.codigo_id',
		]],
	];

	protected $select_count = [
		'select' => 'count(*) as total',
		'from' => [[
			'LEFT_JOIN',
			'codigo as c',
			'alumno as a',
			'ON' => 'c.id = a.codigo_id',
		]],
	];

	public function get_all(repo_query $q):coleccion {
		$s1 = array_merge([], $this->select);
		$s2 = array_merge([], $this->select_count);
		$s1['offset'] = $q->get_offset();
		$s1['limit'] = $q->get_limit();

		$like = $q->get_like() ?
			$this->db->escape("%{$q->get_like()}%"):null;
		$w = [];

		if (null !== $q->get_usado()) {
			if ($q->get_usado())
				$w[] = "a.email IS NOT NULL";
			else
				$w[] = "a.email IS NULL";
		}

		if ($like)
			$w[] = "lower(c.codigo) like lower($like)";

		if (0 < count($w)) {
			$s1['where'] = $w;
			$s2['where'] = $w;
		}

		$rs = $this->db->select($s1);
		$total = $this->db->select($s2)[0]['total'];
		$items = [];
		foreach ($rs as $r)
			$items[] = $this->create_from_row($r);
		return new coleccion($items, $total);
	}

	public function add($codigo) {
		$rs = $this->db->insert([
			'insert' => 'codigo',
			'column' => 'codigo',
			'values' => [
				'codigo' => $this->db->escape($codigo),
			],
			'returning' => 'id, codigo',
		]);
		return $this->create_from_row($rs[0]);
	}

	public function get_by_id($id) {
		$rs = $this->db->select([
			'select' => [
				'c.id     as id',
				'c.codigo as codigo',
				'a.email  as email',
			],
			'from' => [[
				'LEFT_JOIN',
				'codigo as c',
				'alumno as a',
				'ON' => 'c.id = a.codigo_id',
			]],
			'where' => 'c.id = '.$this->db->escape($id),
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get($codigo) {
		$rs = $this->db->select([
			'select' => [
				'c.id     as id',
				'c.codigo as codigo',
				'a.email  as email',
			],
			'from' => [[
				'LEFT_JOIN',
				'codigo as c',
				'alumno as a',
				'ON' => 'c.id = a.codigo_id',
			]],
			'where' => 'c.codigo = '.$this->db->escape($codigo),
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function remove($codigo) {
		$rs = $this->db->delete([
			'delete' => 'codigo',
			'where' => [
				'codigo = '.$this->db->escape($codigo),
			],
			'returning' => 'id, codigo',
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

}
