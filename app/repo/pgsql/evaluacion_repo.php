<?php
namespace repo\pgsql;

use \evaluacion\coleccion;
use \evaluacion\evaluacion;
use \evaluacion\periodo;
use \evaluacion\repo;
use \evaluacion\repo_query;

class evaluacion_repo implements repo {
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	protected $select = [
		'select' => [
			'e.id       as id',
			'e.nombre   as nombre',
			'e.intentos as intentos',
			'e.fecha_inicio  as fecha_inicio',
			'e.fecha_termino as fecha_termino',
			'count(*) as jugados',
		],
		'from' => [[
			'LEFT_JOIN',
			'evaluacion as e',
			'puntaje    as p',
			'ON' => 'e.id = p.evaluacion_id',
		]],
		'group_by' => [
			'e.id',
			'e.nombre',
			'e.intentos',
			'e.fecha_inicio',
			'e.fecha_termino',
		],
	];

	protected $select_count = [
		'select' => 'count(*) as total',
		'from' => 'evaluacion as e',
	];

	private function create_from_row($row) {
		return new evaluacion(
			$row['id'],
			$row['nombre'],
			$row['intentos'],
			new periodo(
				intval($row['fecha_inicio']),
				intval($row['fecha_termino'])),
			isset($row['juegos']) ?
				$row['juegos']:0);
	}

	public function add($nombre, $intentos, periodo $p) {
		$rs = $this->db->insert([
			'insert' => 'evaluacion',
			'column' => [
				'nombre',
				'intentos',
				'fecha_inicio',
				'fecha_termino',
			],
			'values' => [
				'nombre' => $this->db->escape($nombre),
				'intentos' => (int)$intentos,
				'fecha_inicio' =>
					$p->get_inicio()->getTimestamp(),
				'fecha_termino' =>
					$p->get_termino()->getTimestamp(),
			],
			'returning' => '*',
		]);
		return $this->create_from_row($rs[0]);
	}

	public function get_by_id($id) {
		$s = array_merge(array(), $this->select);
		$s['having'] = 'e.id = '.$id;
		$rs = $this->db->select($s);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get($nombre) {
		$s = array_merge(array(), $this->select);
		$s['having'] = 'e.nombre = '.$this->db->escape($nombre);
		$rs = $this->db->select($s);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function remove_by_id($id) {
		$rs = $this->db->delete([
			'delete' => 'evaluacion',
			'where' => 'id = '.$id,
			'returning' => '*',
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function remove($nombre) {
		$rs = $this->db->delete([
			'delete' => 'evaluacion',
			'where' => 'nombre = '.$this->db->escape($nombre),
			'returning' => '*',
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get_all(repo_query $q):coleccion {
		$s1 = array_merge(array(), $this->select);
		$s2 = array_merge(array(), $this->select_count);
		$s1['offset'] = $q->get_offset();
		$s1['limit'] = $q->get_limit();

		$like = $q->get_like() ?
			$this->db->escape("%{$q->get_like()}%"):null;
		$w = [];

		if (null !== $q->get_before())
			$w[] = "e.fecha_termino < {$q->get_before()}";

		if (null !== $q->get_after())
			$w[] = "{$q->get_after()} < e.fecha_inicio";

		if (null !== $q->get_in()) {
			$w[] = "e.fecha_inicio <= {$q->get_in()}";
			$w[] = "{$q->get_in()} <= e.fecha_termino";
		}

		if ($like)
			$w[] = "lower(e.nombre) like lower($like)";

		if (0 < count($w)) {
			$s1['having'] = $w;
			$s2['where'] = $w;
		}

		$rs = $this->db->select($s1);
		$total = $this->db->select($s2)[0]['total'];
		$items = [];
		foreach ($rs as $r)
			$items[] = $this->create_from_row($r);
		return new coleccion($items, $total);
	}

}
