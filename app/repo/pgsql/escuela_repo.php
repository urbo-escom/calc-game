<?php
namespace repo\pgsql;

use \database\client;
use \escuela\coleccion;
use \escuela\escuela;
use \escuela\repo;
use \escuela\repo_query;

class escuela_repo implements repo {
	private $db;

	public function __construct(client $db) {
		$this->db = $db;
	}

	private function create_from_row($row) {
		return new escuela(
			$row['id'],
			$row['nombre'],
			isset($row['alumnos']) ? $row['alumnos']:0);
	}

	protected $select = [
		'select' => [
			'e.id        as id',
			'e.nombre    as nombre',
			'count(a.id) as alumnos',
		],
		'from' => [[
			'LEFT_JOIN',
			'escuela as e',
			'alumno  as a',
			'ON' => 'e.id = a.escuela_id',
		]],
		'group_by' => 'e.id, e.nombre',
	];

	protected $select_count = [
		'select' => 'count(*) as total',
		'from' => 'escuela as e',
	];

	public function get_all(repo_query $q):coleccion {
		$s1 = array_merge([], $this->select);
		$s2 = array_merge([], $this->select_count);
		$s1['offset'] = $q->get_offset();
		$s1['limit'] = $q->get_limit();

		$like = $q->get_like() ?
			$this->db->escape("%{$q->get_like()}%"):null;
		$w = [];

		if ($like)
			$w[] = "lower(e.nombre) like lower($like)";

		if (0 < count($w)) {
			$s1['having'] = $w;
			$s2['where'] = $w;
		}

		$rs = $this->db->select($s1);
		$total = $this->db->select($s2)[0]['total'];
		$items = [];
		foreach ($rs as $r)
			$items[] = $this->create_from_row($r);
		return new coleccion($items, $total);
	}

	public function add($escuela) {
		$rs = $this->db->insert([
			'insert' => 'escuela',
			'column' => 'nombre',
			'values' => [
				'nombre' => $this->db->escape($escuela),
			],
			'returning' => 'id, nombre',
		]);
		return $this->create_from_row($rs[0]);
	}

	public function get_by_id($id) {
		$s = array_merge([], $this->select);
		$s['having'] = 'e.id = '.intval($id);
		$rs = $this->db->select($s);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get($escuela) {
		$s = array_merge([], $this->select);
		$s['having'] = 'e.nombre = '.$this->db->escape($escuela);
		$rs = $this->db->select($s);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function update($escuela, $nuevo_nombre) {
		$e1 = $this->db->escape($escuela);
		$e2 = $this->db->escape($nuevo_nombre);
		$rs = $this->db->insert([
			'update' => 'escuela',
			'column' => 'nombre',
			'values' => [
				'nombre' => $e2,
			],
			'where' => 'nombre = '.$e1,
			'returning' => 'id, nombre',
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function remove($escuela) {
		$rs = $this->db->delete([
			'delete' => 'escuela',
			'where' => [
				'nombre = '.$this->db->escape($escuela),
			],
			'returning' => 'id, nombre',
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

}
