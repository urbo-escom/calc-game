<?php
namespace repo\pgsql;

use \puntaje\puntaje;
use \puntaje\puntaje_maximo;
use \puntaje\repo;

class puntaje_repo implements repo {
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	private function create($row) {
		return new puntaje($row);
	}

	private function create_max($row) {
		return new puntaje_maximo($row);
	}

	public function add(puntaje $p) {
		$intentos_hechos = $this->db->select([
			'select' => 'intentos_hechos as hechos',
			'from'   => 'puntaje_maximo',
			'where' => [
				'evaluacion_id = '.$p->get_evaluacion_id(),
				'alumno_id = '.$p->get_alumno_id(),
				'juego_id = '.$p->get_juego_id(),
			],
		]);
		if (!$intentos_hechos)
			$intentos_hechos = 0;
		else
			$intentos_hechos = $intentos_hechos[0]['hechos'];

		$ev = $this->db->select([
			'select' => 'intentos, fecha_inicio, fecha_termino',
			'from' => 'evaluacion',
			'where' => 'id = '.$p->get_evaluacion_id(),
		])[0];

		$r = $this->db->insert([
			'insert' => 'puntaje',
			'column' => [
				'evaluacion_id',
				'alumno_id',
				'juego_id',
				'intentos',
				'intento',
				'ejercicios',
				'aciertos',
				'fecha_inicio',
				'fecha_termino',
			],
			'values' => [
				'evaluacion_id' => $p->get_evaluacion_id(),
				'alumno_id'     => $p->get_alumno_id(),
				'juego_id'      => $p->get_juego_id(),
				'intentos'      => (int)$ev['intentos'],
				'intento'       => (int)$intentos_hechos,
				'ejercicios'    => $p->get_ejercicios(),
				'aciertos'      => $p->get_aciertos(),
				'fecha_inicio'  => (int)$ev['fecha_inicio'],
				'fecha_termino' => (int)$ev['fecha_termino'],
			],
			'returning' => '*',
		]);
		return $this->create($r[0]);
	}

	public function get_all($evaluacion_id, $alumno_id, $juego_id) {
		$rs = $this->db->select([
			'select' => '*',
			'from' => 'puntaje',
			'where' => [
				'evaluacion_id = '.$evaluacion_id,
				'alumno_id = '.$alumno_id,
				'juego_id = '.$juego_id,
			],
		]);
		$all = [];
		if (!$rs)
			return $all;
		foreach ($rs as $r)
			$all[] = $this->create($r);
		return $all;
	}

	public function get_max($evaluacion_id, $alumno_id, $juego_id) {
		$r = $this->db->select([
			'select' => '*',
			'from' => 'puntaje_maximo',
			'where' => [
				'evaluacion_id = '.$evaluacion_id,
				'alumno_id = '.$alumno_id,
				'juego_id = '.$juego_id,
			],
		]);
		if (0 === count($r))
			return $this->create_max([
				'evaluacion_id' => $evaluacion_id,
				'alumno_id' => $alumno_id,
				'juego_id' => $juego_id,
				'puntaje' => 0,
			]);
		if (!$r)
			return null;
		return $this->create_max($r[0]);
	}

}
