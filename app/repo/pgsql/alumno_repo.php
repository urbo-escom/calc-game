<?php
namespace repo\pgsql;

use \alumno\alumno;
use \alumno\alumno_datos;
use \alumno\coleccion;
use \alumno\repo;
use \alumno\repo_query;
use \escuela\escolaridad;

use \Exception;

class alumno_repo implements repo {
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	private function create_from_row($r) {
		return new alumno(
			$r['id'],
			$r['codigo_id'],
			$r['escuela_id'],
			alumno_datos::builder()
				->email($r['email'])
				->password($r['password'])
				->nombre($r['nombre'])
				->apellido_pat($r['apellido_pat'])
				->apellido_mat($r['apellido_mat'])
				->escolaridad(new escolaridad(
					$r['nivel'],
					$r['grado'],
					$r['grupo']))
				->build());
	}

	public function add($codigo_id, $escuela_id, alumno_datos $datos) {
		$esc = $datos->get_escolaridad();

		$email        = $this->db->escape($datos->get_email());
		$password     = $this->db->escape($datos->get_password());
		$nombre       = $this->db->escape($datos->get_nombre());
		$apellido_pat = $this->db->escape($datos->get_apellido_pat());
		$apellido_mat = $this->db->escape($datos->get_apellido_mat());
		$nivel = $this->db->escape($esc->get_nivel());
		$grado = $this->db->escape($esc->get_grado());
		$grupo = $this->db->escape($esc->get_grupo());

		$rs = $this->db->insert([
			'insert' => 'alumno',
			'column' => [
				'codigo_id',
				'escuela_id',
				'email',
				'password',
				'nombre',
				'apellido_pat',
				'apellido_mat',
				'nivel',
				'grado',
				'grupo',
			],
			'values' => [
				'codigo_id'    => $codigo_id,
				'escuela_id'   => $escuela_id,
				'email'        => $email,
				'password'     => $password,
				'nombre'       => $nombre,
				'apellido_pat' => $apellido_pat,
				'apellido_mat' => $apellido_mat,
				'nivel' => $nivel,
				'grado' => $grado,
				'grupo' => $grupo,
			],
			'returning' => '*',
		]);
		return $this->create_from_row($rs[0]);
	}

	public function get_by_id($id) {
		$rs = $this->db->select([
			'select' => '*',
			'from' => 'alumno',
			'where' => 'id = '.$id,
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get($email) {
		$rs = $this->db->select([
			'select' => '*',
			'from' => 'alumno',
			'where' => 'email = '.$this->db->escape($email),
		]);
		if (!$rs)
			return null;
		return $this->create_from_row($rs[0]);
	}

	public function get_all(repo_query $q):coleccion {
		$s1 = ['select' => '*', 'from' => 'alumno'];
		$s2 = ['select' => 'count(*) as total', 'from' => 'alumno'];
		$s1['offset'] = $q->get_offset();
		$s1['limit'] = $q->get_limit();

		$like = $q->get_like() ?
			$this->db->escape("%{$q->get_like()}%"):null;
		$w = [];

		if ($like) {
			$w[] = 'OR';
			$w[] = "lower(email) like lower($like)";
			$w[] = "lower(nombre) like lower($like)";
			$w[] = "lower(apellido_pat) like lower($like)";
			$w[] = "lower(apellido_mat) like lower($like)";
		}

		if (0 < count($w)) {
			$s1['where'] = $w;
			$s2['where'] = $w;
		}

		$rs = $this->db->select($s1);
		$total = $this->db->select($s2)[0]['total'];
		$items = [];
		foreach ($rs as $r)
			$items[] = $this->create_from_row($r);
		return new coleccion($items, $total);
	}

	private function _update($i, alumno_datos $datos, $what) {
		$a = $this->arr[$i];
		$b = alumno_datos::builder();

		if ($what['email'])
			$b = $b->email($what['email']);
		else
			$b = $b->email($a->get_email());

		if ($what['nombre'])
			$b = $b->nombre($what['nombre']);
		else
			$b = $b->nombre($a->get_nombre());

		if ($what['apellido_pat'])
			$b = $b->apellido_pat($what['apellido_pat']);
		else
			$b = $b->apellido_pat($a->get_apellido_pat());

		if ($what['apellido_mat'])
			$b = $b->apellido_mat($what['apellido_mat']);
		else
			$b = $b->apellido_mat($a->get_apellido_mat());

		if ($what['escolaridad'])
			$b = $b->escolaridad($what['escolaridad']);
		else
			$b = $b->escolaridad($a->get_escolaridad());

		unset($this->arr[$i]);
		$this->arr[$i] = $b->build();
	}

	public function update($email, alumno_datos $datos, $what) {
		throw new Exception("Not implemented yet");
	}

	public function remove($email) {
		throw new Exception("Not implemented yet");
	}

}
