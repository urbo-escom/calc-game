<?php
namespace repo\pgsql;

use \juego\juego;
use \juego\repo;

class juego_repo implements repo {
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	private function create_from_row($row) {
		return new juego(
			$row['id'],
			$row['nombre'],
			$row['nombre_completo']);
	}

	public function get_by_id($id) {
		$r = $this->db->select([
			'select' => '*',
			'from' => 'juego',
			'where' => 'id = '.$id,
		]);
		if (!$r)
			return null;
		return $this->create_from_row($r[0]);
	}

	public function get($nombre) {
		$r = $this->db->select([
			'select' => '*',
			'from' => 'juego',
			'where' => 'nombre = '.$this->db->escape($nombre),
		]);
		if (!$r)
			return null;
		return $this->create_from_row($r[0]);
	}

	public function get_all() {
		$rs = $this->db->select([
			'select' => '*',
			'from' => 'juego',
		]);
		$all = [];
		if (!$rs)
			return $all;
		foreach ($rs as $r)
			$all[] = $this->create_from_row($r);
		return $all;
	}

}
